package main;
import static org.junit.Assert.*;
import main.GUI.Main;

import org.junit.Test;


public class MainTest {

    @Test
    public void test() {
        assertEquals(7, Main.addition(5, 2));
    }
    
    @Test
    public void addition() {
        assertEquals(4, Main.addition(2, 2));
    }

}
