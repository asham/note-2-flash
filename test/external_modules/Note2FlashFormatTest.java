package external_modules;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Card;
import model.DataStorage;
import model.Deck;
import model.QueryProcessor;

public class Note2FlashFormatTest extends ExternalFormatBase {
    private static final String TEST_DIR_LOCATION = "test";
    private static final String DATABASE_COPY_NAME = "note2flash_db_copy";
    
    private static Path tempTestDir;

    /**
     * Constructs a new Note2FlashFormatTest.
     */
    public Note2FlashFormatTest() {
        extFormat = new Note2FlashFormat();
    }
    
    /*
     * Setup the folder that will store all
     * temporary exported files.
     */
    @BeforeClass
    public static void setupFolder() {
        // create a temporary folder in "test" directory
        // to store exported files created during unit testing
        tempTestDir = null;
        try {
            tempTestDir = Files.createTempDirectory(Paths.get(TEST_DIR_LOCATION), null);         
        } catch (IOException e) { 
            e.printStackTrace();
        }
    }
     
    /*
     * Verify that the Note2Flash database can be exported
     * correctly. 
     */
    @Test
    public void testExportCards() {              
        List<Card> cardsToExport 
            = new ArrayList<Card>(Arrays.asList(c1, c2, c3, c4));
        try {
            extFormat.exportCards(tempTestDir.toString(), cardsToExport);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        DataStorage ds = QueryProcessor.getInstance();
        Path dbLocalPath = Paths.get(tempTestDir.toString(), 
                                    DATABASE_COPY_NAME);
                
        ds.changeSaveLocation(dbLocalPath.toAbsolutePath().toString());     
        
        // verify that cards were exported correctly
        Deck defaultDeck = ds.getDeck("DeckCopy");        
        List<Card> exportedCards = defaultDeck.getCards();
        assertEquals(cardsToExport.size(), exportedCards.size());
        
        for (Card card: cardsToExport) {
            assertTrue(exportedCards.contains(card));
        }              
        
        // Change location back to initial location so 
        // that the database files are not in use by the 
        // QueryProcessor when they are being deleted.
        ds.changeSaveLocation("~/test-db");
    }

    @Override
    @Before
    public void setup() {
        // reset the database
        ds = QueryProcessor.getInstance();
        ds.changeSaveLocation("~/test-db");
        ds.clearData(); 
    }
    
    /*
     * Remove all of the files and folders
     * created as a part of the export tests.
     */
    @AfterClass
    public static void removeTempFiles() {
        try {            
            // delete the test files
            File dbCopy = new File(tempTestDir.toString());
            String[] fileNames = dbCopy.list();
            for (String fileName: fileNames) {
                File tempFile = new File(dbCopy, fileName);
                Files.delete(Paths.get(tempFile.toURI()));
            }   
            
            Files.delete(tempTestDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }    
}
