package external_modules;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import model.Card;
import model.QueryProcessor;
import external_modules.AnkiFormat;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AnkiFormatTest extends ExternalFormatBase {
    private static final String TEST_DIR_LOCATION = "test";
    private static final String ANKI_TEXT_FILE = "ankiFormat.txt";
    private static final String IMPORT_DIR = "media/anki/";   
    private static final String ANKI_MEDIA_FOLDER = "collection.media";   
    private static final String TEST_DB_NAME = "~/test_db";
    private static final String TEST_DECK_NAME = "test_deck";
    
    private static Path tempTestDir;
	
    public AnkiFormatTest() {
        super();
        extFormat = new AnkiFormat();          
    }
    
    /*
     * Setup the folder that will store all
     * temporary exported files.
     */
    @BeforeClass
    public static void setupFolder() {
        // create a temporary folder in "test" directory
        // to store exported files created during unit testing
        tempTestDir = null;
        try {
            tempTestDir = Files.createTempDirectory(Paths.get(TEST_DIR_LOCATION), null);         
        } catch (IOException e) { 
            e.printStackTrace();
        }
    }
    
    /*
     * Remove all of the files and folders
     * created as a part of the export tests.
     */
    @AfterClass
    public static void removeTempFiles() {
        try {
            Files.delete(Paths.get(tempTestDir.toString(), ANKI_TEXT_FILE));
            
            File ankiMediaFolder = new File(tempTestDir.toString(), ANKI_MEDIA_FOLDER);
            String[] allImages = ankiMediaFolder.list();
            if (allImages != null) {
                for (String imgName: allImages) {
                    File imgFile = new File(ankiMediaFolder, imgName);
                    Files.delete(Paths.get(imgFile.toURI()));
                }
            }            
            
            Files.delete(Paths.get(tempTestDir.toString(), ANKI_MEDIA_FOLDER));
            
            Files.delete(tempTestDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }    
    
    /*
     * Reset the database, remove all temporary
     * images from media folder.
     */
    @Before
    public void setup() {
        // reset the database
        ds = QueryProcessor.getInstance();
        ds.changeSaveLocation(TEST_DB_NAME);
        ds.clearData();        
        
        // remove all images from anki media folder
        File ankiMediaFolder = new File(tempTestDir.toString(), ANKI_MEDIA_FOLDER);
        String[] allImages = ankiMediaFolder.list();
        if (allImages != null) {
            for (String imgName: allImages) {
                File imgFile = new File(ankiMediaFolder, imgName);
                assertTrue(imgFile.delete());
            }
        }
    }        

    /*
     * Verify that the exported Anki file matches the content
     * for a particular file.
     * @param card The Card that was exported.
     * @param frontSide The expected front side of the Card as a string.
     * @param backSide The expected back side of the Card as a string.
     */
    private void verifyTextExport(Card card, String frontSide, String backSide) {
        assertTrue(card != null);
        List<Card> cards = new ArrayList<Card>(Arrays.asList(card));       
        try {
            extFormat.exportCards(tempTestDir.toString(), cards);
        } catch (Exception e) {
            e.printStackTrace();
        }        
        
        File ankiTextFile = new File(tempTestDir.toString(), ANKI_TEXT_FILE);
        assertTrue(ankiTextFile.exists());
        
        try {
            Scanner fileScanner = new Scanner(ankiTextFile);
            fileScanner.useDelimiter("\t");                    
            
            assertTrue(fileScanner.hasNext());
            
            // remove <div> wrapper
            String cardFront = fileScanner.next();
            assertTrue(cardFront.startsWith("<div>"));
            cardFront = cardFront.replaceAll("<div>", "");          
            
            // verify newline characters have been escaped
            if (frontSide.contains("\n")) {
                assertTrue(cardFront.equals(frontSide.replaceAll("\n", "<br />")));
            } else {
                assertTrue(cardFront.equals(frontSide));
            }      
            
            assertTrue(fileScanner.hasNext());
            Set<String> allTags = card.getTags();
            
            String cardBack = fileScanner.next();            
            if (allTags.size() == 0) {
                // if there are no tags then remove newline 
                // from last token
                cardBack = cardBack.trim();   
            }

            // remove </div> wrapper
            assertTrue(cardBack.endsWith("</div>"));
            cardBack = cardBack.replaceAll("</div>", "");
            
            // verify newline characters have been escaped
            if (backSide.contains("\n")) {
                assertTrue(cardBack.equals(backSide.replaceAll("\n", "<br />")));
            } else {
                assertTrue(cardBack.equals(backSide));
            }      
            
            if (allTags.size() != 0) {
                // get tags, remove new line
                assertTrue(fileScanner.hasNext());
                String tags = fileScanner.next().trim();                  
                
                String[] exportTags = tags.split(" ");  
                for (String tag: exportTags) {
                    assertTrue(allTags.contains(tag));
                }
            }                                                
            
            fileScanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    } 
    
    /*
     * Verify that images are exported correctly. 
     * @param frontImage The expected front image as a String.
     * @param backImage The expected back image as a String.
     */
    private void verifyMediaExport(String frontImage, String backImage) {
        File mediaFolder = new File(tempTestDir.toString(), ANKI_MEDIA_FOLDER);
        assertTrue(mediaFolder.exists());                
        
        List<String> dirContents 
            = new ArrayList<String>(Arrays.asList(mediaFolder.list()));
     
        if (frontImage != null) {
            assertTrue(dirContents.contains(frontImage));
        }        
        if (backImage != null) {
            assertTrue(dirContents.contains(backImage));
        }
    }
    
    /*
     * Verify that Cards with no tags are exported correctly.
     */
    @Test
    public void testNoTags() {
        verifyTextExport(c6, textSide1, textSide2);
    }
    
    /*
     * Verify that Cards with text that contain newline
     * characters are escaped with <br /> tags. 
     */
    @Test
    public void testTextEscape() {
        verifyTextExport(c5, textSide3, textSide4);
    }
    
    /*
     * Verify that Card with simple text on the front and back
     * can be exported to Anki correctly.
     */
    @Test
    public void testExportFrontTextBackText() {
        verifyTextExport(c1, textSide1, textSide2);
    }
    
    /*
     * Verify that Card with simple text on the front and image
     * on back can be exported to Anki correctly.
     */
    @Test
    public void testExportFrontTextBackImage() {
        AnkiFormat af = new AnkiFormat();
        String backImage = "img0.jpg";
        
        verifyTextExport(c2, textSide1, af.createImgTag(backImage));
        verifyMediaExport(null, backImage);
    }
    
    /*
     * Verify that Card with an image on the front and simple text on
     * back can be exported to Anki correctly.
     */
    @Test
    public void testExportFrontImageBackText() {
        AnkiFormat af = new AnkiFormat();
        String frontImage = "img0.png";
        
        verifyTextExport(c3, af.createImgTag(frontImage), textSide2);
        verifyMediaExport(frontImage, null);
    }
    
    /*
     * Verify that Card with images on the front and
     * back can be exported to Anki correctly.
     */
    @Test
    public void testExportFrontImageBackImage() {
        AnkiFormat af = new AnkiFormat();
        String frontImage = "img0.png"; 
        String backImage = "img1.jpg";
        
        verifyTextExport(c4, af.createImgTag(frontImage), 
                        af.createImgTag(backImage));
        verifyMediaExport(frontImage, backImage);
    }
    
    @Test
    public void testSimpleQAImport() throws IOException {
    	String filePath = IMPORT_DIR + "simple_anki_import.txt";
    	try {
    		extFormat.importCards(filePath, ds, TEST_DECK_NAME);
    	} catch (Exception e) {
    		fail();
    	}
    	int numAdded = ds.getCardCount();
    	assertEquals(numAdded, 3);
    }
    
    @Test
    public void testQATagsImport() {
    	String filePath = IMPORT_DIR + "simple_anki_import.txt";
    	try {
    		extFormat.importCards(filePath, ds, TEST_DECK_NAME);
    	} catch (Exception e) {
    		fail();
    	}
    	List<String> tagResult = ds.getTags(TEST_DECK_NAME);
    	assertTrue(tagResult.contains("tag1"));
    	assertTrue(tagResult.contains("tag2"));
    	assertTrue(tagResult.contains("tag3"));
    }
    
    @Test
    public void testImgImport() {
    	String filePath = IMPORT_DIR + "anki_img.txt";
    	try { 
    		extFormat.importCards(filePath, ds, TEST_DECK_NAME);
    	} catch (Exception e) {
    		fail();
    	}
    	int cardsAdded = ds.getCardCount();
    	assertEquals(1, cardsAdded);
    }
    
}
