package external_modules;

import static org.junit.Assert.assertEquals;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import model.Card;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.junit.Before;
import org.junit.Test;

/**
 * Used to test exporting cards using PDFFormat to PDF
 * @author Adrian
 *
 */
public class PDFFormatTest extends ExternalFormatBase{
    
    public static final String FILE_LOC = "testPDF.pdf";
    public static final String FILE_LOC2 = "testPDF2.pdf";

    @Override
    @Before
    public void setup() {
        File f = new File("Note2Flash.pdf");
        f.delete();
    }
    
    /**
     * Try to generate 3 pages of cards with image and text, visual checking
     * required. This test will check for correct number of PDF pages
     * 
     * @throws IOException
     */
    @Test
    public void generate3Pages() throws IOException {
        PDFFormat pdfformat = new PDFFormat();
        List <Card> lc = new LinkedList<Card>();
        BufferedImage cat = ImageIO.read(new File("test/img/cat.jpg"));
        BufferedImage rabbit = ImageIO.read(new File("test/img/rabbit.jpg"));
        BufferedImage snowman = ImageIO.read(new File("test/img/snowman.jpg"));
        BufferedImage sun = ImageIO.read(new File("test/img/sun.jpg"));
        
        Set <String> tags = new HashSet<String>();
        
        tags.add("animal");
        
        Card c1 = new Card(cat, "jpg", "cat", tags);
        Card c2 = new Card(rabbit, "jpg", "rabbit", tags);
        tags.clear();
        tags.add("weather");
        Card c3 = new Card(snowman, "jpg", "snowman", tags);
        Card c4 = new Card(sun, "jpg", "sun", tags);
        Card c5 = new Card(sun, "jpg", "sunny", tags);
        Card c6 = new Card(snowman, "jpg", 
                "I do not like the sun because it is too warm, "
                + "I need to say something long"
                + " so I know word wrap in working", tags);
        tags.clear();
        tags.add("animal");
        Card c7 = new Card(cat, "jpg", "Some one needs to pet me", tags);
        Card c8 = new Card(rabbit, "jpg", "give me a carrot", tags);
        tags.clear();
        tags.add("cse");
        Card c9 = new Card("hello", "world", tags);
        
        lc.add(c1);
        lc.add(c2);
        lc.add(c3);
        lc.add(c4);
        lc.add(c5);
        lc.add(c6);
        lc.add(c7);
        lc.add(c8);
        lc.add(c9);
        
        pdfformat.exportCards(".", lc);
        PDDocument doc = PDDocument.load(new File("Note2Flash.pdf"));
        int count = doc.getNumberOfPages();
        //check we have the correct number of pages
        assertEquals(3, count);
    }
    
    /**
     * Attempt to generate printable cards
     * 
     * @throws IOException 
     */
    @Test
    public void generatePDF2TestCard() throws IOException {
        BufferedImage bf;
        Set <String> tags;
        
        bf = ImageIO.read(new File("test/img/sun.jpg"));
        tags = new HashSet<String>();
        tags.add("weather");
        Card c1 = new Card(bf, "jpg", "sun", tags);
        
        bf = ImageIO.read(new File("test/img/snowman.jpg"));
        tags.clear();
        tags.add("snow");
        Card c2 = new Card(bf, "jpg", "snowman", tags);
        
        List <Card> cards = new LinkedList<Card>();
        cards.add(c1);
        cards.add(c2);
        PDFFormat pdfformat = new PDFFormat();
        pdfformat.exportCards(".", cards);
        
        PDDocument doc = PDDocument.load("Note2Flash.pdf");
        int count = doc.getNumberOfPages();
        assertEquals(1, count);
    }
    
    /**
     * Make sure a exception thrown when a card that has text with no spaces
     * is created does not recur
     * 
     * Cards with a lot of text must NOT overflow out of the border
     * 
     * @throws IOException
     */
    @Test
    public void generateLongCard() throws IOException {
        List <Card> cards = new LinkedList<Card>();
        Card c1 = new Card("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", new HashSet<String>());
        
        Card c2 = new Card ("testing testing", "testing testing", new HashSet<String>());
        
        Card c3 = new Card ("lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsumlourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum",
                "lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum "
                + "lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum lourem ipsum",
                new HashSet<String>());
        
        cards.add(c1);
        cards.add(c2);
        cards.add(c3);
        
        PDFFormat pdfformat = new PDFFormat();
        pdfformat.exportCards(".", cards);
        
        PDDocument doc = PDDocument.load("Note2Flash.pdf");
        int count = doc.getNumberOfPages();
        assertEquals(1, count);
    }
}