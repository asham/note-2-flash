package external_modules;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;

import model.Card;
import model.DataStorage;

import org.junit.Before;

/**
 * Acts as the base class for classes which implement
 * ExternalFormat.
 * @author Mitchell
 *
 */
public abstract class ExternalFormatBase {
    protected ExternalFormat extFormat;
    protected DataStorage ds;
    
    protected static String textSide1;
    protected static String textSide2;
    protected static String textSide3;
    protected static String textSide4;
    protected static BufferedImage imageSide1;
    protected static BufferedImage imageSide2;
    
    protected static String tag1;
    protected static String tag2;
    protected static String tag3; 
    
    protected static Card c1;
    protected static Card c2;
    protected static Card c3;
    protected static Card c4;
    protected static Card c5;
    protected static Card c6;
    
    /**
     * Initialize the ExternalFormat test.
     */
    public ExternalFormatBase() {
        // images and strings are initialized once
        try {
            imageSide1 = ImageIO.read(new File("test/model/data/snow_man.png"));
            imageSide2 = ImageIO.read(new File("test/model/data/uw_logo.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }           
        
        textSide1 = "Front card text.";
        textSide2 = "Back card text.";   
        textSide3 = "There is a newline after this scentence.\nThere are two more after this scentence.\n\n";
        textSide4 = "\nA newline characters at the beginnning of this line,\nand after the comma.";
        tag1 = "Math";
        tag2 = "Science";
        tag3 = "Geography";         
        
        Set<String> tags = new HashSet<String>(Arrays.asList(tag1, tag2, tag3)); 
        c1 = new Card(textSide1, textSide2, tags);
        c2 = new Card(textSide1, imageSide2, "jpg", tags);
        c3 = new Card(imageSide1, "png", textSide2, tags);
        c4 = new Card(imageSide1, "png", imageSide2, "jpg", tags);
        c5 = new Card(textSide3, textSide4, tags);
        c6 = new Card(textSide1, textSide2, new HashSet<String>());
     }
    
    @Before
    public abstract void setup();

}
