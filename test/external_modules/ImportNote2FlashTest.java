package external_modules;

import static org.junit.Assert.*;

import java.util.List;

import model.Card;
import model.Deck;
import model.SideReaderVerify;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Verify import from note2flash format works correctly
 * @author tianchi
 */
public class ImportNote2FlashTest {
	
    @After
    public void cleanUp() {
        Deck.removeAllStoredDecks();
    }
    
    @BeforeClass
    public static void Clear() {
        Deck.removeAllStoredDecks();
    }
    
    @Test
    public void EmptyDeckTest() {
    	String filePath = "./test/external_modules/testDatabase1";
    	
    	ExternalFormat format = new Note2FlashFormat();
    	try {
    		format.importCards(filePath, null, "newDeck");
    	} catch (Exception e) {
    		fail();
    	}
    	Deck d = Deck.getStoredDeck("newDeck");
    	assertEquals(0, d.getNumCards());
    }
    
    @Test
    public void OneCardDeckTest() {
    	String filePath = "./test/external_modules/testDatabase2";
    	
    	ExternalFormat format = new Note2FlashFormat();
    	try {
    		format.importCards(filePath, null, "newDeck");
    	} catch (Exception e) {
    		fail();
    	}
    	Deck d = Deck.getStoredDeck("newDeck");
    	assertEquals(1, d.getNumCards());
    	List<Card> cards = d.getCards();
    	
    	SideReaderVerify verify = new SideReaderVerify(null, "front_1", null, 
    			"back_1", null, null, false, false);
    	Card c = cards.get(0);
    	c.readFront(verify);
    	c.readBack(verify);
    }
    
    @Test
    public void MultiCardDeckTest() {
    	String filePath = "./test/external_modules/testDatabase3";
    	
    	ExternalFormat format = new Note2FlashFormat();
    	try {
    		format.importCards(filePath, null, "newDeck");
    	} catch (Exception e) {
    		fail();
    	}
    	Deck d = Deck.getStoredDeck("newDeck");
    	assertEquals(5, d.getNumCards());	
    }
    
    @Test
    public void MultiCardMultiDeckTest() {
    	String filePath = "./test/external_modules/testDatabase4";
    	
    	ExternalFormat format = new Note2FlashFormat();
    	try {
    		format.importCards(filePath, null, "newDeck");
    	} catch (Exception e) {
    		fail();
    	}
    	Deck d = Deck.getStoredDeck("newDeck");
    	assertEquals(7, d.getNumCards());	
    }
}
