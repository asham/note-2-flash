package system;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import model.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import controller.Controller;

/**
 * Integration tests for Note2Flash
 * @author wmgannon
 *
 */
public class SystemTest {
	
	private static DataStorage dataStorage = QueryProcessor.getInstance();
	private static Controller controller;
	private List<String> decks;
	
	@Before
	public void clearDB(){
	    Deck.removeAllStoredDecks();
	    controller.addDeckButtonPressed("Default");
	}
	
	@BeforeClass
	public static void initController(){
		// instantiate controller to test (in debug mode)
	    controller = new Controller(true);
	}
	
	@Test
	public void testInitialDecks(){
		// database should have the default deck initially 
		decks = dataStorage.getDecks();
		assertEquals(decks.size(), 1);
	}
	
	@Test
	public void testAddDeck(){
	    testInitialDecks();
		// try adding a deck
		controller.addDeckButtonPressed("Math");
		// verify deck was added
		decks = dataStorage.getDecks();
		assertEquals(decks.size(), 2);
		System.out.println(decks.get(0));
		assertTrue(decks.contains("Math"));
		// verify deck is initially empty
		Deck mathDeck = dataStorage.getDeck("Math");
		assertEquals(mathDeck.getNumCards(), 0);
	}
	
	@Test
	public void testAddCard(){
	    String frontString = "testFront";
	    String backString = "testBack";
	    
	    // add a deck
	    controller.addDeckButtonPressed("Math");
	    
	    // check we have default and new deck
	    decks = dataStorage.getDecks();
        assertEquals(decks.size(), 2);
        assertTrue(decks.contains("Math"));
		// try adding a card to the new deck
		controller.deckSelected("Math");
		controller.addCardButtonPressed(frontString, null, null, backString, null, null, "");
		// get the deck from DB
		Deck mathDeck = dataStorage.getDeck("Math");
		// verify card was added
		assertEquals(mathDeck.getNumCards(), 1);
		List<Card> cards = mathDeck.getCards();
		assertEquals(cards.size(), 1);
		Card card = cards.get(0);
		assertTrue(card != null);
		// verify the card that was added has the content we tried to add
		SideReaderVerify reader = new SideReaderVerify(null, frontString, null, backString, null, null, false, false);
		card.readFront(reader);
		card.readBack(reader);
	}
	
	@Test
	public void testEditCard(){
	    testAddCard();
		// try editing a card
		Deck mathDeck = dataStorage.getDeck("Math");
		List<Card> cards = mathDeck.getCards();
		Card card = cards.get(0);
		controller.editCardCallback(card, "editFront", null, null, "editBack", null, null, "");
		// verify the card was edited
		SideReaderVerify reader = new SideReaderVerify(null, "editFront", null, "editBack", null, null, false, false);
		card.readFront(reader);
		card.readBack(reader);
	}
	
	@Test
	public void testRemoveCard(){
	    testAddCard();
		// try removing the card we just added
		controller.removeCardButtonPressed();
		Deck mathDeck = dataStorage.getDeck("Math");
		// verify it was removed
		assertEquals(mathDeck.getNumCards(), 0);
	}
    
	
	@Test
	public void testDeleteDeck(){
	    
	    List<String> decks = dataStorage.getDecks();
        assertEquals(decks.size(), 1);
        
	    controller.addDeckButtonPressed("Math");
	    controller.addDeckButtonPressed("Science");
	    
	    decks = dataStorage.getDecks();
        assertEquals(decks.size(), 3);
        assertTrue(decks.contains("Math"));
        
		// try deleting the deck we created
		controller.deckSelected("Math");
		controller.deleteDeckButtonPressed();
		// verify it was deleted
		decks = dataStorage.getDecks();
		assertEquals(decks.size(), 2);
		assertTrue(!decks.contains("Math"));
	}
	
	/**
	 * Test that having more front images than back images causes an
	 * exception.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testAddMultipleCardsNotSameSize(){
	    
	    List<BufferedImage> front = new ArrayList<BufferedImage>();
	    List<BufferedImage> back = new ArrayList<BufferedImage>();
	    
	    File img = new File("test/model/data/snow_man.png");
	    
	    BufferedImage testImage;
	    
        try {
            testImage = ImageIO.read(img);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        }
	    
        // add more to front
	    front.add(testImage);
	    front.add(testImage);
	    
	    back.add(testImage);
	    controller.createMultipleCardsButtonPressed(front, back, "png");
	}
}
