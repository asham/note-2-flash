package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;


/**
 * Unit tests for the Card class.
 * @author Mitchell
 */
public class CardTest {
    private static final String TAG1 = "Math";
    private static final String TAG2 = "Science";
    private static final String TAG3 = "Geography";    
    private static final String TAG4 = "foo";
    private static final String TAG5 = "bar";
    private static final String TAG6 = "biz";
    private static final int NUM_TAGS = 3;
    
    private static String textSide1;
    private static String textSide2;
    private static BufferedImage imageSide1;
    private static BufferedImage imageSide2;
    private static Set<String> tags;
    
    private static Card c1a;
    private static Card c1b;
    private static Card c1c;
    
    private static Card c2a;
    private static Card c2b;
    private static Card c2c;
    
    private static Card c3a;
    private static Card c3b;
    private static Card c3c;
    
    private static Card c4a;
    private static Card c4b;
    private static Card c4c;
    
    // used to verify that cards contain/return correct content
    private final SideReaderVerify sr1;
    private final SideReaderVerify sr2;
    private final SideReaderVerify sr3;
    private final SideReaderVerify sr4;   
   
    /**
     * Initializes test fields.
     */
    @Before
    public void initCards() {
        // reset the database
        DataStorage dataStorage = QueryProcessor.getInstance();
        dataStorage.clearData();
        
        /*
         * NOTE: multiple instantiations of the same card are created
         * in order to test out the equals and hashCode methods
         */
        c1a = new Card(textSide1, textSide2, tags);
        c1b = new Card(textSide1, textSide2, tags);
        c1c = new Card(textSide1, textSide2, tags);
        
        c2a = new Card(textSide1, imageSide2, "jpg", tags);
        c2b = new Card(textSide1, imageSide2, "jpg", tags);
        c2c = new Card(textSide1, imageSide2, "jpg", tags);
        
        c3a = new Card(imageSide1, "png", textSide2, tags);
        c3b = new Card(imageSide1, "png", textSide2, tags);
        c3c = new Card(imageSide1, "png", textSide2, tags);      
        
        c4a = new Card(imageSide1, "png", imageSide2, "jpg", tags);
        c4b = new Card(imageSide1, "png", imageSide2, "jpg", tags);
        c4c = new Card(imageSide1, "png", imageSide2, "jpg", tags);
    }
    
    /**
     * Initializes the CardTest.
     */
    public CardTest() {
        // images and strings are initialized once
        try {
            imageSide1 = ImageIO.read(new File("test/model/data/snow_man.png"));
            imageSide2 = ImageIO.read(new File("test/model/data/uw_logo.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        tags = new HashSet<String>(Arrays.asList(TAG1, TAG2, TAG3));        
        textSide1 = "Front card text.";
        textSide2 = "Back card text.";
        
        // SideReaderTest is used to verify that Cards contain correct content
        sr1 = new SideReaderVerify(null, textSide1, null, textSide2, 
                                    null, null, false, false);
        sr2 = new SideReaderVerify(null, textSide1, imageSide2, null,
                                    null, "jpg", false, true);
        sr3 = new SideReaderVerify(imageSide1, null, null, textSide2, 
                                    "png", null, true, false);
        sr4 = new SideReaderVerify(imageSide1, null, imageSide2, null, 
                                    "png", "jpg", true, true);           
     }

    /*
     * Verifies that getTags is implemented correctly.
     */
    @Test
    public void testGetTags() {
        // All Cards in the unit test class initially have the
        // same set of tags, so if getTags works with c1 then 
        // it also works with c2, c3, and c4.
        Set<String> storedTags = c1a.getTags();
        
        // verify correct return value
        assertEquals(storedTags.size(), NUM_TAGS);        
        assertTrue(storedTags.contains(TAG1));
        assertTrue(storedTags.contains(TAG2));
        assertTrue(storedTags.contains(TAG3));
    }
    
    /*
     * Verifies that setTags is implemented correctly.
     */
    @Test
    public void testSetTags() {
        // NOTE: requires getTags to work correctly
        
        Set<String> newTags 
            = new HashSet<String>(Arrays.asList(TAG4, TAG5, TAG6));

        
        // All Cards in the unit test class initially have the
        // same set of tags, so if setTags works with c1 then 
        // it also works with c2, c3, and c4.
        c1a.setTags(newTags);        
        Set<String> storedTags = c1a.getTags();
        
        // verify correct return value
        assertEquals(storedTags.size(), NUM_TAGS);        
        assertTrue(storedTags.contains(TAG4));
        assertTrue(storedTags.contains(TAG5));
        assertTrue(storedTags.contains(TAG6));
    }
    
    /*
     * Verifies that setID is implemented correctly.
     */
    @Test
    public void testSetID() {
        // NOTE: requires getID to work correctly
        int idNum = 5;
        c1a.setID(idNum);
        
        // verify correct return value
        assertEquals(c1a.getID(), idNum);
    }
    
    /*
     * Verifies that readFront is implemented correctly.
     */
    @Test
    public void testReadFront() {
        // NOTE: relies on SideReaderTest to work correctly
        c1a.readFront(sr1);
        c2a.readFront(sr2);
        c3a.readFront(sr3);
        c4a.readFront(sr4);
    }
    
    /*
     * Verifies that readBack is implemented correctly.
     */
    @Test
    public void testReadBack() {
        // NOTE: relies on SideReaderTest to work correctly
        c1a.readBack(sr1);
        c2a.readBack(sr2);
        c3a.readBack(sr3);
        c4a.readBack(sr4);
    }
    
    /*
     * Verifies that setFront is implemented correctly.
     */
    @Test
    public void testSetFront() {
        // NOTE: relies on readFront, SideReaderTest to work correctly
        
        // This test changes the content on the front of these card and verifies
        // that the changes have been after reading from the front of them.
        c1a.setFront(imageSide1, "png");
        c1a.readFront(sr3);
        
        c3a.setFront(textSide1);
        c3a.readFront(sr1);
    }
    
    /*
     * Verifies that setBack is implemented correctly.
     */
    @Test
    public void testSetBack() {
        // NOTE: relies on readBack, SideReaderTest to work correctly
        
        // This test changes the content on the back of these card and verifies
        // that the changes have been after reading from the back of them.
        c1a.setBack(imageSide2, "jpg");
        c1a.readBack(sr2);
        
        c2a.setBack(textSide2);
        c2a.readBack(sr1);
    }
    
    /*
     * Verifies that equals is implemented correctly.
     */
    @Test
    public void testEquals() {
        // check that equals relationships hold true
        // for every type of card
        verifyEqual(c1a, c1b, c1c);
        verifyEqual(c2a, c2b, c2c);
        verifyEqual(c3a, c3b, c3c);
        verifyEqual(c4a, c4b, c4c);   
        
        // sanity check
        assertFalse(c1a.equals(c2a));
        assertFalse(c3a.equals(c4a));
        assertFalse(c1a.equals(c4a));
    }
    
    /*
     * Verifies equals relationships are true. 
     */
    public void verifyEqual(Card card1, Card card2, Card card3) {
        // verify equals is reflexive
        assertTrue(card1.equals(card1));
        
        // verify that equals is symmetric
        assertTrue(card1.equals(card2));
        assertTrue(card2.equals(card1));
        
        // verify that equals is transitive
        assertTrue(card1.equals(card2));
        assertTrue(card2.equals(card3));
        assertTrue(card1.equals(card3));
    }
    
    /*
     * Verifies that hashCode is implemented correctly.
     */
    @Test
    public void testHashCode() {
        assertTrue(c1a.hashCode() == c1b.hashCode());
        assertTrue(c2a.hashCode() == c2b.hashCode());
        assertTrue(c3a.hashCode() == c3b.hashCode());
        assertTrue(c4a.hashCode() == c4b.hashCode());
    }
}
