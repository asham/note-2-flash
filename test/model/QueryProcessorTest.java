package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Used to test a QueryProcessor instance of DataStorage
 * @author Adrian
 *
 */
public class QueryProcessorTest extends DataStorageBase {
    
    
    /**
     * Setup and clear out db before each test
     */
    @Override
    @Before
    public void setup() {
        ds.clearData();
    }
    
    @BeforeClass
    public static void setupTestDB() {
        ds = QueryProcessor.getInstance();
        ds.changeSaveLocation(QueryProcessor.DB_TEST_DEFAULT_LOC);
        ds.clearData();
    }
    
    /**
     * Check file location is correct
     */
    @Test
    public void checkDbFileLocation() {
        QueryProcessor db = QueryProcessor.getInstance();
        assertEquals(QueryProcessor.DB_TEST_DEFAULT_LOC, db.getFileLocation());
    }
    
    /**
     * Test delete card from database
     */
    @Test
    public void testDeleteCard() {
        assertEquals(0, ((QueryProcessor)ds).getTotalNumOfCards());
        Set <String> tags = new HashSet<String>();
        tags.add("test1");
        tags.add("test2");
        Card c = new Card("testFront", "testBack", tags);
        assertEquals(1, ((QueryProcessor)ds).getTotalNumOfCards());
        ((QueryProcessor) ds).deleteCard(c);
        assertEquals(0, ((QueryProcessor)ds).getTotalNumOfCards());
    }
    
    /**
     * Test getting a card from database
     */
    @Test
    public void testCreateAndGetCard() {
        Set <String> tags = new HashSet<String>();
        tags.add("hello");
        tags.add("world");
        BufferedImage imgFront = null;
        try {
            imgFront = ImageIO.read(new File("test/model/data/snow_man.png"));
        } catch (IOException e) {
            
        }
        Card c = new Card(imgFront, "png", "test234", tags);
        Card actual = ((QueryProcessor)ds).getCard(c.getID());
        assertEquals("test234", actual.textBack);
    }
    
    /**
     * Test creating a card with text
     */
    @Test
    public void testTextCard() {
        Set <String> tags = new HashSet<String>();
        tags.add("hello");
        tags.add("world");
        Card c = new Card("test123", "test234", tags);
        Card actual = ((QueryProcessor)ds).getCard(c.getID());
        assertEquals(c.textFront, actual.textFront);
        assertEquals(c.textBack, actual.textBack);
    }
    
    /**
     * When passed two null arguments, should throw illegal argument exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void removeCardsFromDeckPassedTwoNull() {
        ds.removeCardsFromDeck(null, null);
    }
    
    /**
     * When passed one null argument, should throw illegal argument exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void removeCardsFromDeckPassedOneNull() {
        ds.removeCardsFromDeck(new Deck("testDeck"), null);
    }
    
    /**
     * Should throw exception if location string passed is null
     */
    @Test(expected = IllegalArgumentException.class)
    public void changeSaveLocationPassedNull() {
        ds.changeSaveLocation(null);
    }
    
    @Test
    public void newCardWithNullTag() {
        Card c = new Card("test", "test", null);
        List <Card> cards = new LinkedList<Card>();
        cards.add(c);
        Deck d = new Deck("testDeck");
        d.addCards(cards);
        Deck actual = ds.getDeck("testDeck");
        List<Card> actualCard = actual.getCards();
        assertTrue(actualCard.contains(c));
    }
}