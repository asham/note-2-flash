package model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.*;

/**
 * Unit tests for the Card class.
 * @author Tianchi
 */
public class DeckTest {
    
    // Cards to be used later
    private Card card1;
    private Card card2;
    private Card card3;
    private Card card4;
    private Card card5;
    private Card card6;
    private Card card7;


    @After
    public void cleanUp() {
        Deck.removeAllStoredDecks();
    }

    /**
     * Set up cards to be used
     */
    @Before
    public void setUp() {
        card1 = new Card("front1", "back", new HashSet<String>());
        card2 = new Card("front2", "back", new HashSet<String>(Arrays.asList("tag1")));
        card3 = new Card("front3", "back", new HashSet<String>(Arrays.asList("tag2")));
        card4 = new Card("front4", "back", new HashSet<String>(Arrays.asList("tag1", "tag2")));
        card5 = new Card("front5", "back", new HashSet<String>(Arrays.asList("tag2", "tag3")));
        card6 = new Card("front6", "back", new HashSet<String>(Arrays.asList("tag1", "tag2", "tag3")));
        card7 = new Card("front7", "back", new HashSet<String>(Arrays.asList("tag1", "tag2", "tag3", "tag4")));
    }

    /**
     * Test getDeckName
     */
    @Test
    public void testGetDeckName() {
    	System.out.println("< ------------ test 1 ---------------->");
        Deck d1 = new Deck("Deck1");
        Deck d2 = new Deck("Deck2");
        Deck d3 = new Deck("Deck3");
        Deck d4 = new Deck("Deck4");

        assertEquals("Deck1", d1.getDeckName());
        assertEquals("Deck2", d2.getDeckName());
        assertEquals("Deck3", d3.getDeckName());
        assertEquals("Deck4", d4.getDeckName());
    }
    
    /**
     * Test getNumcards
     */
    @Test
    public void testGetNumCards() {
    	System.out.println("< ------------ test 2 ---------------->");
        // Case: 4 cards
        Deck d1 = new Deck("Deck1");
        List<Card> addList1 = new ArrayList<Card>();
        addList1.add(card1);
        addList1.add(card2);
        addList1.add(card3);
        addList1.add(card4);
        d1.addCards(addList1);
        assertEquals(4, d1.getNumCards());
        
        // Case: 0 cards
        Deck d2 = new Deck("Deck2");
        assertEquals(0, d2.getNumCards());
        
        // Case: 1 cards
        Deck d3 = new Deck("Deck3");
        List<Card> addList3 = new ArrayList<Card>();
        addList3.add(card1);
        d3.addCards(addList3);
        assertEquals(1, d3.getNumCards());
        
        // Case 3 cards
        Deck d4 = new Deck("Deck4");
        List<Card> addList4 = new ArrayList<Card>();
        addList4.add(card1);
        addList4.add(card2);
        addList4.add(card3);
        d4.addCards(addList4);
        assertEquals(3, d4.getNumCards());
    }
    
    /**
     * Test remove cards
     */
    @Test
    public void testRemoveCards() {
    	System.out.println("< ------------ test 3 ---------------->");
        // Case: nothing to remove
        Deck d1 = new Deck("Deck1");
        List<Card> addList = new ArrayList<Card>();
        addList.add(card1);
        addList.add(card2);
        addList.add(card3);
        addList.add(card4);
        addList.add(card5);
        addList.add(card6);
        addList.add(card7);
        d1.addCards(addList);
        
        List<Card> removeList1 = new ArrayList<Card>();
        d1.removeCards(removeList1);
        assertEquals(7, d1.getNumCards());
        
        // Case: remove 1 cards
        Deck d2 = new Deck("Deck2");
        d2.addCards(addList);
        
        List<Card> removeList2 = new ArrayList<Card>();
        removeList2.add(card2);
        removeList2.add(card3);
        d2.removeCards(removeList2);
        assertEquals(5, d2.getNumCards());
    }
    
    /**
     * Test get cards
     */
    @Test
    public void testGetCards() {
    	System.out.println("< ------------ test 4 ---------------->");
        // Case: no cards
        Deck d1 = new Deck("Deck1");
        d1.addCards(new ArrayList<Card>());
        List<Card> res1 = d1.getCards();
        assertEquals(0, res1.size());
        
        // Case: 1 card
        Deck d2 = new Deck("Deck2");
        List<Card> addList2 = new ArrayList<Card>();
        addList2.add(card1);
        d2.addCards(addList2);
        List<Card> res2 = d2.getCards();
        assertEquals(1, res2.size());
        
        // Case: 4 cards
        Deck d3 = new Deck("Deck3");
        List<Card> addList3 = new ArrayList<Card>();
        addList3.add(card1);
        addList3.add(card2);
        addList3.add(card3);
        addList3.add(card4);
        d3.addCards(addList3);
        List<Card> res3 = d3.getCards();
        assertEquals(4, res3.size());
        
        // Case: 7 cards
        Deck d4 = new Deck("Deck4");
        List<Card> addList4 = Arrays.asList(card1, card2, card3,
                card4, card5, card6, card7);
        d4.addCards(addList4);
        List<Card> res4 = d4.getCards();
        assertEquals(7, res4.size());
    }
    
    /**
     * Test get cards by tag
     */
    @Test
    public void testGetCardsByTag() {
    	System.out.println("< ------------ test 5 ---------------->");
        // Create a new deck
        Deck d1 = new Deck("Deck1");
        List<Card> addList = Arrays.asList(card1, card2, card3,
                card4, card5, card6, card7);
        d1.addCards(addList);
        
        // Case: empty tag list
        Set<String> tagList1 = new HashSet<String>();
        List<Card> res1 = d1.getCardsByTag(tagList1);
        assertEquals(7, res1.size());
        
        // Case: tag1
        Set<String> tagList2 = new HashSet<String>(Arrays.asList("tag1"));
        List<Card> res2 = d1.getCardsByTag(tagList2);
        assertEquals(4, res2.size());
        
        // Case: tag1, tag2
        Set<String> tagList3 = new HashSet<String>(Arrays.asList("tag1", "tag2"));
        List<Card> res3 = d1.getCardsByTag(tagList3);
        assertEquals(3, res3.size());
        
        // Case: tag3
        Set<String> tagList4 = new HashSet<String>(Arrays.asList("tag3"));
        List<Card> res4 = d1.getCardsByTag(tagList4);
        assertEquals(3, res4.size());
        
        // Case: tag0, which does not exist
        Set<String> tagList5 = new HashSet<String>(Arrays.asList("tag0"));
        List<Card> res5 = d1.getCardsByTag(tagList5);
        assertEquals(0, res5.size());
        
        // Case: tag1, tag2, tag3
        Set<String> tagList6 = new HashSet<String>(Arrays.asList("tag1", "tag2", "tag3"));
        List<Card> res6 = d1.getCardsByTag(tagList6);
        assertEquals(2, res6.size());
        
        // Case: tag1, tag2, tag3, tag4
        Set<String> tagList7 = new HashSet<String>(Arrays.asList("tag1", "tag2", "tag3", "tag4"));
        List<Card> res7 = d1.getCardsByTag(tagList7);
        assertEquals(1, res7.size());
    }
    
    /**
     * Test getStoredDeck
     */
    @Test
    public void testGetStoredDeck() {
    	System.out.println("< ------------ test 6 ---------------->");

        // Case: empty deck
        new Deck("Deck1");
        Deck res1 = Deck.getStoredDeck("Deck1");
        assertEquals(0, res1.getCards().size());


        // Case: deck with 1 card
        Deck deck2 = new Deck("Deck222");
        List<Card> addList2 = Arrays.asList(card5);
        deck2.addCards(addList2);
        Deck res2 = Deck.getStoredDeck("Deck222");
        assertEquals(1, res2.getCards().size());

        // Case: deck does not exist
        Deck res3 = Deck.getStoredDeck("UnknownDeck");
        assertNull(res3);
        
        // Case: deck with 7 cards
        Deck deck4 = new Deck("sevenCardsDeck");
        List<Card> addList4 = Arrays.asList(card1, card2, 
                card3, card4, card5, card6, card7);
        deck4.addCards(addList4);
        Deck res4 = Deck.getStoredDeck("sevenCardsDeck");
        assertEquals(7, res4.getCards().size());
    }
    
    /**
     * Test removeStoredDeck
     */
    @Test
    public void testRemoveStoredDeck() {
    	System.out.println("< ------------ test 7 ---------------->");
        Deck d1 = new Deck("newDeck");
        Deck res1 = Deck.getStoredDeck("newDeck");
        assertEquals(0, res1.getCards().size());
        
        d1.remove();
        Deck res2 = Deck.getStoredDeck("newDeck");
        assertNull(res2);
    }

    /**
     * Test unique deck name
     */
    @Test
    public void testUniqueDeckName() {
    	System.out.println("< ------------ test 8 ---------------->");
        boolean thrown = false;
        new Deck("deck1");
        try {
            new Deck("deck1");
        }
        catch (IllegalArgumentException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }
}