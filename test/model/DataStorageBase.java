package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

/**
 * This is the base test for classes that implement DataStorage
 * 
 * This class should be run directly, instead should be extended by subclass
 * 
 * @author Adrian
 *
 */
public abstract class DataStorageBase {
    public static final String CHANGE_DB = "./testdb";
    
    protected static DataStorage ds;
    
    @Before
    public abstract void setup();
    
    /**
     * Add a deck to model, check added
     */
    @Test
    public void addDeck() {
        ds.newDeck("testDeck");
        assertTrue(ds.hasDeck("testDeck"));
    }
    
    /**
     * add cards to a deck in model, check added
     */
    @Test
    public void addCardsToDeck() {
        Deck.removeAllStoredDecks();
        Deck d = new Deck("testDeck");
        assertTrue(ds.hasDeck("testDeck"));
        
        List <Card> cards = new LinkedList<Card>();
        Set <String> tags = new HashSet<String>();
        tags.add("test1");
        tags.add("test2");
        Card c = new Card("testFront", "testBack", tags);
        cards.add(c);
        
        ds.addCardsToDeck(d, cards);
        // check db is correct
        d = ds.getDeck("testDeck");
        List <Card> actualCards = d.getCards();
        assertEquals(c.getID(), actualCards.get(0).getID());
    }
    
    /**
     * try removing cards from a deck
     */
    @Test
    public void testRemoveCardFromDeck() {
        Deck.removeAllStoredDecks();
        Deck d = new Deck("testDeck");
        assertTrue(ds.hasDeck("testDeck"));
        
        List <Card> cards = new LinkedList<Card>();
        Set <String> tags = new HashSet<String>();
        tags.add("test1");
        tags.add("test2");
        Card c = new Card("testFront", "testBack", tags);
        cards.add(c);
        
        ds.addCardsToDeck(d, cards);
        // check db is correct
        d = ds.getDeck("testDeck");
        List <Card> actualCards = d.getCards();
        assertEquals(c.getID(), actualCards.get(0).getID());
        
        ds.removeCardsFromDeck(d, cards);
        d = ds.getDeck("testDeck");
        assertEquals(new LinkedList<Card>(), d.getCards());
    }
    
    /**
     * try deleting a deck
     */
    @Test
    public void deleteDeck() {
        if (!ds.hasDeck("testDeck")) {
            ds.newDeck("testDeck");
        }
        ds.deleteDeck("testDeck");
        assertFalse(ds.hasDeck("testDeck"));
    }
    
    /**
     * try changing the contents of a card in model
     */
    @Test
    public void changeCardContent() {
        Set <String> tags = new HashSet<String>();
        tags.add("hello");
        tags.add("world");
        Card c = new Card("test123", "test234", tags);
        
        c.setBack("nottesting123");
        assertEquals("nottesting123", c.textBack);
    }
    
    /**
     * try adding a deck
     */
    @Test
    public void testAddDeck() {
        ds.newDeck("testing123");
        
        List <String> actual = ds.getDecks();
        assertTrue(actual.contains("testing123"));
    }
    
    /**
     * Try grabbing a deck from model
     */
    @Test
    public void testgetDecks() {
        List <String> actual = ds.getDecks();
        assertEquals(0, actual.size());
        
        ds.newDeck("testing235");
        actual = ds.getDecks();
        assertTrue(actual.contains("testing235"));
        assertFalse(actual.contains("testing999"));
    }
    
    /**
     * Try grabbing multiple decks from model
     */
    @Test
    public void testGetDecksMultiple() {
        List <String> actual = ds.getDecks();
        assertEquals(0, actual.size());
        
        ds.newDeck("testing123");
        ds.newDeck("testing345");
        actual = ds.getDecks();
        assertTrue(actual.contains("testing123"));
        assertTrue(actual.contains("testing123"));
    }
    
    /**
     * Try deleting decks
     */
    @Test
    public void testDeleteDeck() {
        assertFalse(ds.hasDeck("testing379"));
        ds.newDeck("testing379");
        assertTrue(ds.hasDeck("testing379"));
        ds.deleteDeck("testing379");
        assertFalse(ds.hasDeck("testing379"));
    }    
    
    /**
     * Must throw IllegalArgumentException when passed null
     */
    @Test (expected = IllegalArgumentException.class)
    public void testNullDeckName() {
        ds.newDeck(null);
    }
    
    @Test (expected = IllegalArgumentException.class) 
    public void addCardToDeckNll() {
        ds.addCardsToDeck(null, null);
    }
    
    @Test (expected = IllegalArgumentException.class)
    public void testNullCard() {
        ds.addCardsToDeck(new Deck("testdeck"), null);
    }
    
    @Test
    public void testGetTag() {
        Deck.removeAllStoredDecks();
        Deck d = new Deck("testDeck");
        assertTrue(ds.hasDeck("testDeck"));
        
        List <Card> cards = new LinkedList<Card>();
        Set <String> tags = new HashSet<String>();
        tags.add("test1");
        tags.add("test2");
        Card c = new Card("testFront", "testBack", tags);
        cards.add(c);
        
        tags.clear();
        tags.add("test3");
        tags.add("test2");
        c = new Card("testFront2", "testBack2", tags);
        cards.add(c);
        
        ds.addCardsToDeck(d, cards);
        List<String> actual = ds.getTags("testDeck");
        List<String> expected = new LinkedList<String>();
        expected.add("test1");
        expected.add("test2");
        expected.add("test3");
        //there should be 3 tags
        assertEquals(3, actual.size());
        //all tags present
        assertTrue(actual.containsAll(expected));
    }
    
    /**
     * Expect IllegalArgumentException if deck name does not exist
     */
    @Test (expected = IllegalArgumentException.class)
    public void testGetTagException() {
        ds.getTags("invalid");
    }
    
    @Test
    public void testChangeLocation() {
        ds.newDeck("testing123");
        
        List <String> actual = ds.getDecks();
        assertTrue(actual.contains("testing123"));
        // change location to test
        ds.changeSaveLocation(CHANGE_DB);
        actual = ds.getDecks();
        // check to see if deck still exists
        assertFalse(actual.contains("testing123"));
        //try adding something to this deck
        ds.newDeck("testing245");
        actual = ds.getDecks();
        assertTrue(actual.contains("testing245"));
        //clear out test db
        ds.clearData();
        ds.changeSaveLocation(QueryProcessor.DB_TEST_DEFAULT_LOC);
        actual = ds.getDecks();
        assertTrue(actual.contains("testing123"));
        //should not contain deck from another db
        assertFalse(actual.contains("testing245"));
        File testdb = new File(CHANGE_DB+".mv.db");
        File testdbtrace = new File (CHANGE_DB + ".trace.db");
        testdb.delete();
        testdbtrace.delete();
    }
    
    @Test
    public void testCardCount() {
        Deck.removeAllStoredDecks();
        new Deck("testDeck");
        assertTrue(ds.hasDeck("testDeck"));
        
        assertEquals(0, ds.getCardCount());
        List <Card> cards = new LinkedList<Card>();
        Set <String> tags = new HashSet<String>();
        tags.add("test1");
        tags.add("test2");
        Card c = new Card("testFront", "testBack", tags);
        cards.add(c);
        
        // check db is correct
        assertEquals(1, ds.getCardCount());
        new Card("testFront2", "testBack2", tags);
        assertEquals(2,ds.getCardCount());
        ds.clearData();
        
        assertEquals(0, ds.getCardCount());
    }
}