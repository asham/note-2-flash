package model;

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;

/**
 * Represents an immutable class which verifies that 
 * a Card contains/returns correct content on each side.
 * @author Mitchell
 *
 */
public class SideReaderVerify implements SideReader {
    private BufferedImage expectedFrontImage;
    private String expectedFrontText;
    private BufferedImage expectedBackImage;
    private String expectedBackText;
    private String frontFileExt;
    private String backFileExt;
    private boolean isFrontImage;
    private boolean isBackImage;
    
    /**
     * Contructs a new SideReaderTest.
     * @param expectedFrontImage Expected image on front of the card.
     * @param expectedFrontText Expected text on front of the card.
     * @param expectedBackImage Expected image on back of the card.
     * @param expectedBackText Expected text on back of the card.
     * @param isFrontImage Indicates if font of card is an image.
     * @param isBackImage Indicates if font of card is an image.
     * @param frontFileExt The expected file extension for the front image.
     * @param backFileExt The expected file extension for the back image.
     */
    public SideReaderVerify(BufferedImage expectedFrontImage, String expectedFrontText,
                          BufferedImage expectedBackImage, String expectedBackText,
                          String frontFileExt, String backFileExt,
                          boolean isFrontImage, boolean isBackImage) {
        this.expectedFrontImage = expectedFrontImage;
        this.expectedFrontText = expectedFrontText;
        this.expectedBackImage = expectedBackImage;
        this.expectedBackText = expectedBackText;
        this.frontFileExt = frontFileExt;
        this.backFileExt = backFileExt;
        this.isFrontImage = isFrontImage;
        this.isBackImage = isBackImage;            
    }
    
    /*
     * Verifies that the front of the card contains an image
     * and that the image matches what is expected.
     * @param imgFront The image on the front of the card.
     */
    @Override
    public void readFrontSide(BufferedImage imgFront, String fileExt) {
        // verify that front should be an image
        assertTrue(isFrontImage);
        // verify that the image is correct
        assertTrue(imgFront.equals(expectedFrontImage));
        // verify that file extension is correct
        assertTrue(frontFileExt.equals(fileExt));
    }

    /*
     * Verifies that the front of the card contains text
     * and that the text matches what is expected.
     * @param textFront The text on the front of the card.
     */
    @Override
    public void readFrontSide(String textFront) {
        // verify that front should be text
        assertTrue(!isFrontImage);
        // verify that the text is correct
        assertTrue(textFront.equals(expectedFrontText));
    }

    /*
     * Verifies that the back of the card contains an image
     * and that the image matches what is expected.
     * @param imgFront The image on the back of the card.
     */
    @Override
    public void readBackSide(BufferedImage imgBack, String fileExt) {
        // verify that back should be an image
        assertTrue(isBackImage);
        // verify that the image is correct
        assertTrue(imgBack.equals(expectedBackImage));
        // verify that file extension is correct
        assertTrue(backFileExt.equals(fileExt));
    }

    /*
     * Verifies that the back of the card contains text
     * and that the text matches what is expected.
     * @param textFront The text on the back of the card.
     */
    @Override
    public void readBackSide(String textBack) {
        // verify that back should be text
        assertTrue(!isBackImage);
        // verify that the text is correct
        assertTrue(textBack.equals(expectedBackText));
    }        
}
