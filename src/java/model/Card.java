package model;

import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.Set;


/**
 * Represents a mutable flashcard 
 * with a front and back side.
 * 
 * Specification fields:
 *  @specfield front-side // Front of the flashcard.
 *  @specfield back-side  // Back of the flashcard.
 *  @specfield metadata   // Information about card content. 
 * 
 * @author Mitchell
 */
public class Card {
    // 'front' and 'back' fields store 
    // actual card content
    protected BufferedImage imgFront;
    protected String textFront;    
    protected BufferedImage imgBack;
    protected String textBack;
    
    // store image file extensions
    private String frontImageExt;
    private String backImageExt;
    
    // tags store this flashcard's metadata 
    private Set<String> tags;
    
    private boolean isDeleted;
    private int id;  
    
    protected static DataStorage dataStorage = QueryProcessor.getInstance();

    /*
     * Representation invariant: 
     *     The front-side and back-side of this flashcard are
     *     only set to one type of content at any given time:
     *          front-side.imgFront != null XOR front-side.textFront != null               
     *          back-side.imgBack != null XOR back-side.textBack != null
     *          
     *     Also tags != null. 
     * 
     * Abstraction function:
     *     AF(r) = r.front-side = imgFront or textFront,
     *             r.back-side = imgBack or textBack,
     *             r.metadata = tags
     */
    
    /**
     * Constructs a Card.
     * @param imgFront Image on the front of the card.
     * @param frontImageExt The file extension of the front image.
     * @param textFront Text on the front of the card.
     * @param imgBack Image on the back of the card.
     * @param backImageExt The file extension of the back image. 
     * @param textBack Text on the back of the card.
     * @param id The ID for this card.
     * @param tags Tags which apply to this card.
     */
    Card(BufferedImage imgFront, String frontImageExt, String textFront, 
         BufferedImage imgBack, String backImageExt, String textBack, 
         int id, Set<String> tags) {
        this.id = id;
        this.tags = tags;
        this.isDeleted = false;
        
        this.imgFront = imgFront;
        this.textFront = textFront;
        this.imgBack = imgBack;
        this.textBack = textBack;  
        
        this.frontImageExt = frontImageExt;
        this.backImageExt = backImageExt;
    }
    
    /*
     * Initializes this Card.
     * @param tags Tags which apply to this card.
     */
    protected Card(Set<String> tags) {
        // id initialized to a default value prior
        // to being set by the QueryProcessor
        this.id = -1;
        this.tags = tags;
        this.isDeleted = false;
        
        // these are set by individual constructors
        this.imgFront = null;
        this.textFront = null;
        this.imgBack = null;
        this.textBack = null;     
        
        this.frontImageExt = null;
        this.backImageExt = null;
    }

    /**
     * Constructs a Card with text on the
     * front and back.
     * @param textFront Text on the front of the card.
     * @param textBack Text on the back of the card.
     * @param tags Tags which apply to this card.
     */
    public Card(String textFront, String textBack, Set<String> tags) {      
        this(tags);       
        this.textFront = textFront;
        this.textBack = textBack;     
        dataStorage.newCard(this);   
    }
    
    /**
     * Constructs a Card with an image on the
     * front and text on the back.
     * @param imgFront Image on the front of the card.
     * @param frontImageExt The file extension of the front image.
     * @param textBack Text on the back of the card.
     * @param tags Tags which apply to this card.
     */
    public Card(BufferedImage imgFront, String frontImageExt, 
                String textBack, Set<String> tags) {
        this(tags);        
        this.imgFront = imgFront;
        this.textBack = textBack;
        this.frontImageExt = frontImageExt;
        dataStorage.newCard(this);   
    }
    
    /**
     * Constructs a Card with text on the
     * front and an image on the back.
     * @param textFront Text on the front of the card.
     * @param imgBack Image on the back of the card.
     * @param backImageExt The file extension of the back image.
     * @param tags Tags which apply to this card.
     */
    public Card(String textFront, BufferedImage imgBack, 
                String backImageExt, Set<String> tags) {
        this(tags);
        this.textFront = textFront;
        this.imgBack = imgBack;
        this.backImageExt = backImageExt;
        dataStorage.newCard(this);   
    }
    
    /**
     * Constructs a Card with an image on the
     * front and an image on the back.
     * @param imgFront Image on the front of the card.
     * @param frontImageExt The file extension of the front image.
     * @param imgBack Image on the back of the card.
     * @param backImageExt The file extension of the back image.
     * @param tags Tags which apply to this card.
     */
    public Card(BufferedImage imgFront, String frontImageExt, 
                BufferedImage imgBack, String backImageExt, Set<String> tags) {
        this(tags);        
        this.imgFront = imgFront;
        this.imgBack = imgBack;
        this.frontImageExt = frontImageExt;
        this.backImageExt = backImageExt;
        dataStorage.newCard(this);   
    }   
    
    /**
     * Allows a SideReader to read the content on the front
     * of the flashcard.
     * @requires reader != null, deleteCard has not been 
     * previously called on this
     * @param reader Reads the content on the front of the card.
     */
    public void readFront(SideReader reader) {
        assert !isDeleted;
        if (isFrontImage()) {
            reader.readFrontSide(imgFront, frontImageExt);
        } else {
            reader.readFrontSide(textFront);
        }
    }
    
    /**
     * Allows a SideReader to read the content on the back
     * of the flashcard.
     * @requires reader != null, deleteCard has not been 
     * previously called on this
     * @param reader Reads the content on the back of the card.
     */
    public void readBack(SideReader reader) {
        assert !isDeleted;
        if (isBackImage()) {
            reader.readBackSide(imgBack, backImageExt);
        } else {
            reader.readBackSide(textBack);
        }
    }
    
    /**
     * Sets the front of this card to text.
     * @requires text != null, deleteCard has not been
     * previously called on this
     * @param text Text to be set on the front of the card.
     */
    public void setFront(String text) {
        assert !isDeleted;
        textFront = text;
        imgFront = null;        
        dataStorage.updateCardContent(this);
    }
    
    /**
     * Sets the front of this card to an image.
     * @requires img != null, deleteCard has not been 
     * previously called on this
     * @param img Image to be set on the front of the card.
     * @param fileExt The file extension of the image.
     */
    public void setFront(BufferedImage img, String fileExt) {
        assert !isDeleted;
        imgFront = img;
        textFront = null;
        frontImageExt = fileExt;
        dataStorage.updateCardContent(this);
    }
    
    /**
     * Sets the back of this card to text.
     * @requires text != null, deleteCard has not been 
     * previously called on this
     * @param text Text to be set on the back of the card.
     */
    public void setBack(String text) {
        assert !isDeleted;
        textBack = text;
        imgBack = null;
        dataStorage.updateCardContent(this);
    }
    
    /**
     * Sets the back of this card to an image.
     * @requires img != null, deleteCard has not been
     * previously called on this
     * @param img Image to be set on the back of the card.
     * @param fileExt The file extension of the image.
     */
    public void setBack(BufferedImage img, String fileExt) {
        assert !isDeleted;
        imgBack = img;
        textBack = null;
        backImageExt = fileExt;
        dataStorage.updateCardContent(this);
    }
    
    /**
     * Sets the tags that this Card uses.
     * @requires tags != null, deleteCard has not been 
     * previously called on this
     * @modifies this
     * @effect this_post.tags = tags
     */
    public void setTags(Set<String> tags) {
        assert !isDeleted;
        this.tags = tags;
        dataStorage.updateCardContent(this);
    }
    
    /**
     * Gets the tags that this Card uses.
     * @requires deleteCard has not been previously called on this
     * @return unmodifiable set of tags this Card uses.
     */
    public Set<String> getTags() {
        assert !isDeleted;
        if (tags != null) {
        	return Collections.unmodifiableSet(tags);
        }
        return null;
    }
    
    /**
     * Deletes the entire flashcard.
     * @requires deleteCard has not been previously called on this
     */
    public void deleteCard() {
        assert !isDeleted;
        isDeleted = true;
        dataStorage.deleteCard(this);
    }
    
    /* 
     * Checks if the front of the card is an image.
     * @return true if front is an image, else false
     */
    private boolean isFrontImage() {
        return imgFront != null;
    }
    
    /* 
     * Checks if the back of the card is an image.
     * @return true if back is an image, else false
     */
    private boolean isBackImage() {
        return imgBack != null;
    }
    
    /**
     * Sets the id of this flashcard.
     * @param id The id that this card will be set to.
     */
    public void setID(int id) {
        this.id = id;
    }
    
    /**
     * Gets the id of this flashcard.
     * @return id of this flashcard.
     */
    public int getID() {
        return id;
    }
    
    /**
     * Compares two flashcards for equality.
     * @return true iff the flashcards contain the same content, 
     * false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Card)) {
            return false;
        }
        // check for equality
        Card otherCard = (Card) obj;
        
        // verify that the cards contain the
        // same content on the front
        if (this.isFrontImage()) {
            // other side must be an image and have same file extension
            if (!otherCard.isFrontImage() 
                    || !frontImageExt.equals(otherCard.frontImageExt)) {
                return false;
            }
            // we can only compare .png file because they do not
            // loose quality when saved
            if (frontImageExt.equals("png") 
                    && !imagesAreEqual(imgFront, otherCard.imgFront)) {
                return false;
            }            
        } else {
            // verify that text is the same
            if (otherCard.isFrontImage() 
                    || !this.textFront.equals(otherCard.textFront)) {
                return false;
            }      
        }        
        
        // verify that the cards contain the
        // same content on the back
        if (this.isBackImage()) {
            // verify that images are the same
            if (!otherCard.isBackImage() 
                    || !backImageExt.equals(otherCard.backImageExt)) {
                return false;
            }         
            // we can only compare .png file because they do not
            // loose quality when saved
            if (backImageExt.equals("png") 
                    && !imagesAreEqual(imgBack, otherCard.imgBack)) {
                return false;
            }  
        } else {
            // verify that text is the same
            if (otherCard.isBackImage() 
                    || !this.textBack.equals(otherCard.textBack)) {
                return false;
            }                   
        }        
        
        return true;
    }
    
    /**
     * Compares two BufferedImages for equality. There is no built-in Java 
     * support for comparing BufferedImages for equality. This code is a 
     * solution for comparing BufferedImages which was borrowed from a 
     * StackOverflow post.
     * 
     * URL: http://tinyurl.com/buff-img-compare
     * 
     * @param image1 A BufferedImage to compare for equality.
     * @param image2 A BufferedImage to compare for equality.
     * @return true iff the images have the same size and pixel values, else false.
     */
    boolean imagesAreEqual(BufferedImage image1, BufferedImage image2) {
        if (image1.getWidth() != image2.getWidth() 
            || image1.getHeight() != image2.getHeight()) {
             return false;
        }
        
        for (int x = 1; x < image2.getWidth(); x++) {
            for (int y = 1; y < image2.getHeight(); y++) {
                 if (image1.getRGB(x, y) != image2.getRGB(x, y)) {
                     return false;
                 }
            }
        }               
        return true;
    }
    
    /**
     * Computes the hashcode for this Card.
     * @return hashcode for this Card.
     */
    @Override
    public int hashCode() {
        int code = 0;
        if (isFrontImage()) {
            code += imgFront.hashCode() * 17;
            code += frontImageExt.hashCode() * 7;
        } else {
            code += textFront.hashCode() * 17;
        }
        
        if (isBackImage()) {
            code += imgBack.hashCode() * 9;
            code += backImageExt.hashCode() * 7;
        } else {
            code += textBack.hashCode() * 9;
        }
        
        return code;
    }
}
