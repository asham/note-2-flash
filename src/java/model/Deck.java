package model;

import java.util.*;

/**
 * Represents a mutable deck object. 
 * A deck contains list of cards and knows
 * how to manipulate those cards.
 * 
 * @author Tianchi
 */

public class Deck {
    // Deck private field
    private String deckName;
    private List<Card> cardList;

    // A reference to dataStorage class
    private static DataStorage dataStorage = QueryProcessor.getInstance();

    // A global data structure to store all Deck names we have so far,
    // so that we can ensure unique names to all decks
    private static Set<String> allDeckNames = new HashSet<String>();
    
    /*
     * Representation invariant: 
     * deckName is not null, cardList is not null, dataStorage is not null
     */
    
    /**
     * Construct a new deck with deckName.
     * The deckName should be unique for each deck.
     * @param deckName The name of deck, which is not null
     * @throws IllegalArgumentException if deckName already exists
     * @requires deckName is not null, deckName is unique 
     * for decks we have so far
     */
    public Deck(String deckName) {
        deckName = deckName.trim();
        if (allDeckNames.contains(deckName)) {
            throw new IllegalArgumentException("Duplicate deck name.");
        }
        allDeckNames.add(deckName);
        this.deckName = deckName;
        cardList = new ArrayList<Card>();
        dataStorage.newDeck(deckName);
    }

    /**
     * Package only constructor. Create a new deck with given
     * list of cards. This constructor does not consult database
     * @param cards The list of card given to this deck. 
     * @requires cards is not null
     */
    Deck(String deckName, List<Card> cards) {
        this.deckName = deckName;
        this.cardList = new ArrayList<Card>();
        this.cardList.addAll(cards);
    }

    /**
     * Get the name of this deck
     * @return Name of this deck
     */
    public String getDeckName() {
        return deckName;
    }
    
    /**
     * Get the number of cards in this deck
     * @return The number of cards in this deck
     */
    public int getNumCards() {
        return cardList.size();
    }
    
    /**
     * Remove a list of cards from this deck
     * @param removeList List of cards to be removed, which is not null 
     */
    public void removeCards(List<Card> removeList) {
    	// Create a temporary list to hold new cards
        List<Card> newList = new ArrayList<Card>();
        for (Card card : cardList) {
            boolean keep = true;
            for (Card card2 : removeList) {
                if (card.getID() == card2.getID()) {
                    keep = false;
                }
            }
            if (keep) {
                newList.add(card);
            }
        }
        // Update cardList to the temporary list
        cardList = newList;
        
        // Remove those cards from database
        dataStorage.removeCardsFromDeck(this, removeList);
        // Tell each card to remove itself from database
        for (Card c : removeList) {
            c.deleteCard();
        }
    }
    
    /**
     * Add a list of cards to this deck
     * @param addList List of cards to be added, which is not null
     */
    public void addCards(List<Card> addList) {
        for (Card card : addList) {
            cardList.add(card);
        }
        dataStorage.addCardsToDeck(this, addList);
    }
    
    /**
     * Get the list of cards contained in this deck.
     * Users can not change this list of cards
     * @return List of cards contained in this deck
     */
    public List<Card> getCards() {
        return Collections.unmodifiableList(cardList);
    }
    
    /**
     * Get the list of cards referred by the tags.
     * Client can not change this list
     * @param tagSet set of tags
     * @return List of cards referred by the tags,
     * return an empty list if no cards found
     */
    public List<Card> getCardsByTag(Set<String> tagSet) {
    	
        // Empty tag list
        if (tagSet.size() == 0) {
            return Collections.unmodifiableList(cardList);
        }

        // tagSet has one or more elements
        List<Card> res = new ArrayList<Card>();
        int count = 0;
        for (String tag : tagSet) {
            // First tag
            if (count == 0) {
                for (Card card : cardList) {
                    if (card.getTags().contains(tag)) {
                        res.add(card);
                    }
                }
            // Other tags
            } else {
                List<Card> res2 = new ArrayList<Card>();
                for (Card card : res) {
                    // Keep if contains next tag
                    if (card.getTags().contains(tag)) {
                        res2.add(card);
                    }
                }
                // Update return list
                res = res2;
            }
            count++;
        }
        return Collections.unmodifiableList(res);
    }

    /**
     * Remove this deck from database. All cards
     * in this deck are removed as well.
     */
    public void remove() {
        for (Card c : cardList) {
            c.deleteCard();
        }
        dataStorage.deleteDeck(this.deckName);
        allDeckNames.remove(this.deckName);
    }
    
    /**
     * Retrieve the deck from database
     * @param deckName The name of deck to be retrieved
     * @return A deck referred by deckName, null if such deck
     * does not exist in the database
     */
    public static Deck getStoredDeck(String deckName) {
        try {
            return dataStorage.getDeck(deckName);
        }
        catch(IllegalArgumentException e) {
            return null;
        }
    }

    /**
     * Clear all decks as well as cards they
     * contains from database
     */
    public static void removeAllStoredDecks() {
        allDeckNames.clear();
        dataStorage.clearData();
    }
    
}
