package model;

import java.awt.image.BufferedImage;

/**
 * Represents a class which is capable of reading the
 * content stored on the front and back of Card.
 * @author Mitchell
 *
 */
public interface SideReader {
    public void readFrontSide(BufferedImage imgFront, String fileExt);
    public void readFrontSide(String textFront);
    public void readBackSide(BufferedImage imgBack, String fileExt);
    public void readBackSide(String textBack);
}
