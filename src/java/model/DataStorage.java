package model;

import java.util.List;

/**
 * This interface represents the basic functionality the model
 * must provide.
 * 
 * The way the model actually stores the data is up to the implementing
 * class
 * 
 * @author Adrian
 *
 */
public interface DataStorage {
    
    /**
     * Creates a new deck with "deckName" in the model
     * 
     * @requires deckName != null
     * @param deckName name of the deck
     * @effects creates a new deck with deckName
     * @modifies model
     */
    public void newDeck (String deckName);
    
    /**
     * Adds a list of cards to a deck
     * 
     * @param deck the target deck
     * @param cards the cards to be added to deck
     * @requires deck != null && cards != null
     * @modifies deck by adding cards
     * @throws IllegalArgumentException if deck does not exist in model
     */
    public void addCardsToDeck (Deck deck, List<Card> cards);
    
    /**
     * Removes a list of cards from a deck. 
     * 
     * @param deck the target deck
     * @param cards the cards to be removed from the deck
     * @requires deck != null && cards != null && deck exists in model
     * @modifies deck by removing cards
     * @throws IllegalArgumentException if deck does not exist in model
     */
    public void removeCardsFromDeck (Deck deck, List<Card> cards);
    
    /**
     * Creates a new card in the model
     * 
     * @param card card to be added to model
     * @requires card != null
     * @modifies model
     */
    public void newCard (Card card);
    
    /**
     * Deletes a card from the model
     * 
     * @param card card to be deleted
     * @requires card != null
     * @modifies model
     */
    public void deleteCard (Card card);
    
    /**
     * Updates the model to reflect the content of the cards
     * 
     * @param card card with updated data
     * @requires card exists in model
     * @modifies card
     */
    public void updateCardContent (Card card);
    
    /**
     * Check if the model has this deck
     * 
     * @param deckName name of deck
     * @return true if deck exists, false otherwise
     */
    public boolean hasDeck (String deckName);
    
    /**
     * Deletes the deck from the model
     * 
     * @param deckName name of deck
     * @requires deck is empty
     * @see deleteCard
     * 
     */
    public void deleteDeck (String deckName);
    
    /**
     * Returns a list of the decks in the model
     * 
     * @return the list of names of decks in the model
     */
    public List<String> getDecks ();
    
    /**
     * Grab a deck from the model
     * 
     * @param deckName name of the deck in the model
     * @return deck from the model
     * @throws IllegalArgumentException if the deck does not exist
     */
    public Deck getDeck(String deckName);
    
    /**
     * Clears out all data in model
     */
    public void clearData();
    
    /**
     * Returns a list of all tags in this deck
     * @param name of the deck
     * @return a list of strings for all tags in this deck
     * @throws IllegalArgumentException if deck is not in model
     */
    public List<String> getTags(String deckName); 
    
    /**
     * Change the save location of the model
     * <p>
     * A file path that is implicitly relative to the current working directory is not allowed in the database URL "jdbc:h2:test". 
     * Use an absolute path, ~/name, ./name, or the baseDir setting instead.
     * <p>
     * If switching to a new save file location that was previously empty, 
     * will create a new save file
     * 
     * @param location new location of model
     * @throws IllegalArgumentException if location == null
     */
    public void changeSaveLocation(String location);
    
    /**
     * 
     * @return number of cards in this DataStorage class, or -1 if it fails
     */
    public int getCardCount();

}
