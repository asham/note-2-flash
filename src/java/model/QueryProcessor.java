package model;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

/**
 * Implements DataStorage interface by using a database to store user data. 
 * QueryProcessor is a singleton class so all instances of QueryProcessor 
 * is obtained using getInstance
 *  * 
 * @author Adrian
 *
 */
public class QueryProcessor implements DataStorage {
    /*
     * Definitions:
     * 
     * QueryProcessor maintains a connection to a single database. This database
     * is used to store information about the cards and decks for the system
     * permanently.
     * 
     * QueryProcessor is a singleton class in order to enforce the idea that
     * there should only be one database in use within the application. Therefore,
     * the constructors are private
     * 
     * Database schema explanation
     * 
     * FlashCard: Used to store data for individual flash cards
     * - cid: ID of card
     * - front: used to store image data for front of card
     * - frontType: used to store text for front of card
     * - frontImgType: used to store the file extension of image
     * - back: used to store image data for back of card
     * - fronType: used to store text for back of card
     * - frontImgType: used to store the file extension of image
     * 
     * Tag: Used to store the tags for each flash card
     * - cid: Refers to the cid of card
     * - tag: tag of a card
     * 
     * Deck: Used to store the Decks available
     * - deckName: Store the name of a deck, must be unique
     * 
     * CardToDeck: Used to link cards to a specific deck
     * - cid: card id
     * - deckName: name of the deck
     * 
     * Representation Invariant:
     * 
     * con != null && dbLocation != null
     */
    private static final Logger LOGGER = Logger.getLogger( QueryProcessor.class.getName() );

    /** Embedded (local) connection */
    public static final String DB_PREFIX = "jdbc:h2:";
    /** Database save file location*/
    public static final String DB_DEFAULT_LOC = "./note2flash";
    
    /** Test database save file location*/
    public static final String DB_TEST_DEFAULT_LOC = "./dbtest";

    /**
     * The maximum number of characters that can be stored on one side of card
     */
    public static final int MAX_CHARS_PER_SIDE = 500;

    /**
     * The maximum number of characters for a tag
     */
    public static final int MAX_TAG_LENGTH = 500;
    
    /**
     * The maximum length of a deck name
     */
    public static final int MAX_DECKNAME_LENGTH = 500;
    
    //SQL STATEMENTS

    /** SQL Statement for creating FlashCard table*/
    public static final String CREATE_FLASHCARD_TABLE = "CREATE TABLE IF NOT EXISTS FlashCard (cid int NOT NULL AUTO_INCREMENT PRIMARY KEY,"+ 
            "front blob,"+
            "frontType varchar("+MAX_CHARS_PER_SIDE+"),"+
            "frontImgType varchar(10),"+
            "back blob,"+
            "backType varchar("+MAX_CHARS_PER_SIDE+"), "+
            "backImgType varchar(10));";

    /** SQL statement Create Tag table*/
    public static final String CREATE_TAG_TABLE = "CREATE TABLE IF NOT EXISTS Tag (cid int NOT NULL REFERENCES FlashCard(cid), tag varchar("+MAX_TAG_LENGTH+"));";

    /** Create Deck table*/
    public static final String CREATE_DECK_TABLE = "CREATE TABLE IF NOT EXISTS Deck (deckName varchar("+ MAX_CHARS_PER_SIDE +") PRIMARY KEY);";

    /** Create card to deck table*/
    public static final String CREATE_CARDTODECK_TABLE = "CREATE TABLE IF NOT EXISTS CardToDeck (cid int NOT NULL REFERENCES FlashCard(cid), "+
            "deckName varchar("+MAX_DECKNAME_LENGTH+") NOT NULL REFERENCES Deck(deckName)); ";

    /** Drop card to deck table*/
    public static final String DROP_CARDTODECK_TABLE = "DROP TABLE cardtodeck;";

    /** Drop deck table*/
    public static final String DROP_DECK_TABLE = "DROP TABLE deck;";

    /** Drop tag table*/
    public static final String DROP_TAG_TABLE = "DROP TABLE tag;";

    /** Drop flashcard table*/
    public static final String DROP_FLASHCARD_TABLE = "DROP TABLE flashcard;";

    /**Insert a card into table*/
    public static final String INSERT_CARD = "INSERT INTO FlashCard (front, frontType, frontImgType, back, backType, backImgType) " +
            "VALUES (?, ?, ?, ?, ?, ?);";

    /**Delete card from table */
    public static final String DELETE_CARD = "DELETE FROM FlashCard WHERE cid = ?;";

    /**Update all information on card */
    public static final String EDIT_CARD = "UPDATE FlashCard SET front=?, frontType=?, frontImgType=?, back=?, backType=?, backImgType=? "+
            "WHERE cid = ?;";

    /**Edit 'front' field of card*/
    public static final String EDIT_CARD_FRONT = "UPDATE FlashCard SET front=?, frontImgType=? WHERE cid = ?;";

    /**Edit 'frontType' field of card*/
    public static final String EDIT_CARD_FRONT_TYPE = "UPDATE FlashCard SET frontType=? WHERE cid = ?;";

    /**Edit 'back' field of card*/
    public static final String EDIT_CARD_BACK = "UPDATE FlashCard SET back=?, backImgType=? WHERE cid = ?;";

    /**Edit 'backType' field of card*/
    public static final String EDIT_CARD_BACK_TYPE = "UPDATE FlashCard SET backType=? WHERE cid = ?;";

    /**Delete deck from deck table*/
    public static final String DELETE_DECK = "delete from deck where deckName = ?; ";

    /**Add a new deck to the deck table*/
    public static final String INSERT_DECK = "INSERT INTO deck (deckName) values (?);";

    /**Insert a new tag into tag table*/
    public static final String INSERT_TAG = "INSERT INTO Tag (cid, tag) VALUES (?, ?);";

    /**Delete a tag from tag table with matching cid and tag*/
    public static final String DELETE_TAG = "DELETE FROM Tag WHERE cid = ? AND tag = ?;";

    /**Delete a tag from tag table with matching cid*/
    public static final String DELETE_TAG_CID = "DELETE FROM Tag WHERE cid = ?;";

    /**Insert a new card to deck record*/
    public static final String INSERT_CARDTODECK ="INSERT INTO CARDTODECK (cid, deckName) values (?,?);";

    /**Delete record from card to deck with matching cid*/
    public static final String DELETE_CARDTODECK = "DELETE FROM CARDTODECK WHERE cid=?;";

    /**Delete record from card to deck with matching cid and deckName*/
    public static final String DELETE_CARDTODECK_NAME = "DELETE FROM CARDTODECK WHERE cid=? AND deckName=?";

    /**Get a names of all decks*/
    public static final String GET_DECK_NAMES = "SELECT deckName from Deck;";

    /**Get a specific deck name from deck table*/
    public static final String GET_SPECIFIC_DECK_NAME = "SELECT deckName from Deck WHERE deckName = ?;";

    /**Count the number of cards*/
    public static final String COUNT_CARD = "SELECT COUNT(*) as number FROM FlashCard;";

    /**Get cards in a specific deck*/
    public static final String GET_CARDS_IN_DECK = "SELECT DISTINCT fc.cid, fc.front, fc.frontType, fc.frontImgType, fc.back, fc.backType, fc.backImgType "+
            "FROM FlashCard fc, Deck d, CardToDeck ctd " +
            "WHERE fc.cid = ctd.cid AND ctd.deckName = ?;";

    /**Get a tag with matching cid*/
    public static final String GET_TAG_BY_CID = "SELECT * FROM tag where cid = ?;";

    /**Get a card with matching cid*/
    public static final String GET_CARD_BY_CID = "SELECT cid, front, frontType, frontImgType, back, backType, backImgType FROM FlashCard WHERE cid = ?;";

    /**Get all the tags present in a certain deck*/
    private static final String GET_TAGS_FOR_DECK = "SELECT DISTINCT tag FROM Tag t, CardToDeck ctd WHERE ctd.deckName = ? AND t.cid = ctd.cid;";
    
    /**Database connection*/
    private static Connection con = null;

    /**An instance of QueryProcessor*/
    private static final QueryProcessor INSTANCE = new QueryProcessor();

    /**
     * Location of database file
     */ 
    private String dbLocation;    

    /**
     * Private constructor since this is a singleton class
     */
    private QueryProcessor() {
        this(DB_DEFAULT_LOC);
    }

    /**
     * Overloaded constructor
     * @param givenDBLocation database file location
     */
    private QueryProcessor(String givenDBLocation) {
        if (givenDBLocation == null) {
            LOGGER.severe("dbLocation was null");
            throw new IllegalArgumentException("dbLocation cannot be null");
        }
        setupConnection(givenDBLocation);
    }

    /**
     * Return an instance of Query Processor
     * 
     * @return instance of Query Processor
     * @throws IllegalStateException if database is already in use
     */
    public static QueryProcessor getInstance() {
        return INSTANCE;
    }

    @Override
    public void newDeck(String deckName) {
        if (deckName == null) {
            LOGGER.severe("deckName was null");
            throw new IllegalArgumentException("deckName cannot be null");
        }
        LOGGER.info("Create new deck: " + deckName);
        try (PreparedStatement insertDeck = con.prepareStatement(INSERT_DECK)) {
            insertDeck.setString(1, deckName);
            insertDeck.executeUpdate();
        } catch (SQLException e ) {
            LOGGER.log(Level.SEVERE, "Error creating new deck: ", e);
        } 
    }

    @Override
    public void addCardsToDeck(Deck deck, List<Card> cards) {
        if (deck == null || cards == null) {
            LOGGER.severe("input was null");
            throw new IllegalArgumentException("deck and cards can't be null");
        }
        if (!hasDeck(deck.getDeckName())) {
            LOGGER.severe("Invalid deck name");
            throw new IllegalArgumentException("deck does not exist in model");
        }
        LOGGER.info("add cards " +cards+ " to deck " + deck.getDeckName());
        try (PreparedStatement insertCardToDeck = con.prepareStatement(INSERT_CARDTODECK)){
            for (Card card : cards) {
                LOGGER.info("card id " + card.getID());
                insertCardToDeck.setInt(1, card.getID());
                insertCardToDeck.setString(2, deck.getDeckName());
                insertCardToDeck.executeUpdate();
            }
        } catch (SQLException e ) {
            LOGGER.log(Level.SEVERE, "Error adding card to deck: " + deck.getDeckName(), e);
        }
    }

    @Override
    public void removeCardsFromDeck(Deck deck, List<Card> cards) {
        if (deck == null || cards == null) {
            LOGGER.severe("input was null");
            throw new IllegalArgumentException("deck and cards can't be null");
        }
        if (!hasDeck(deck.getDeckName())) {
            LOGGER.severe("Invalid deck name");
            throw new IllegalArgumentException("deck does not exist in model");
        }
        LOGGER.info("Remove cards " + cards + " from " + deck.getDeckName());
        try (PreparedStatement removeCardFromDeck = con.prepareStatement(DELETE_CARDTODECK_NAME)) {
            for (Card card : cards) {
                removeCardFromDeck.setInt(1, card.getID());
                removeCardFromDeck.setString(2, deck.getDeckName());
                removeCardFromDeck.executeUpdate();
            }
        } catch (SQLException e ) {
            LOGGER.log(Level.SEVERE, "Error removing card from deck: ", e);
        }
    }

    @Override
    public void newCard(Card card) {
        LOGGER.info("Create new card: " + card);
        //id generated by db, initialize with something impossible to check for error
        int cardIndex = -1;

        try (PreparedStatement newCard = con.prepareStatement(INSERT_CARD)) {

            newCard.setString(1, null);
            newCard.setBinaryStream(2, null);
            newCard.setString(3, null);
            newCard.setString(4,  null);
            newCard.setBinaryStream(5, null);
            newCard.setString(6, null);
            newCard.executeUpdate();
            ResultSet rs = newCard.getGeneratedKeys();
            while(rs.next()) {
                cardIndex = rs.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "failed to add new card: ", e);
        } 
        if (cardIndex <= 0) {
            //abort if cardIndex is not correct
            LOGGER.severe("insert failed: " + cardIndex);
            return;
        }

        InsertReader read = new InsertReader(cardIndex);
        card.readFront(read);
        card.readBack(read);
        // should be inside db already

        // work on tags
        if (card.getTags() != null) {
        	Set <String> tags = card.getTags();
        	Iterator <String> it = tags.iterator();
        	while (it.hasNext()) {
        		// add tags
        		try (PreparedStatement insertTag = con.prepareStatement(INSERT_TAG)) {
        			insertTag.setInt(1, cardIndex);
        			insertTag.setString(2, it.next());
        			insertTag.executeUpdate();
        		} catch (SQLException e) {
        			LOGGER.log(Level.SEVERE, "failed to add tags: ", e);
        		}
        	}
        }
        // only set the card id if it was invalid to start, else move on
        card.setID(cardIndex);
    }

    private static class InsertReader implements SideReader {
        private int id;

        public InsertReader (int id) {
            this.id = id;
        }

        @Override
        public void readFrontSide(String textFront) {
            try (PreparedStatement insertFront = con.prepareStatement(EDIT_CARD_FRONT_TYPE)) {
                insertFront.setString(1, textFront);
                insertFront.setInt(2, id);
                insertFront.executeUpdate();
            } catch (SQLException e) {
                LOGGER.log(Level.SEVERE, "failed to insert text to front: ", e);
            }
        }

        @Override
        public void readBackSide(String textBack) {
            try (PreparedStatement insertFront = con.prepareStatement(EDIT_CARD_BACK_TYPE)) {
                insertFront.setString(1, textBack);
                insertFront.setInt(2, id);
                insertFront.executeUpdate();
            } catch (SQLException e) {
                LOGGER.log(Level.SEVERE, "failed to insert text to back: ", e);
            }
        }

        @Override
        public void readFrontSide(BufferedImage imgFront, String fileExt) {
            try (PreparedStatement insertFront = con.prepareStatement(EDIT_CARD_FRONT)) {
                ByteArrayOutputStream bs = new ByteArrayOutputStream();
                ImageIO.write(imgFront, fileExt, bs);
                InputStream is = new ByteArrayInputStream(bs.toByteArray());
                insertFront.setBinaryStream(1, is);
                insertFront.setString(2, fileExt);
                insertFront.setInt(3, id);
                insertFront.executeUpdate();
            } catch (SQLException | IOException e) {
                LOGGER.log(Level.SEVERE, "failed to insert image to front: ", e);
            }
        }

        @Override
        public void readBackSide(BufferedImage imgBack, String fileExt) {
            try (PreparedStatement insertFront = con.prepareStatement(EDIT_CARD_BACK)) {
                ByteArrayOutputStream bs = new ByteArrayOutputStream();
                ImageIO.write(imgBack, fileExt, bs);
                InputStream is = new ByteArrayInputStream(bs.toByteArray());
                insertFront.setBinaryStream(1, is);
                insertFront.setString(2, fileExt);
                insertFront.setInt(3, id);
                insertFront.executeUpdate();
            } catch (SQLException | IOException e) {
                LOGGER.log(Level.SEVERE, "failed to insert image to back: ",  e);
            }  
        }
    }

    @Override
    public void deleteCard(Card card) {
        LOGGER.info("delete card" + card.toString());
        try (PreparedStatement deleteCard = con.prepareStatement(DELETE_CARD)) {
            deleteCardToDeck(card.getID());
            deleteCard.setInt(1, card.getID());
            deleteTag(card.getID());
            deleteCard.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "failed to delete card", e);
        }
    }

    /**
     * Delete a card from the cardToDeck table
     * @param cid cid of card to be deleted
     */
    private void deleteCardToDeck (int cid) {
        LOGGER.info("delete cardtodeck cid " + cid);
        try (PreparedStatement deleteCTD = con.prepareStatement(DELETE_CARDTODECK)) {
            deleteCTD.setInt(1, cid);
            deleteCTD.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "failed to delete cardtotag", e);
        }

    }

    /**
     * Delete all tags for a card
     * 
     * @param cid cid of target card
     */
    private void deleteTag(int cid) {
        LOGGER.info("delete tag with cid " + cid);
        try (PreparedStatement deleteTag = con.prepareStatement(DELETE_TAG_CID)) {
            deleteTag.setInt(1, cid);
            deleteTag.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "failed to delete tag", e);
        }
    }

    @Override
    public void updateCardContent(Card card) {
        int cardIndex = card.getID();
        InsertReader read = new InsertReader(cardIndex);
        card.readFront(read);
        card.readBack(read);
        // should be inside db already

        // work on tags
        Set <String> tags = card.getTags();
        Iterator <String> it = tags.iterator();
        deleteTag(cardIndex);

        // add tags
        try (PreparedStatement insertTag = con.prepareStatement(INSERT_TAG)) {
            while (it.hasNext()) {
                insertTag.setInt(1, cardIndex);
                insertTag.setString(2, it.next());
                insertTag.executeUpdate();
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "failed to add tags: ", e);
        }
    }

    @Override
    public boolean hasDeck(String deckName) {

        List<String> listOfDecks = new LinkedList<String>();
        try (PreparedStatement getDecks = con.prepareStatement(GET_SPECIFIC_DECK_NAME)) {
            getDecks.setString(1, deckName);
            ResultSet result = getDecks.executeQuery();
            while (result.next()) {
                String deckInDb = result.getString("deckName");
                listOfDecks.add(deckInDb);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "failed to check if deck exists ", e);
        }
        return listOfDecks.contains(deckName);
    }

    @Override
    public void deleteDeck(String deckName) {
        LOGGER.info("Delete deck: " + deckName);
        try (PreparedStatement deleteDeck = con.prepareStatement(DELETE_DECK)) {
            deleteDeck.setString(1, deckName);
            deleteDeck.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "unable to delete deck", e);
        }
    }

    /**
     * Returns the db save file location, <b>without</b> prefix
     * @return db save file location
     */
    public String getFileLocation() {
        int cut = DB_PREFIX.length();
        return dbLocation.substring(cut, dbLocation.length());
    }

    @Override
    public List<String> getDecks() {
        List<String> listOfDecks = new LinkedList<String>();
        try (PreparedStatement getDecks = con.prepareStatement(GET_DECK_NAMES)) {
            ResultSet result = getDecks.executeQuery();
            while (result.next()) {
                String deckName = result.getString("deckName");
                listOfDecks.add(deckName);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "could not get deck ", e);
        }
        return listOfDecks;
    }

    @Override
    public Deck getDeck(String deckName) {
        if (!hasDeck(deckName)) {
            LOGGER.info("no such deck in model");
            throw new IllegalArgumentException("No such deck: " + deckName);
        }
        List <Card> cards = null;

        // grab all cards in this deck
        ResultSet rs = null;
        try (PreparedStatement getCardsInDeck = con.prepareStatement(GET_CARDS_IN_DECK)) {
            getCardsInDeck.setString(1, deckName);
            rs = getCardsInDeck.executeQuery();
            cards = contructCard(rs);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "could not get cards ", e);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "could not read image ", e);
        }
        if (cards == null) {
            LOGGER.severe("list of cards is null");
            throw new RuntimeException("list of cards is null");
        }
        Deck newDeck = new Deck(deckName, cards);
        return newDeck; 
    }

    /**
     * Given a result set from db, construct a list of card
     * 
     * @param rs result set from query
     * @return list of created cards
     * @throws SQLException if a database access error occurs or this method is called on a closed connection
     * @throws IOException if an error occurs during reading
     */
    private List<Card> contructCard (ResultSet rs) throws SQLException, IOException{
        PreparedStatement getTags = con.prepareStatement(GET_TAG_BY_CID);
        List <Card> cards = new LinkedList<Card>();
        while(rs.next()) {
            //construct card
            BufferedImage imgFront = null;
            String imgFrontExt = null;
            if (rs.getBinaryStream(2) != null) {
                imgFront = ImageIO.read(rs.getBinaryStream(2));
                imgFrontExt = rs.getString(4);
            }
            BufferedImage imgBack = null;
            String imgBackExt = null;
            if (rs.getBinaryStream(5) != null) {
                imgBack = ImageIO.read(rs.getBinaryStream(5));
                imgBackExt = rs.getString(7);
            }
            Set <String> tags = new HashSet<String>();
            getTags.setInt(1, rs.getInt(1));
            ResultSet tagSet = getTags.executeQuery();
            while (tagSet.next()) {
                tags.add(tagSet.getString(2));
            }
            Card c = new Card(imgFront, imgFrontExt, rs.getString(3), imgBack, imgBackExt, rs.getString(6), rs.getInt(1), tags);
            cards.add(c);
        }
        getTags.close();
        return cards;
    }

    /**
     * **<b>TESTING ONLY</b>**
     * 
     * This method should not be called externally, and is mainly available for testing
     * Creates the initial tables needed for database
     */
    private void initializeTables() {
        LOGGER.info("Initialize tables");
        try (PreparedStatement createFlashCard = con.prepareStatement(CREATE_FLASHCARD_TABLE)) {
            createFlashCard.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "could not create flashcard", e);
        }

        try (PreparedStatement createTag = con.prepareStatement(CREATE_TAG_TABLE)) {
            createTag.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "could not create tag ", e);
        }

        try (PreparedStatement createDeck = con.prepareStatement(CREATE_DECK_TABLE)) {
            createDeck.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "could not create deck ", e);
        }

        try (PreparedStatement createCardToDeck = con.prepareStatement(CREATE_CARDTODECK_TABLE)) {
            createCardToDeck.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "could not create cardtodeck ", e);
        }
    }

    /**
     * **<b>TESTING ONLY</b>**
     * 
     * "Drops" / deletes all the tables in the db!
     * Use only for testing when a clean database
     * is needed
     */
    private void dropTables() {
        LOGGER.info("Drop all tables");
        try (PreparedStatement deleteCardToDeck = con.prepareStatement(DROP_CARDTODECK_TABLE)) {
            deleteCardToDeck.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "fail to drop cardtodeck: ", e);
        }

        try (PreparedStatement deleteDeck = con.prepareStatement(DROP_DECK_TABLE)) {

            deleteDeck.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "fail to drop deck: ", e);
        }

        try (PreparedStatement deleteTag = con.prepareStatement(DROP_TAG_TABLE)) {
            deleteTag.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "fail to drop tag: ", e);
        }

        try (PreparedStatement deleteFlashCard = con.prepareStatement(DROP_FLASHCARD_TABLE)) {
            deleteFlashCard.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "fail to drop flashcard: ", e);
        }
    }

    /**
     * **<b>TESTING ONLY</b>**
     * Used to testing to see how many cards are in database
     * @return number of cards in database
     */
    protected int getTotalNumOfCards() {
        int count = -1;
        try (PreparedStatement countCard = con.prepareStatement(COUNT_CARD)) {
            ResultSet rs = countCard.executeQuery();
            while(rs.next()) {
                count = rs.getInt("number");
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "failed to count cards: ", e);
        }
        return count;
    }

    /**
     * **<b>TESTING ONLY</b>**
     * Used to grab a card from DB
     * 
     * @param cid cid of card to be grabbed
     * @return Card object from database
     */
    protected Card getCard(int cid) {
        List<Card> card = null;
        try (PreparedStatement getCard  = con.prepareStatement(GET_CARD_BY_CID)) {
            getCard.setInt(1, cid);
            ResultSet rs = getCard.executeQuery();
            card = contructCard(rs);
        } catch (SQLException | IOException e) {
            LOGGER.log(Level.SEVERE, "failed to get card: ", e);
        }
        if (card != null) {
            return card.get(0);
        } 
        return null;
    }

    @Override
    public void clearData() {
        dropTables();
        initializeTables();
    }

    @Override
    public List<String> getTags(String deckName) {
        if (!hasDeck(deckName)) {
            throw new IllegalArgumentException("no such deck " + deckName);
        }
        List <String> tags = new LinkedList<String>();
        try(PreparedStatement getTags = con.prepareStatement(GET_TAGS_FOR_DECK)) {
            getTags.setString(1, deckName);
            ResultSet rs = getTags.executeQuery();
            while(rs.next()) {
                String tag = rs.getString("tag");
                tags.add(tag);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "failed to get tags for deck ", e);
        }
        return tags;
    }

    @Override
    public void changeSaveLocation(String location) {
        if (location == null) {
            LOGGER.severe("new location cannot be null");
            throw new IllegalArgumentException("new location cannot be null");
        }
        setupConnection(location);
    }

	@Override
	public int getCardCount() {
		return getTotalNumOfCards();
	}
	
	/**
	 * Connect to a database in the given location. If there is no
	 * database in the location, initialize a new database.
	 * 
	 * @param location location of database
	 */
	private void setupConnection(String location) {
	    try {
            if (con != null) {
                //close any previous connection
                con.close();
            }
            Class.forName("org.h2.Driver");
            this.dbLocation = DB_PREFIX + location;
            LOGGER.info("Changing db to " + dbLocation);
            con = DriverManager.getConnection(dbLocation);
            initializeTables();
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Class not found: ", e);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Error connecting to database: ", e);
            throw new IllegalStateException();
        }
	    checkRep();
	}
	
	/**
	 * Used to check the representation invariant of QueryProcessor
	 */
	private void checkRep() {
	    if (dbLocation == null) {
	        throw new RuntimeException("dbLocation cannot be null");
	    }
	    if (con == null) {
	        throw new RuntimeException("con cannot be null");
	    }
	}
}