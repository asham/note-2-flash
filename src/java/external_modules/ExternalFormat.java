package external_modules;

import java.util.List;

import model.Card;
import model.DataStorage;

/**
 * Represents an interface which all importing
 * and exporting modules implement.
 * @author Mitchell
 *
 */
public abstract class ExternalFormat {
    private String fileExtImport;
    private String fileExtExport;
    
    /**
     * Default constructor
     */
    protected ExternalFormat() {
        this(null, null);
    }
    
    /**
     * Constructs a new ExternalFormat.
     */
    protected ExternalFormat(String fileExtExport, String fileExtImport) {
        this.fileExtExport = fileExtExport;
        this.fileExtImport = fileExtImport;
    }
    
    /**
     * Returns boolean to indicate if capable
     * of importing file with extension fileExt.
     * @param fileExt A file extension.
     * @return true iff this class can import fileExt, 
     * false otherwise
     */
    public boolean isImporter(String fileExt) {
        if(fileExtImport == null) return false;
        return fileExtImport.equals(fileExt);
    }
    
    /**
     * Returns boolean to indicate if capable
     * of exporting file with extension fileExt.
     * @param fileExt A file extension.
     * @return true iff this class can export fileExt, 
     * false otherwise.
     */
    public boolean isExporter(String fileExt) {
        if(fileExtExport == null) return false;
        return fileExtExport.equals(fileExt);
    }
    
    /**
     * Exports a collection of cards to a file.
     * @param fileName The name of the file to be created.
     * @param cards The list of cards which will be imported.
     * @throws Exception if a failure occurs during the export.
     */
    public abstract void exportCards(String fileName, List<Card> cards) throws Exception;
    
    
    /**
     * Imports a collection of cards from a file - stores them in the deck given
     * by deckName and the DataStorage module given by ds. 
     * If the deck doesn't already exist, a new deck with the given name is create
     * If the deck isn't specified, cards will go into the default deck
     * @param fileName The name of the file to be imported from.
     * @param ds The DataStorage module to store the new cards
     * @param deckName The name of the deck the new cards should go into
     * @return The list of cards that were imported.
     * @throws Exception 
     */
    public abstract void importCards(String fileName, DataStorage ds, String deckName) throws Exception;
}
