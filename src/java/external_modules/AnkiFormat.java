package external_modules;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import model.Card;
import model.DataStorage;
import model.Deck;
import model.SideReader;

/**
 * Represents a class which is capable of importing
 * flashcards from Anki files and exporting to Anki.
 * @author Mitchell
 *
 */
public class AnkiFormat extends ExternalFormat implements SideReader {    
    private static final String ANKI_TEXT_FILE = "ankiFormat.txt";
    private static final String ANKI_MEDIA_FILE = "collection.media";
    private PrintStream ankiTextOutputStream;
    private File ankiMediaFolder;
    // count is used to produce unique filenames
    private int imgCount;   
    private static final Logger LOGGER = Logger.getLogger( AnkiFormat.class.getName() );
    
    /**
     *  Constructs a new AnkiFormat.
     */
    public AnkiFormat() {
        super(".txt", ".txt");
        
        ankiTextOutputStream = null;
        ankiMediaFolder = null;
        imgCount = 0;    
    }   

    @Override
    public void exportCards(String dirPath, List<Card> cards) throws Exception {
        // create the output file, output directory for media
            File ankiTextFile = new File(dirPath, ANKI_TEXT_FILE);
            if (!ankiTextFile.exists()) {
                try {
                    ankiTextFile.createNewFile();
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Could not create anki text file.", e);
                    throw new Exception("Could not create Anki file in requested directory.");
                }
            }
            
            try {
                ankiTextOutputStream = new PrintStream(ankiTextFile);
            } catch (FileNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Could not write to anki text file: \"" 
                        + ankiTextFile.getName() + " in directory: " 
                        + ankiTextFile.getAbsolutePath() + "\"", e);
                throw new Exception("Could not write to Anki export file.");
            }
            ankiMediaFolder = new File(dirPath, ANKI_MEDIA_FILE);
            
            // create the media output folder if it does not exist
            if (!ankiMediaFolder.exists()) {                
                if(!ankiMediaFolder.mkdirs()) {
                    LOGGER.log(Level.SEVERE, "Could not create anki media directory: \"" 
                            + ankiTextFile.getName() + " in directory: " 
                            + ankiTextFile.getAbsolutePath() + "\""); 
                    throw new Exception("Could not create Anki media directory.");
                }
            }
                   
            // write each card to the file
            for (Card card : cards) {
                if (card == null) {
                    throw new RuntimeException("card == null");
                }
                card.readFront(this);
                card.readBack(this);
                outputTags(card.getTags());
            }
            
            ankiTextOutputStream.close();
            imgCount = 0;
    }

    @Override
    //Note: is the media folder isn't squished in with the fileName, method assumes
    //the default "collection.media" folder
    public void importCards(String fileName, DataStorage ds, String deckName) throws Exception {
    	BufferedReader br;
    	String mediaFolder;
    	if (fileName.contains("%")) {
    		mediaFolder = fileName.substring(fileName.lastIndexOf("%") + 1);
    		fileName = fileName.substring(0, fileName.lastIndexOf("%"));
    	} else {
    		mediaFolder = "";
    	}
    	//if no deckName given, assume the default deck
    	if (deckName.equals("")) {
    		deckName = "Default";
    	}
    	File ankiFile = new File(fileName);
    		String parentStr = ankiFile.getParent();
    		br = new BufferedReader(new FileReader(new File(fileName)));
    		String line;
			List<Card> cards = new ArrayList<Card>();
    		while ((line = br.readLine()) != null) {
    			String[] split = line.split("\t", -1);
    			if (split.length < 2) {
    				throw new Exception("Improperly formatted anki file: must be tab separated.");
    			}
    			BufferedImage frontImg = null;
    			BufferedImage backImg = null;
    			String frontImgExt = "";
    			String backImgExt = "";
    			
    			String q = split[0];
    			if (q.replaceAll("<div>", "").startsWith("<img src=")) {
    				File imgDir;
    				if (mediaFolder != "") {
    					imgDir = new File(mediaFolder);
    				} else {
    					imgDir = new File(parentStr, ANKI_MEDIA_FILE);
    				}
    				// verify that the media folder exists
    	    		if (!imgDir.exists()) {
    	    			throw new Exception("Couldn't locate image directory for anki file");
    	    		}
    	    		String imgLoc = imgDir + "/" + getImgLocFromLine(q);
    				frontImgExt = imgLoc.replaceFirst(".*\\.", "");
    				//throws IOException if image not found
    				try {
    					frontImg = ImageIO.read(new File(imgLoc));
    				} catch (IOException e) {
    					throw new Exception("Couldn't find image in given folder");
    				}
    			} else {
    				//split on <div>
    				String[] frontSplit = q.split("<div>");
    				q = "";
    				for (String s: frontSplit) {
        				//replace any <br />s with newlines
    					s.replaceAll("<br />", "\n");
    					//delete all </div>s
    					s.replaceAll("</div>", "");
    					//merge together results
    					q = q.concat(s);
    				}
    			}
    			String a = split[1];
    			if (a.replaceAll("<div>", "").startsWith("<img src=")) {
    				File imgDir;
    				if (mediaFolder != "") {
                        imgDir = new File(mediaFolder);
                    } else {
                        imgDir = new File(parentStr, ANKI_MEDIA_FILE);
                    }
                    // verify that the media folder exists
    	    		if (!imgDir.exists()) {
    	    			throw new FileNotFoundException("Couldn't locate image directory for anki file");
    	    		}
    	    		String imgLoc = imgDir + "/" + getImgLocFromLine(a);
    				backImgExt = imgLoc.replaceFirst(".*\\.", "");
    				//throws IOException if image not found
    				try {
    					backImg = ImageIO.read(new File(imgLoc));
    				} catch (IOException e) {
    					throw new Exception("couldn't find image in given folder");
    				}
    				
    			} else {
    				//split on <div>
    				String[] backSplit = a.split("<div>");
    				a = "";
    				for (String s: backSplit) {
        				//replace any <br />s with newlines
    					s = s.replaceAll("<br />", "\n");
    					//delete all </div>s
    					s = s.replaceAll("</div>", "");
    					//merge together results
    					a = a.concat(s);
    				}
    			}
    			Set<String> tags = new HashSet<String>();
    			for (int i = 2; i < split.length; i++) {
    				tags.add(split[i]);
    			}
    			Card c;
    			if (frontImg == null && backImg == null) {
    				//text front, text back
    				c = new Card(q, a, tags);
    			} else if (frontImg != null && backImg == null) {
    				//image front, text back
    				c = new Card(frontImg, frontImgExt, a, tags);
    			} else if (frontImg == null && backImg != null) {
    				//text front, image back
    				c = new Card(q, backImg, backImgExt, tags);
    			} else {
    				// img front, img back
    				c = new Card(frontImg, frontImgExt, backImg, backImgExt, tags);
    			}
    			cards.add(c);
    		}
    		br.close();
    		Deck deck;
    		try {
    			deck = ds.getDeck(deckName);
    		} catch (IllegalArgumentException e) {
    			//deck doesn't already exist - create a new one
    			ds.newDeck(deckName);
    			deck = ds.getDeck(deckName);
    		}
			ds.addCardsToDeck(deck, cards);
    }
    
    private String getImgLocFromLine(String line) {
    	String imgLoc = line.replaceAll("<div>", "").replaceFirst("<img src=\"", "");
		imgLoc = imgLoc.replaceFirst("\".*", "");
		return imgLoc;
    }
    
    /**
     * Creates an HTML img tag of an image filename. Plaintext Anki 
     * exports requires images be exported as HTML img tags. The img
     * src references an image in the respective collection.media folder.
     * @requires imgName != null
     * @param imgName HTML img tag formatted as a string.
     */
    String createImgTag(String imgName) {
        return "<img src=\"" + imgName + "\" />";
    }
    
    /**
     * Outputs the set of flashcard tags to the current
     * line in the ankiTextOutputStream.
     * @param tags Set of flashcard tags.
     */
    private void outputTags(Set<String> tags) {
        String[] allTags = new String[tags.size()];
        tags.toArray(allTags);
        
        if (allTags.length > 0) {
            ankiTextOutputStream.print("\t");
            
            // fence post problem: create space between
            // tags, new line character after last tag
            for (int i = 0; i < allTags.length - 1; i++) {
                ankiTextOutputStream.print(allTags[i]);
                ankiTextOutputStream.print(" ");
            }
            ankiTextOutputStream.print(allTags[allTags.length - 1]);
        }
        ankiTextOutputStream.print("\n");
    }
    
    /**
     * Writes an image to the collections.media folder.
     * @param buffImg The BufferedImage to write.
     * @param fileExt The file extension of the image.
     */
    private void writeImage(BufferedImage buffImg, String fileExt) {
        try {
            String imgName = "img" + imgCount + "." + fileExt;
            File cardImage = new File(ankiMediaFolder, imgName);
            ImageIO.write(buffImg, fileExt, cardImage);
            imgCount++;
            ankiTextOutputStream.print(createImgTag(cardImage.getName()));
        } catch (Exception e) {
            e.printStackTrace();
        }       
    }
    
    /*
     *  Outputs text to delineate the start of
     *  an Anki text flashcard. 
     */
    private void printCardStart() {
        ankiTextOutputStream.print("<div>");
    }
    
    /*
     *  Outputs text to delineate the end of
     *  an Anki text flashcard. 
     */
    private void printCardEnd() {
        ankiTextOutputStream.print("</div>");
    }


    @Override
    public void readFrontSide(BufferedImage imgFront, String fileExt) {
        printCardStart();
        writeImage(imgFront, fileExt);
        ankiTextOutputStream.print("\t");
    }

    @Override
    public void readFrontSide(String textFront) {
        printCardStart();
        // escape all newline characters with <br /> tag
        textFront = textFront.replaceAll("\n", "<br />");
        ankiTextOutputStream.print(textFront + "\t");
    }

    @Override
    public void readBackSide(BufferedImage imgBack, String fileExt) {
        writeImage(imgBack, fileExt);     
        printCardEnd();
    }

    @Override
    public void readBackSide(String textBack) {
        // escape all newline characters with <br /> tag
        textBack = textBack.replaceAll("\n", "<br />");
        ankiTextOutputStream.print(textBack);
        printCardEnd();
    }
}
