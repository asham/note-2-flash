package external_modules;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import model.Card;
import model.DataStorage;
import model.Deck;
import model.QueryProcessor;

/**
 * Represents a class which is capable of importing
 * and export Note2Flash formats
 * @author Tianchi, Mitchell
 *
 */
public class Note2FlashFormat extends ExternalFormat {
    private static final String TEMP_DATABASE_LOCATION = "src/java/external_modules";
    private static final String DATABASE_COPY_NAME = "note2flash_db_copy";
    private static final String FILE_EXT = ".mv.db";
    private static final Logger LOGGER = Logger.getLogger( Note2FlashFormat.class.getName() );
    
    private static Path tempDir;
    
    /**
     *  Constructs a new Note2FlashFormat.
     */
    public Note2FlashFormat() {
        super(".db", ".db");
    }

    @Override
    public void exportCards(String filePath, List<Card> cards) throws Exception {
        LOGGER.log(Level.INFO, "Exporting to Note2Flash, location: " 
                + filePath + ", # of cards: " + cards.size());
       
        try {                      
            if (new File(filePath, DATABASE_COPY_NAME + FILE_EXT).exists()) {
                throw new FileAlreadyExistsException("\"" + DATABASE_COPY_NAME 
                        + "\" already exists  in the export directory.");
            }
            
            // save the ID's for each card so we can restore them
            // after the export has completed
            List<Integer> idList = new ArrayList<Integer>();
            for (int i = 0; i < cards.size(); i++) {
                idList.add(cards.get(i).getID());
            }     
            
            /*
             * The database is initially exported to a temporary location.
             * This is because when we call changeSaveLocation on 
             * QueryProcessor it only allows relative file locations. 
             * 
             * After the export has completed then we move all of these 
             * database files to the directory that the user specified.
             */
            
            // create temporary database location
            QueryProcessor ds = QueryProcessor.getInstance();
            tempDir = Files.createTempDirectory(Paths.get(TEMP_DATABASE_LOCATION), null);

            Path fullCopyName = Paths.get(tempDir.toAbsolutePath().toString(), 
                                          DATABASE_COPY_NAME);
            
            // save reference to current database
            String currDatabaseLoc = ds.getFileLocation();
            ds.changeSaveLocation(fullCopyName.toString());
            
            // export all cards to the new database
            for (Card card: cards) {
                ds.newCard(card);
            }
            
            // save cards to the new database
            Deck defaultDeck = new Deck("DeckCopy");
            defaultDeck.addCards(cards);

            // restore reference to original DataStorage
            ds.changeSaveLocation(currDatabaseLoc);
            
            // restore card ID's
            for (int i = 0; i < cards.size(); i++) {
                cards.get(i).setID(idList.get(i));
            }            
            
            // move exported files to requested location
            Path dstDir = Paths.get(filePath);                      
            String[] allFiles = new File(tempDir.toUri()).list();            
            for (String fileName: allFiles) {
                Files.move(Paths.get(tempDir.toString(), fileName), 
                           Paths.get(dstDir.toString(), fileName));
            }     
            
            // delete temp folder           
            Files.delete(tempDir);
        } catch (FileAlreadyExistsException e) {
            LOGGER.log(Level.SEVERE, "Could not create Note2Flash export file.", e);
            throw new Exception("A Note2Flash file with the name \"" + DATABASE_COPY_NAME 
                    + "\" already exists in this directory." + "\n" + "Please choose another export directory.");
        } catch (IOException e) {
            // delete all temporarily created files
            String[] allFiles = new File(tempDir.toUri()).list();            
            for (String fileName: allFiles) {
                Files.delete(Paths.get(tempDir.toString(), fileName));
            }          
            
            // delete temp folder           
            Files.delete(tempDir);
            
            LOGGER.log(Level.SEVERE, "Could not create Note2Flash export file.", e);
            throw new Exception(e.getMessage());
        }       
    }

    @Override
    public void importCards(String fileName, DataStorage ds, String newName) {
    	
    	if (fileName.contains(".mv.db")) {
    		fileName = fileName.substring(0, fileName.length() - 6);
    	}
    	
        Connection con;

        // Try to open data base file
        if (fileName == null) {
            throw new IllegalArgumentException("database name is null");
        }
        if (newName == null) {
            throw new IllegalArgumentException("new deck name is null");
        }
        try {
            Class.forName("org.h2.Driver");
            fileName = QueryProcessor.DB_PREFIX + fileName;
            con = DriverManager.getConnection(fileName);
            //initializeTables();
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Class not found: " + e);
        }
        catch (SQLException e) {
            throw new IllegalStateException("Error connecting to database: ", e);
        }

        // Get the list of deck names
        List<String> listOfDecks = new ArrayList<String>();
        try (PreparedStatement getDecks = con.prepareStatement(QueryProcessor.GET_DECK_NAMES)) {
            ResultSet result = getDecks.executeQuery();
            while (result.next()) {
                String deckName = result.getString("deckName");
                listOfDecks.add(deckName);
            }
        } catch (SQLException e) {
            throw new IllegalStateException("could not get deck ", e);
        }

        // Get list of cards
        // Add them to local db as well

        List <Card> cards = new ArrayList<Card>();
        for (String deckName : listOfDecks) {
            // grab all cards in this deck
            ResultSet rs = null;
            try (PreparedStatement getCardsInDeck = con.prepareStatement(QueryProcessor.GET_CARDS_IN_DECK)) {
                getCardsInDeck.setString(1, deckName);
                rs = getCardsInDeck.executeQuery();
                cards.addAll(constructCard(con, rs));
            } catch (SQLException e) {
                throw new IllegalStateException("could not get cards ", e);
            } catch (IOException e) {
                throw new IllegalStateException("could not read image ", e);
            }

        }
        Deck dk = new Deck(newName);
        dk.addCards(cards);
    }

    /**
     * Given a result set from db, construct a list of card
     *
     * @param rs result set from query
     * @return list of created cards
     * @throws SQLException sql error
     * @throws java.io.IOException io error
     */
    private List<Card> constructCard (Connection con, ResultSet rs) throws SQLException, IOException {
    	PreparedStatement getTags = con.prepareStatement(QueryProcessor.GET_TAG_BY_CID);
        List <Card> cards = new LinkedList<Card>();
        while(rs.next()) {
            //construct card
            BufferedImage imgFront = null;
            String imgFrontExt = null;
            if (rs.getBinaryStream(2) != null) {
                imgFront = ImageIO.read(rs.getBinaryStream(2));
                imgFrontExt = rs.getString(4);
            }
            BufferedImage imgBack = null;
            String imgBackExt = null;
            if (rs.getBinaryStream(5) != null) {
                imgBack = ImageIO.read(rs.getBinaryStream(5));
                imgBackExt = rs.getString(7);
            }
            Set <String> tags = new HashSet<String>();
            getTags.setInt(1, rs.getInt(1));
            ResultSet tagSet = getTags.executeQuery();
            while (tagSet.next()) {
                tags.add(tagSet.getString(2));
            }
            
            // While card is being constructed, it is recorded in local database as well
            Card c;
            if (imgFront == null && imgBack == null) {
            	c = new Card(rs.getString(3), rs.getString(6), tags);
            } else if (imgFront != null && imgBack == null) {
            	c = new Card(imgFront, imgFrontExt, rs.getString(6), tags);
            } else if (imgFront == null && imgBack != null) {
            	c = new Card(rs.getString(3), imgBack, imgBackExt, tags);
            } else {
            	c = new Card(imgFront, imgFrontExt, imgBack, imgBackExt, tags);
            }        
            
            cards.add(c);
        }
        getTags.close();
        return cards;
    }
}