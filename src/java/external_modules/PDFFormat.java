package external_modules;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Card;
import model.DataStorage;
import model.SideReader;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;

/**
 * Used to convert Card representation in Note 2 Flash to PDF
 * 
 * Takes in a list of card and outputs a printable PDF document
 * 
 * @author Adrian Sham
 *
 */
public class PDFFormat extends ExternalFormat{
    
    private static final Logger LOGGER = Logger.getLogger(PDFFormat.class.getName());
    
    private PDPage page;
    
    private PDPageContentStream contentStream;
    
    private int noOfCardsPerPage;
    
    private boolean isDoubleSided;
    
    private List<Point> printCoordinates;
    
    public static final String DEFAULT_FILE_NAME = System.getProperty("file.separator") + "Note2Flash.pdf";
    
    /**
     * Width of card
     */
    public static final int WIDTH = 250;
    
    /**
     * Height of card
     */
    public static final int HEIGHT = 150;
    
    /**
     * Space between cards
     */
    public static final int MARGIN = 30;
    
    /**
     * No of columns
     */
    public static final int COLUMN = 2;
    
    /**
     * Font size for pdf
     */
    public static final float FONT_SIZE = 12;
    
    /**
     * Creates new PDFFormat instance
     */
    public PDFFormat () {
        super(".pdf", null);
        noOfCardsPerPage = 4;
        isDoubleSided = false;
        printCoordinates = calculatePoints();
    }
    
    /**
     * Set the no of cards printed per page
     * 
     * @param noOfCardsPerPage no of cards per page
     * @throws IllegalArgumentException if noOfCardsPerPage <= 0
     */
    public void setNoOfCardsPerPage(int noOfCardsPerPage) {
        if (noOfCardsPerPage <= 0) {
            throw new IllegalArgumentException();
        }
        this.noOfCardsPerPage = noOfCardsPerPage;
        printCoordinates = calculatePoints();
    }
    
    /**
     * Returns the no of cards printed per page
     * 
     * @return no of cards per page
     */
    public int getNoOfCardsPerPage () {
        return noOfCardsPerPage;
    }
    
    /**
     * Returns whether cards will be printed in double sided manner
     * @return true if double sided
     */
    public boolean getIsDoubleSided () {
        return isDoubleSided;
    }
    
    /**
     * Set whether or not PDF page will created in double sided format
     * @param isDoubleSided
     */
    public void setDoubleSided(boolean isDoubleSided) {
        this.isDoubleSided = isDoubleSided;
    }

    @Override
    public void exportCards(String fileName, List<Card> cards) {
        //get the number of cards
        int numOfCards = cards.size();
        
        int noOfPages = (int)Math.ceil((double)numOfCards / (double)noOfCardsPerPage);
        
        //start creating pdf document
        try (PDDocument document = new PDDocument()){
           //keep track of the current card
           int currentCard = 0;
           boolean readFront = true;
           //work on each page of pdf
           for (int currentPage = 0; currentPage < noOfPages; currentPage++) {
               //set page size
               page = new PDPage(PDPage.PAGE_SIZE_LETTER);
               //add a page to the document
               document.addPage(page);
               //start up a new content stream, think of it as a paint brush for pdfs
               contentStream = new PDPageContentStream(document, page);
               //iterate through coordinates for printing cards on pdf
               Iterator <Point> coordinate = printCoordinates.iterator(); 
               //keep going until run out of cards or out of space in page
               while (currentCard < cards.size()) {
                   Card c = cards.get(currentCard);
                   if (readFront) {
                       //print front of card
                       Point printHere = coordinate.next();
                       CardReader cardReader = new CardReader(contentStream, document, printHere.x, printHere.y);
                       c.readFront(cardReader);
                       readFront = false;
                       continue;
                   } else {
                       //print back of card
                       Point printHere = coordinate.next();
                       CardReader cardReader = new CardReader(contentStream, document, printHere.x, printHere.y);
                       c.readBack(cardReader);
                       readFront = true;
                       currentCard++;
                   }
                   //ran out of space in this space, go to next page
                   if (currentCard % noOfCardsPerPage == 0) {
                       break;
                   }
               }
               contentStream.close();
           }
           document.save(fileName + DEFAULT_FILE_NAME);

        } catch (IOException | COSVisitorException e) {
            LOGGER.log(Level.SEVERE, "pdf creation failed", e);
        }
    }
    
    /**
     * Calculates the coordinates where cards should be written at each page
     * 
     * @return List of point
     */
    private List<Point> calculatePoints() {
        ArrayList <Point> points = new ArrayList<Point>();
        //start at x = 10, y = 10
        int x = MARGIN;
        int y = MARGIN;
        int counter = 0;
        for (int i = 0; i < noOfCardsPerPage * 2; i++) {
            points.add(new Point(x, y));
            counter++;
            if (counter % 2 == 1) {
                x = x + WIDTH + MARGIN;
            } else {
                x = x - WIDTH - MARGIN;
                y  = y + HEIGHT + MARGIN;
            }
        }
        Collections.reverse(points);
        //swap pairs
        for (int i = 0; i < points.size(); i = i+2) {
            Collections.swap(points, i, i+1);
        }
        return points;
    }

    /**
     * Used to "read" a side of a card
     * 
     * @author Adrian
     *
     */
    public static class CardReader implements SideReader {
        private PDPageContentStream contentStream;
        private float x;
        private float y;
        private PDDocument document;
        
        /**
         * Pass in some useful parameters so write on PDF
         * 
         * @param contentStream PDPageContentStream of document
         * @param document current document
         * @param x x coordinate to print card
         * @param y y coordinate to print card
         */
        public CardReader(PDPageContentStream contentStream,PDDocument document, 
                float x, float y) {
            
            this.contentStream = contentStream;
            this.x = x;
            this.y = y;
            this.document = document;
        }
        
        /**
         * Read front side of the card, and print picture onto page
         */
        @Override
        public void readFrontSide(BufferedImage imgFront, String fileExt) {
            //front side is image
            try {
                PDJpeg img = new PDJpeg(document, imgFront);
                contentStream.drawXObject(img, x, y, WIDTH, HEIGHT);
                drawCardBorder(contentStream, x, y, WIDTH, HEIGHT);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "read front side error", e);
            }
            
        }

        /**
         * Read front side of the card, and print text onto page
         */
        @Override
        public void readFrontSide(String textFront) {
            PDFont font = PDType1Font.HELVETICA_BOLD;
            try {
                writeTextInCard(contentStream, x, y, font, textFront);
                drawCardBorder(contentStream, x, y, WIDTH, HEIGHT);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "read front side error text", e);
            }
        }

        /**
         * Read back side of the card, and print picture onto page
         */
        @Override
        public void readBackSide(BufferedImage imgBack, String fileExt) {
            try {
                PDJpeg img = new PDJpeg(document, imgBack);
                contentStream.drawXObject(img, x, y, WIDTH, HEIGHT);
                drawCardBorder(contentStream, x, y, WIDTH, HEIGHT);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "read back side error", e);
            }
        }

        /**
         * Read back side of the card, and print text onto page
         */
        @Override
        public void readBackSide(String textBack) {
            PDFont font = PDType1Font.HELVETICA_BOLD;
            try {
                writeTextInCard(contentStream, x, y, font, textBack);
                drawCardBorder(contentStream, x, y, WIDTH, HEIGHT);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "read back side error text", e);
            }
        }
    }

    @Override
    public void importCards(String fileName, DataStorage ds, String deckName) {
        throw new UnsupportedOperationException("Method is unimplemented.");
    }

    /**
     * Draw a border for each card, given correct parameters
     * 
     * @param contentStream PDPageContentStream of document
     * @param x x coordinate of lower left corner of card
     * @param y y coordinate of lower left corner of card
     * @param width width of card
     * @param height height of card
     * @throws IOException IOException on error
     */
    private static void drawCardBorder(PDPageContentStream contentStream, float x, float y, 
            float width, float height) throws IOException {
        contentStream.addRect(x, y, width, height);
        contentStream.stroke();
    }
    
    /**
     * Prints out the text in a card, with word wrapping
     * 
     * @param contentStream PDPageContentStream of document
     * @param x x coordinate of lower left corner of card
     * @param y y coordinate of lower left corner of card
     * @param font width of card
     * @param text height of card
     * @throws IOException IOException or error
     */
    private static void writeTextInCard(PDPageContentStream contentStream, float x, 
            float y, PDFont font, String text) throws IOException {
        //top margin of card
        float heightMargin = 15;
        //left margin of card
        float widthMargin = 10;
        String copyOfText = text;
        ArrayList<String> lines = new ArrayList<String>();
        //if the text too long, break it down to smaller pieces
        int noOfLines = 0;
        while (font.getStringWidth(copyOfText)/1000*FONT_SIZE > WIDTH - widthMargin * 2) {
            noOfLines++;
            int cutHere = getSubstringByWidth(copyOfText, font, widthMargin);
            if (noOfLines >= 9) {
                String temp = copyOfText.substring(0, cutHere - 3).trim();
                String truncated = temp + "...";        
                lines.add(truncated);
                break;
            } else {
                lines.add(copyOfText.substring(0, cutHere).trim());
            }
            copyOfText = copyOfText.trim();
            copyOfText = copyOfText.substring(cutHere, copyOfText.length());
        }
        if (noOfLines < 9) {
            lines.add(copyOfText);
        }
        contentStream.beginText();
        contentStream.setFont(font, FONT_SIZE);
        //start writing text to card
        float startX = x + widthMargin;
        float startY = y + HEIGHT - heightMargin;
        //print each line of text
        for (String line : lines) {
            contentStream.moveTextPositionByAmount(startX, startY);
            contentStream.drawString(line);
            startY = -heightMargin;
            startX = 0;
        }
        contentStream.endText();
    }
    
    /**
     * Based on the width of the string, returns an index for which the string
     * should be cut so that the string is not over the width of the card
     * 
     * @param cutThis the string to be cut
     * @param font the font of the pdf document
     * @return index at which the string should be cut
     * @throws IOException
     */
    private static int getSubstringByWidth (String cutThis, PDFont font, float cardMargin) throws IOException {
        int i = 0;
        //find the longest substring possible that is smaller than width of card
        while (i < cutThis.length() && 
                font.getStringWidth(cutThis.substring(0, i))/1000 * FONT_SIZE < WIDTH-cardMargin*2) {
            i++;
        }
        //since the string failed the while loop, it is too big, decrement by one
        if (i != 0) {
            i--;
        }
        //try to cut on a space instead
        if (cutThis.lastIndexOf(' ', i) != -1) {
            i = cutThis.lastIndexOf(' ', i);
        }
        return i;
    }
}
