package main.GUI;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import main.GUI.Dialog.NewDeckDialog;

public class MainDeckView extends JPanel {

    // Default serial version number
    private static final long serialVersionUID = 1L;

    /**
     * Model for the JTree deck explorer
     */
    private DefaultTreeModel model;

    /**
     * Root node of the model
     */
    private DefaultMutableTreeNode root;

    private JTree tree;
    
    private JPanel thisPanel;
    
    /**
     * Constructs deck viewing portion of the GUI, as well as a toolbar containing functions related
     * to deck creation
     */
    public MainDeckView(List<String> decks) {
        setLayout(new BorderLayout(0, 0));

        thisPanel = this;
        
        JPanel panel = new JPanel();
        FlowLayout flowLayout = (FlowLayout) panel.getLayout();
        flowLayout.setAlignment(FlowLayout.LEFT);
        add(panel, BorderLayout.NORTH);

        JLabel lblNewLabel = new JLabel("Decks");
        panel.add(lblNewLabel);

        // Add buttons to menu bar
        JButton addDeck = new JButton("Add");
        panel.add(addDeck);
        addDeck.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Container c = thisPanel.getTopLevelAncestor();
                NewDeckDialog newDeckDialog = new NewDeckDialog(new Point(c.getX()+c.getWidth()/2, c.getY()+c.getHeight()/2));
                newDeckDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                newDeckDialog.setVisible(true);
                newDeckDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            }
        });
        JButton deleteDeck = new JButton("Delete");
        panel.add(deleteDeck);
        deleteDeck.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Main.mainController.deleteDeckButtonPressed();
            }
        });

        JPanel panel_1 = new JPanel();
        add(panel_1, BorderLayout.CENTER);
        panel_1.setLayout(new BorderLayout(0, 0));

        // Set up horizontal/vertical spaces
        Component horizontalStrutLeft = Box.createHorizontalStrut(20);
        panel_1.add(horizontalStrutLeft, BorderLayout.WEST);
        Component horizontalStrutRight = Box.createHorizontalStrut(20);
        panel_1.add(horizontalStrutRight, BorderLayout.EAST);
        Component verticalStrutDown = Box.createVerticalStrut(20);
        panel_1.add(verticalStrutDown, BorderLayout.SOUTH);

        // Add file explorer
        tree = new JTree(decks.toArray());

        model = (DefaultTreeModel) tree.getModel();
        root = (DefaultMutableTreeNode) model.getRoot();

        if(!SwingUtilities.isEventDispatchThread()) throw new IllegalStateException("illegal thread is modifying swing component");
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.addTreeSelectionListener( new TreeSelectionListener(){

            @Override
            public void valueChanged(TreeSelectionEvent arg0) {
                // TODO Auto-generated method stub
                
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
                
                // if nothing is selected
                if (node == null) return;
                
                // on startup the controller is null
                if (Main.mainController != null) {
                    Main.mainController.deckSelected(node.toString());
                }
            }

        });

        JScrollPane deckScroller = new JScrollPane(tree);
        deckScroller.setPreferredSize(new Dimension(GUIConstants.DECK_VIEW_WIDTH * 2 / 5, GUIConstants.DECK_VIEW_HEIGHT));
        deckScroller.getVerticalScrollBar().setUnitIncrement(GUIConstants.SCROLL_SPEED);
        deckScroller.getHorizontalScrollBar().setUnitIncrement(GUIConstants.SCROLL_SPEED);
        
        panel_1.add(deckScroller, BorderLayout.CENTER);

    }

    /**
     * Set the decks to be displayed in the JTree explorer
     * @param decks List of strings representing decks
     */
    public void setDecks(List<String> decks){
        if(!SwingUtilities.isEventDispatchThread()) throw new IllegalStateException("illegal thread is modifying swing component");
        root.removeAllChildren();
        for(String deck : decks){
            root.add(new DefaultMutableTreeNode(deck));
            
        }
        model.reload(root);
    }

    /**
     * Sets the selected row of the JTree explorer.
     * @param index Index of the row to select.
     */
    public void setSelectedDeckRow(int index) {
        tree.setSelectionRow(index);
    }
}
