package main.GUI;

import controller.Controller;

/**
 * Main entry point into program
 * @author adrian
 */
public class Main {

    public static Controller mainController;

    /**
     * Main method for running the program. Creates the MainWindowGUI.
     * @param args
     */
    public static void main (String[] args) {
        // Everything starts here
        mainController = new Controller(false);
    }

    /**
     * Temporary method for testing the unit test framework
     * @param a
     * @param b
     * @return
     */
    public static int addition(int a, int b) {
        return a+ b;
    }
}
