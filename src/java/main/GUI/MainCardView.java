package main.GUI;


import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import main.GUI.Dialog.AddMultipleCardsDialog;
import main.GUI.Dialog.EditCardDialog;
import main.GUI.Dialog.NewCardDialog;
import model.Card;

/**
 * Main card view window of the application 
 * @author Mitchell, Tianchi
 */

public class MainCardView extends JPanel {

    // Default serial version number
    private static final long serialVersionUID = 1L;

    /**
     * The panel containing the CardDisplay panels
     */
    private static JPanel cardPanel;
    
    /**
     * The CardDisplays currently being displayed on the main window
     */
    private static List<CardDisplay> currentlyDisplaying;
    
    /**
     * Combo box for selecting tags to filter cards by
     */
    private JComboBox<String> filterBox;

    /**
     * Construct the card view portion of the main GUI, which displays the
     * flash cards, and a toolbar with operations related to cards.
     */
    
    private JPanel thisPanel;
    
    public MainCardView(List<CardDisplay> startingCards) {

        thisPanel = this;
        
        currentlyDisplaying = startingCards;

        setBackground(UIManager.getColor("Button.background"));
        setLayout(new BorderLayout(0, 0));

        JPanel panel = new JPanel();
        add(panel, BorderLayout.NORTH);

        JLabel lblCards = new JLabel("Cards");
        panel.add(lblCards);

        // Add buttons to toolbar, Add button
        JButton addCard = new JButton("Add");          
        addCard.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Container c = thisPanel.getTopLevelAncestor();
                NewCardDialog newCardDialog = new NewCardDialog(new Point(c.getX()+c.getWidth()/2, c.getY()+c.getHeight()/2));
                newCardDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                newCardDialog.setVisible(true);
                newCardDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            }
        });
        panel.add(addCard);

        // Edit button
        JButton editCard = new JButton("Edit");
        editCard.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Main.mainController.editCardButtonPressed();
            }
        });
        panel.add(editCard);

        // Remove button
        JButton removeCard = new JButton("Remove");
        removeCard.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent arg0) {
                Main.mainController.removeCardButtonPressed();
            }        
        });
        
        panel.add(removeCard);

        // Add from image button
        JButton addFromImage = new JButton("Add From Image");
        addFromImage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Container c = thisPanel.getTopLevelAncestor();
                AddMultipleCardsDialog multipleCardsDialog = new AddMultipleCardsDialog(new Point(c.getX()+c.getWidth()/2, c.getY()+c.getHeight()/2));
                multipleCardsDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                multipleCardsDialog.setVisible(true);
                multipleCardsDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            }
        });
        panel.add(addFromImage);

        // Select all
        JButton selectAllButton = new JButton("Select");
        selectAllButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                // Select marks all cards as selected, unless any card is already selected
                // if so, it will mark them all unselected
                
                boolean oneSelected = false;
                for(CardDisplay cd: currentlyDisplaying){
                    if(cd.isSelected()){
                        oneSelected = true;
                        break;
                    }
                }
                for(CardDisplay cd: currentlyDisplaying){
                    cd.setSelected(!oneSelected);
                    cd.updateBorders();
                }
                
                Main.mainController.updateCardView();
            }
            
        });
        panel.add(selectAllButton);
        
        // Filter 
        JLabel filter = new JLabel("Filter");
        panel.add(filter);

        ComboBoxModel<String> defaultComboBoxModel = new DefaultComboBoxModel<String>(new String[] {""});

        filterBox = new JComboBox<String>(defaultComboBoxModel);
        filterBox.setSelectedItem(null);
        filterBox.setMaximumRowCount(10);
        filterBox.setEditable(true);
        filterBox.getEditor().addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent action) {       
                // controller can be null at the beginning if this is called before
                // it is done being constructed                    
                if (Main.mainController == null){
                    return;
                }
                
                // should only execute this event when the combo box
                // has been edited, not changed
                if (!action.getActionCommand().equals("comboBoxChanged")) {                    
                    String tags = (String) filterBox.getEditor().getItem();
                    if (tags != null) {                    
                        tags = tags.trim();
                        Main.mainController.filterCardsAction(tags);
                    }
                }
            }               
        });
        filterBox.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent action) {
                // controller can be null at the beginning if this is called before
                // it is done being constructed                    
                if (Main.mainController == null){
                    return;
                }
                
                // should only execute this event when the combo box
                // has been changed, not edited
                if (!action.getActionCommand().equals("comboBoxEdited")) {
                    String tags = (String) filterBox.getSelectedItem();
                    if (tags != null) {                    
                        Main.mainController.filterCardsAction(tags);
                    }
                }
            }
            
        });
        panel.add(filterBox);
  
        cardPanel = new JPanel(new WrapLayout(WrapLayout.LEFT));

        // Add cards pool to display cards
        JScrollPane scrollPane = new JScrollPane(cardPanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(GUIConstants.SCROLL_SPEED);
        scrollPane.setPreferredSize(new Dimension(GUIConstants.MAIN_WINDOW_WIDTH - GUIConstants.DECK_VIEW_WIDTH, GUIConstants.MAIN_WINDOW_HEIGHT - 50));

        // disabled horizontal scroll bar since it sometimes appears randomly for half a second when flipping cards
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        setCards(currentlyDisplaying);

        add(scrollPane);
    }

    /**
     * Returns the cards currently selected in GUI
     * @return A list of the currently selected CardDisplays
     */
    public List<CardDisplay> getSelectedCards(){

        List<CardDisplay> result = new ArrayList<CardDisplay>();

        for(CardDisplay cd : currentlyDisplaying){
            if(cd.isSelected()){
                result.add(cd);
            }
        }
        return result;
    }
    
    /**
     * Sets the cards that are currently being displayed by the GUI
     * @param newCards
     */
    public void setCards(List<CardDisplay> newCards){
        
        currentlyDisplaying = newCards;

        cardPanel.removeAll();

        for(CardDisplay card : currentlyDisplaying){
            if(!card.isFlipped()){
                cardPanel.add(card.getFrontSide());
            } else {
                cardPanel.add(card.getBackSide());
            }
        }

        revalidate();
        repaint();

    }
    
    /**
     * Updates the CardDisplays to display the correct card sides
     */
    public void updateCards(){
        cardPanel.removeAll();

        for(CardDisplay card : currentlyDisplaying){
            if(!card.isFlipped()){
                cardPanel.add(card.getFrontSide());
            } else {
                cardPanel.add(card.getBackSide());
            }
        }

        revalidate();
        repaint();
    }
    
    public void setTags(Set<String> tags){

        tags.add("");
        
        String[] tagArray = tags.toArray(new String[tags.size()]);
        
        ComboBoxModel<String> model = new DefaultComboBoxModel<String>(tagArray);
        filterBox.setModel(model);
        
        revalidate();
        repaint();
    }

    public void openEditDialog(List<Card> oldCards){
        Container c = thisPanel.getTopLevelAncestor();
        for(int i=0; i<oldCards.size(); i++){
            EditCardDialog editCardDialog = new EditCardDialog(new Point(c.getX()+c.getWidth()/2, c.getY()+c.getHeight()/2), oldCards.get(i));
            if(i==0) editCardDialog.disablePrevCard();
            if(i==oldCards.size()-1) editCardDialog.disableNextCard();
            editCardDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            editCardDialog.setVisible(true);
            editCardDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            editCardDialog.getGoBack();
            if(editCardDialog.getGoBack()) i-=2;
            if(editCardDialog.getClose()) break;
            editCardDialog.dispose();
        }
    }
}
