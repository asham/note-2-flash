package main.GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JPanel;

/**
 * JPanel class containing a single image
 * needed for the ability to select portions to make cards from
 * @author Trey
 *
 */
public class ImageSelectionPanel extends JPanel {

    /**
     * Required serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * If drawingTable is true, then the user has clicked on the "Table Mode" button.
     * If it is false, then the user has clicked on the "Select Mode" button.
     */
    private Boolean drawingTable;
    
    /**
     * List of points containing all of the dividers used to separate the
     * boxes which will become the front and back of flash cards
     */
    private List<Point> dividers;
    
    /**
     * List of points used in the Selection Mode. Points are coupled to make
     * the bounding boxes for the front and back of flash cards
     */
    private List<Point> selections;
    /**
     * The start point of the selection
     */
    private Point startPoint;
    
    /**
     * The end point of the selection
     */
    private Point endPoint;
    
    /**
     * The image displayed by this panel
     */
    private BufferedImage image;
    
    /**
     * Create a new ImageSelectionPanel with the given image 
     * @param image
     */
    public ImageSelectionPanel(BufferedImage image){
        this.image = image;
        this.drawingTable = false;
        dividers = new ArrayList<Point>();
        selections = new ArrayList<Point>();
    }
    
    /**
     * Set the image held by this ImageSelectionPanel
     * @param image
     */
    public void setImage(BufferedImage image){
        this.image = image;
    }

    /**
     * Add a divider point to the ImageSelectionPanel
     * @param p The point to use as a divider
     */
    public void addDivider(Point p){
        if(startPoint != null && endPoint != null) {
            if(p.x > startPoint.x && p.x < endPoint.x){
                if(Math.abs(endPoint.y - p.y) < 20){
                    Collections.sort(dividers, new PointComparator());
                    dividers.remove(dividers.size()-1);
                    dividers.add(new Point(p.x, endPoint.y));
                    Collections.sort(dividers, new PointComparator());
                } else if( p.y > startPoint.y && p.y < endPoint.y ){
                    dividers.add(p);
                    Collections.sort(dividers, new PointComparator());
                }
            }
        }
    }
    
    /**
     * Remove a divider at the given location
     * @param p The divider point to remove
     */
    public void removeDivider(Point p){
        Point closestDivider = null;
        for(Point divider:dividers){
            
            if((closestDivider == null || p.distance(divider) < p.distance(closestDivider)) && divider.y != endPoint.y){
                closestDivider = divider;
            }
            
        }
        
        if(closestDivider != null && p.distance(closestDivider) < 20) dividers.remove(closestDivider);
    }
    
    /**
     * Set the start point for the selection
     * @param p The point to use as the start point
     */
    public void setStartPoint(Point p){
        startPoint = p;
        endPoint = null;
    }
    
    /**
     * Set the end point for the selection
     * @param p The point to use as the end point
     */
    public void setEndPoint(Point p){
        if(p.x > startPoint.x && p.y > startPoint.y){
            if(image != null && p.x < image.getWidth() && p.y < image.getHeight()){
                Point newEndpoint = p;
                dividers.clear();
                endPoint = newEndpoint;
                dividers.add(new Point(startPoint.x + (newEndpoint.x-startPoint.x)/2, newEndpoint.y));
            }
        }
    }
    
    /**
     * Adds the current front and end point to the selections list
     */
    public void flushSelection(){
        if(!drawingTable){
            if(startPoint != null && endPoint != null){
                System.out.println("flushed to selections");
                selections.add(startPoint);
                selections.add(endPoint);
            }
            startPoint = null;
            endPoint = null;
        }
    }
    
    /**
     * Clears any selection made
     */
    public void clearSelection(){
        dividers.clear();
        selections.clear();
        startPoint = null;
        endPoint = null;
    }
    
    /**
     * Change to selection mode
     */
    public void drawSelection(){
        drawingTable = false;
        clearSelection();
        revalidate();
        repaint();
    }
    
    /**
     * Change to table mode
     */
    public void drawTable(){
        drawingTable = true;
        clearSelection();
        revalidate();
        repaint();
    }
    
    /**
     * Getter method to check if the user is in table or selection mode
     * @return true if the user is in table mode
     */
    public Boolean isDrawingTable(){
        return drawingTable;
    }
    
    /**
     * Returns the images on the left side of the selection rectangle
     * ordered from top to bottom
     * @return A list of images representing the front of the cards
     */
    public List<BufferedImage> getFrontList(){
        List<BufferedImage> result = new ArrayList<BufferedImage>();
        if(drawingTable){
            if(startPoint != null && endPoint != null){
                    Point currentRow = startPoint;
                    for(Point divider: dividers){
                        result.add(getFront(currentRow, divider));
                        currentRow = new Point(currentRow.x, divider.y);
                    }
            }
        } else {
            for(int i=0; i+3<selections.size(); i+=4){
                result.add(getFront(selections.get(i), selections.get(i+1)));
            }
        }       
        return result;
    }
    
    /**
     * Get the front image from a row
     * @param row The top left corner of the row
     * @param divider The divider point of the row
     * @return The image created from the two points
     */
    public BufferedImage getFront(Point row, Point divider){
        BufferedImage result = this.image.getSubimage(row.x, row.y, divider.x - row.x,
                divider.y - row.y);
        return result;
    }
    
    /**
     * Returns the images on the right side of the selection rectangle
     * ordered from top to bottom
     * @return A list of images representing the back of the cards
     */
    public List<BufferedImage> getBackList(){
        List<BufferedImage> result = new ArrayList<BufferedImage>();
        if(drawingTable){
            if(startPoint != null && endPoint != null){
                    Point currentRow = startPoint;
                    for(Point divider: dividers){
                        result.add(getBack(currentRow, divider));
                        currentRow = new Point(currentRow.x, divider.y);
                    }
            }
        } else {
            for(int i=2; i+1<selections.size(); i+=4){
                result.add(getFront(selections.get(i), selections.get(i+1)));
            }
        }
        selections.clear();
        return result;
    }
    
    /**
     * Get the back image from a row denoted by the given points
     * @param row
     * @param divider
     * @return An image representing the back of the card
     */
    public BufferedImage getBack(Point row, Point divider){
        // the bottom right point of the row
        Point rowBottomRight = new Point(endPoint.x, divider.y);
        
        BufferedImage result = this.image.getSubimage(divider.x, row.y, rowBottomRight.x - divider.x, rowBottomRight.y - row.y);
        return result;
    }
    
    /**
     * Overridden paintComponent class from JPanel, paints the image
     * onto the panel
     * @param g The graphics object to use to paint the image
     */
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        if(image != null){
            g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
        }
        g.setColor(Color.RED);
        if(startPoint != null && endPoint != null){
            if(drawingTable){
                g.drawRect(startPoint.x, startPoint.y, endPoint.x-startPoint.x, endPoint.y-startPoint.y);
                Point lastDivider = startPoint;
                for(Point divider:dividers){
                    g.fillOval(divider.x-2, divider.y-2, 4, 4);
                    g.drawRect(startPoint.x, lastDivider.y, divider.x - startPoint.x, divider.y - lastDivider.y);
                    g.drawRect(divider.x, lastDivider.y, endPoint.x - divider.x, divider.y - lastDivider.y);
                    lastDivider = divider;
                }
            } else {
                g.drawRect(startPoint.x, startPoint.y, endPoint.x-startPoint.x, endPoint.y-startPoint.y);
            }
        }
        for(int i=0; i<selections.size(); i+=2){
            g.setColor(Color.GREEN);
            if(selections.size() % 4 == 2 && i == selections.size() - 2) g.setColor(Color.RED);
            Point firstPoint = selections.get(i);
            Point secondPoint = selections.get(i+1);
            g.drawRect(firstPoint.x, firstPoint.y, secondPoint.x-firstPoint.x, secondPoint.y-firstPoint.y);
        }

    }
    
    /**
     * Returns the current number of selections
     * @return The current number of selections
     */
    public int selectionsCount(){
        if(selections == null){
            return 0;
        } else { 
            return selections.size() / 2;
        }
    }
    
    /**
     * Returns true if the selection is valid, false otherwise
     * @return True if the points in the selection are within the image bounds, false otherwise
     */
    public boolean pointsValid(){
        if(image == null || startPoint == null || endPoint == null){
            return false;
        }
        return startPoint.x < image.getWidth() && startPoint.y < image.getHeight() &&
                endPoint.x < image.getWidth() && endPoint.y < image.getHeight();
    }
    
    /**
     * Custom comparator class for points in selection
     * @author Will
     */
    static class PointComparator implements Comparator<Point>{

        @Override
        public int compare(Point arg0, Point arg1) {
            return ((Integer)arg0.y).compareTo((Integer)arg1.y);
        }
        
    }
}
