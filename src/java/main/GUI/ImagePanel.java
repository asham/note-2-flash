package main.GUI;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * JPanel class containing a single image
 * @author Trey
 *
 */
public class ImagePanel extends JPanel{

    /**
     * Required serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * The image to display
     */
    protected BufferedImage image;

    /**
     * Create a new ImagePanel with the given image 
     * @param image
     */
    public ImagePanel(BufferedImage image){
        this.image = image;
        
    }


    /**
     * Overriden paintComponent class from JPanel, paints the image
     * onto the panel
     * @param g The graphics object to use to paint the image
     */
    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        //g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        if(image != null)
        {
            int height = image.getHeight();
            int width = image.getWidth();
            if(height > width) {
                int newHeight = getHeight();
                double ratio = (double)newHeight / (double)height;
                int newWidth = (int)((double)width * ratio);
                if(newWidth > getWidth())
                {
                    newWidth = getWidth();
                    ratio = (double)newWidth / (double)width;
                    newHeight = (int)((double)height * ratio);
                }
                g.drawImage(image, (getWidth() - newWidth)/2, (getHeight() - newHeight)/2, newWidth, newHeight, null);
            } else {
                int newWidth = getWidth();
                double ratio = (double)newWidth / (double)width;
                int newHeight = (int)((double)height * ratio);
                if(newHeight > getHeight())
                {
                    newHeight = getHeight();
                    ratio = (double)newHeight / (double)height;
                    newWidth = (int)((double)width * ratio);
                }
                g.drawImage(image, (getWidth() - newWidth)/2, (getHeight() - newHeight)/2, newWidth, newHeight, null);
            }
        }
        
    }

}
