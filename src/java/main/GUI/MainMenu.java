package main.GUI;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import main.GUI.Dialog.ExportCardDialog;
import main.GUI.Dialog.ImportCardDialog;
import main.GUI.Dialog.NewCardDialog;
import main.GUI.Dialog.NewDeckDialog;
import main.GUI.Dialog.PrintPreviewDialog;

/**
 * Main menu toolbar for the application. Contains core operations 
 * of the application (e.g. saving, load, importing, etc.).
 * @author Mitchell
 */
public class MainMenu extends JMenuBar {
	
	// Default serial version id
	private static final long serialVersionUID = 1L;

	private JMenuBar thisMenu;
	/**
	 * Constructs a new MainMenu. 
	 */
	public MainMenu() {
	    thisMenu = this;
		// Create File submenu
		JMenu fileSubMenu = new JMenu("File");
		add(fileSubMenu);
		
		// Create File/New, File/New/Flashcard, File/New/Deck
		JMenu newFile = new JMenu("New");
		JMenuItem newFlashCard = new JMenuItem("Flashcard");
		newFlashCard.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                Container c = thisMenu.getTopLevelAncestor();
                NewCardDialog newCardDialog = new NewCardDialog(new Point(c.getX()+c.getWidth()/2, c.getY()+c.getHeight()/2));
                newCardDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                newCardDialog.setVisible(true);
                newCardDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            }
	    });
		newFile.add(newFlashCard);


        JMenuItem newDeck = new JMenuItem("Deck");
        newDeck.addActionListener(new ActionListener() {
        
            public void actionPerformed(ActionEvent e) {
                Container c = thisMenu.getTopLevelAncestor();
                NewDeckDialog newDeckDialog = new NewDeckDialog(new Point(c.getX()+c.getWidth()/2, c.getY()+c.getHeight()/2));
                newDeckDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                newDeckDialog.setVisible(true);
                newDeckDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            }
        });	
        newFile.add(newDeck);
        
		fileSubMenu.add(newFile);
		
		// Create File/Import
		JMenuItem importFile = new JMenuItem("Import");
		fileSubMenu.add(importFile);
		importFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Container c = thisMenu.getTopLevelAncestor();
                ImportCardDialog importCardDialog = new ImportCardDialog(new Point(c.getX()+c.getWidth()/2, c.getY()+c.getHeight()/2));
                importCardDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                importCardDialog.setVisible(true);
                importCardDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            }
        });
		
		// Create File/Export
		JMenuItem exportFile = new JMenuItem("Export");
		fileSubMenu.add(exportFile);
		exportFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Container c = thisMenu.getTopLevelAncestor();
                ExportCardDialog exportCardDialog = new ExportCardDialog(new Point(c.getX()+c.getWidth()/2, c.getY()+c.getHeight()/2));
                exportCardDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                exportCardDialog.setVisible(true); 
                exportCardDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            }
        });
		
		/*
		// Create File/Print
		JMenuItem printOption = new JMenuItem("Print");
		printOption.addActionListener(new PrintListener());
		fileSubMenu.add(printOption);
		*/
		
		// add horizontal space between menus
		add(Box.createHorizontalGlue());
		
		/*
		// Create Help submenu
		JButton helpSubMenu = new JButton("Help");
		helpSubMenu.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                Container c = thisMenu.getTopLevelAncestor();
                TutorialDialog dialog = new TutorialDialog(new Point(c.getX()+c.getWidth()/2, c.getY()+c.getHeight()/2));
                dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                dialog.setVisible(true); 
                dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            }                 
        });
		add(helpSubMenu);	
		*/	
	}
	
	/**
	 * A ListenerClass for the Print button witin the 
	 * MainMenu toolbar.
	 * @author Mitchell
	 */
    public class PrintListener implements ActionListener{

    	// open up the print dialog
        public void actionPerformed(ActionEvent arg0) {
            // to be modal, we need to have a reference to the parent window
            Container c = thisMenu.getTopLevelAncestor();
            PrintPreviewDialog printDialog = new PrintPreviewDialog(new Point(c.getX()+c.getWidth()/2, c.getY()+c.getHeight()/2));
            printDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
            printDialog.setVisible(true);
            printDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        }

    }
}
