package main.GUI.Dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.border.EmptyBorder;


/**
 * Dialog menu that users are prompted with 
 * to preview printed flashcards, apply formatting,
 * and print them.
 * @author Mitchell
 */
public class PrintPreviewDialog extends JDialog {

    private static final long serialVersionUID = 1L;

    private static final Dimension WINDOW_SIZE = new Dimension(600,450);
    
    /**
     * Constructs a new PrintPreviewDialog.
     * @param parent The main window screen of the GUI.
     * @param modal Makes dialog modal if true, false otherwise.
     */
    public PrintPreviewDialog(Point center) {
		setTitle("Print Preview");
		getContentPane().setLayout(new BorderLayout(0, 0));
		setBounds(center.x - WINDOW_SIZE.width / 2, center.y - WINDOW_SIZE.height / 2, WINDOW_SIZE.width, WINDOW_SIZE.height);
		this.setResizable(false);
		
		// initialize print preview area
		JPanel contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		
		// initialize toolbar options bar
		JToolBar toolBar = new JToolBar();
		contentPanel.add(toolBar, BorderLayout.NORTH);
		toolBar.setFloatable(false);
		
		
		JButton btnPrint = new JButton("Print");
		toolBar.add(btnPrint);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		toolBar.add(horizontalStrut);
		
		JLabel lblPrintStyle = new JLabel("Print style");
		toolBar.add(lblPrintStyle);
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setToolTipText("Print Style");
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Single-Sided", "Double-Sided"}));
		toolBar.add(comboBox);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		toolBar.add(horizontalStrut_1);
		
		JLabel lblPage = new JLabel("Page");
		toolBar.add(lblPage);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		toolBar.add(horizontalStrut_3);
		
		JButton btnLast = new JButton("First");
		toolBar.add(btnLast);
		
		Component horizontalStrut_6 = Box.createHorizontalStrut(15);
		toolBar.add(horizontalStrut_6);
		
		JButton btnPrevious = new JButton("Previous");
		toolBar.add(btnPrevious);
		
		Component horizontalStrut_4 = Box.createHorizontalStrut(15);
		toolBar.add(horizontalStrut_4);
		
		JLabel lblOf = new JLabel("1 of N");
		toolBar.add(lblOf);
		
		Component horizontalStrut_5 = Box.createHorizontalStrut(15);
		toolBar.add(horizontalStrut_5);
		
		JButton btnNext = new JButton("Next");
		toolBar.add(btnNext);
		
		Component horizontalStrut_7 = Box.createHorizontalStrut(15);
		toolBar.add(horizontalStrut_7);
		
		JButton btnLast_1 = new JButton("Last");
		toolBar.add(btnLast_1);
		
		Component horizontalStrut_8 = Box.createHorizontalStrut(80);
		toolBar.add(horizontalStrut_8);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPanel.add(scrollPane);
	}
}
