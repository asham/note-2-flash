package main.GUI.Dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import main.GUI.Main;

/**
 * Import card dialog
 * @author Will
 */

public class ImportCardDialog extends JDialog implements ActionListener {

    // Default serial version number
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JTextField fileNamePane;
    private JTextField mediaFolderPane;
    private JTextField deckNamePane;

    private static final Dimension WINDOW_SIZE = new Dimension(450,300);

    /**
     * Create the dialog.
     */
    public ImportCardDialog(Point center) {

        setTitle("Import Flashcards");
        setResizable(false);
        setBounds(center.x - WINDOW_SIZE.width / 2, center.y - WINDOW_SIZE.height / 2, 450, 367);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);

        fileNamePane = new JTextField();
        fileNamePane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        fileNamePane.setBounds(60, 42, 300, 20);
        contentPanel.add(fileNamePane);

        JButton btnBrowse = new JButton("Browse");
        btnBrowse.setBounds(271, 73, 89, 23);
        btnBrowse.setActionCommand("FileBrowser");
        btnBrowse.addActionListener(this);
        contentPanel.add(btnBrowse);

        JLabel lblImportLocation = new JLabel("Import Location:");
        lblImportLocation.setBounds(60, 26, 300, 14);
        contentPanel.add(lblImportLocation);
        
        mediaFolderPane = new JTextField();
        mediaFolderPane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        mediaFolderPane.setBounds(60, 175, 300, 20);
        contentPanel.add(mediaFolderPane);
        
        JButton btnMediaBrowse = new JButton("Browse");
        btnMediaBrowse.setActionCommand("MediaBrowser");
        btnMediaBrowse.addActionListener(this);
        btnMediaBrowse.setBounds(271, 205, 89, 23);
        contentPanel.add(btnMediaBrowse);
        
        JLabel lblmediaFolder = new JLabel("Media Folder: (for Anki files)");
        lblmediaFolder.setBounds(60, 159, 300, 14);
        contentPanel.add(lblmediaFolder);

        deckNamePane = new JTextField();
        deckNamePane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        deckNamePane.setBounds(60, 250, 144, 20);
        contentPanel.add(deckNamePane);

        JLabel label = new JLabel("Deck Name:");
        label.setBounds(60, 234, 144, 14);
        contentPanel.add(label);

        JLabel lblAcceptableFormats = new JLabel("Acceptable formats:\r\n\t");
        lblAcceptableFormats.setBounds(70, 73, 191, 14);
        contentPanel.add(lblAcceptableFormats);

        JLabel label_1 = new JLabel("Anki Package");
        label_1.setBounds(94, 104, 102, 14);
        contentPanel.add(label_1);

        JLabel label_3 = new JLabel("Note2Flash");
        label_3.setBounds(94, 90, 102, 14);
        contentPanel.add(label_3);

        JSeparator separator = new JSeparator();
        separator.setBounds(15, 151, 400, 2);
        contentPanel.add(separator);

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        JButton okButton = new JButton("OK");
        okButton.setActionCommand("OK");
        okButton.addActionListener(this);
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
    }

    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        if(action.equals("FileBrowser")){
            final JFileChooser openFileChooser = new JFileChooser();
            openFileChooser.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter filter_all = new FileNameExtensionFilter(
                    "All valid formats", "db", "txt");
            FileNameExtensionFilter filter_mv = new FileNameExtensionFilter(
                    "Note 2 Flash (.mv.db)", "db");
            FileNameExtensionFilter filter_anki = new FileNameExtensionFilter(
                    "Anki Flashcards (.txt)", "txt");


            openFileChooser.addChoosableFileFilter(filter_all);
            openFileChooser.addChoosableFileFilter(filter_mv);
            openFileChooser.addChoosableFileFilter(filter_anki);
            //            openFileChooser.addChoosableFileFilter(filterppt);
            int ret = openFileChooser.showOpenDialog(this);
            if(ret == JFileChooser.APPROVE_OPTION){
                File file = openFileChooser.getSelectedFile();
                fileNamePane.setText(file.getPath());
                // use this file
            }
        }
        
        if(action.equals("MediaBrowser")){
            final JFileChooser mediaDirChooser = new JFileChooser();
            mediaDirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int ret = mediaDirChooser.showOpenDialog(this);
            if(ret == JFileChooser.APPROVE_OPTION){
                File file = mediaDirChooser.getSelectedFile();
                mediaFolderPane.setText(file.getPath());
                // use this directory to search for media files associated with anki deck
            }
        }
        if(action.equals("OK")){
            boolean isSuccess = Main.mainController.importButtonPressed(fileNamePane.getText(), mediaFolderPane.getText(), deckNamePane.getText());
            if (isSuccess) {
                this.dispose();
            }
        }
        if(action.equals("Cancel")){
            this.dispose();
        }
    }
}
