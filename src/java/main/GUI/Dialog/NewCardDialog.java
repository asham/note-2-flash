package main.GUI.Dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import main.GUI.GUIConstants;
import main.GUI.ImagePanel;
import main.GUI.Main;

/**
 * Dialog window for creating one card at a time
 * @author Adrian
 */
public class NewCardDialog extends JDialog {

    private static final long serialVersionUID = 1L;

    private final JPanel contentPanel = new JPanel();

    private static final String ADD = "Make Card";
    private static final String CLOSE = "Close";
    private static final String TITLE = "New Card";
    private static final String RESET = "Clear";
    private static final String FILE = "Image";

    private static final Dimension WINDOW_SIZE = new Dimension(530,300);
    
    private static final int MAX_NUM_CHARS = 500;

    private JPanel frontPanel;
    private JPanel leftTextPanel;
    private JTextArea leftText;

    private String frontFileExt;
    private BufferedImage frontImage;

    private JPanel backPanel;
    private JPanel rightTextPanel;
    private JTextArea rightText;

    private String backFileExt;
    private BufferedImage backImage;

    private JTextField tagsBox;

    /**
     * Create a New Card Dialog
     */
    public NewCardDialog(Point center) {
        setTitle(TITLE);

        setBounds(center.x - WINDOW_SIZE.width / 2, center.y - WINDOW_SIZE.height / 2, WINDOW_SIZE.width, WINDOW_SIZE.height);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
        Action traverseFocus = new AbstractAction(){
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent arg0) {
                Component parent = (Component)arg0.getSource();
                parent.transferFocus();
            }
        };

        // left panel
        leftText = new JTextArea();
        leftText.getInputMap().put(KeyStroke.getKeyStroke("TAB"), "focus");
        leftText.getActionMap().put("focus", traverseFocus);
        leftText.setLineWrap(true);
        
        leftTextPanel = new JPanel(new BorderLayout());
        leftTextPanel.add(leftText);
        frontPanel = makePanel(leftTextPanel, "Front", true);
        frontPanel.setPreferredSize(contentPanel.getSize());
        contentPanel.add(frontPanel);

        // right panel
        rightText = new JTextArea();
        rightText.getInputMap().put(KeyStroke.getKeyStroke("TAB"), "focus");
        rightText.getActionMap().put("focus", traverseFocus);
        rightText.setLineWrap(true);
        rightTextPanel = new JPanel(new BorderLayout());
        rightTextPanel.add(rightText);
        backPanel = makePanel(rightTextPanel, "Back", false);
        backPanel.setPreferredSize(contentPanel.getSize());
        contentPanel.add(backPanel);


        // buttons
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        JLabel tags = new JLabel("Tags: ");
        buttonPane.add(tags);

        tagsBox = new JTextField();
        tagsBox.setColumns(12);
        buttonPane.add(tagsBox);


        JButton okButton = new JButton(ADD);
        okButton.setActionCommand(ADD);
        okButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String left = leftText.getText();
                String right = rightText.getText();
                String leftString = left.length() < MAX_NUM_CHARS ? left : left.substring(0, MAX_NUM_CHARS);
                String rightString = right.length() < MAX_NUM_CHARS ? right : right.substring(0, MAX_NUM_CHARS);
                Main.mainController.addCardButtonPressed(leftString, frontImage, frontFileExt, 
                        rightString, backImage, backFileExt, tagsBox.getText());
                resetTextArea(true);
                resetTextArea(false);
            }
        });
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);


        JButton cancelButton = new JButton(CLOSE);
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        cancelButton.setActionCommand(CLOSE);
        buttonPane.add(cancelButton);

    }


    private void resetTextArea(boolean isFront){

        if(isFront){
            // left panel
            frontImage = null;
            leftText.setText(" ");
            leftTextPanel.removeAll();
            leftTextPanel.add(leftText);
            leftTextPanel.repaint();
            leftText.setText("");
            frontPanel.revalidate();
            frontPanel.repaint();
        } else {
            // right panel
            backImage = null;
            rightText.setText(" ");
            rightTextPanel.removeAll();
            rightTextPanel.add(rightText);
            rightTextPanel.repaint();
            rightText.setText("");
            backPanel.revalidate();
            backPanel.repaint();
        }

    }

    private JPanel makePanel(JPanel textArea, String lblText, final boolean isFront){
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout(0, 0));
        {
            JPanel panel_1 = new JPanel();
            panel.add(panel_1, BorderLayout.NORTH);
            {
                JLabel lblNewLabel = new JLabel(lblText);
                panel_1.add(lblNewLabel);
            }

        }


        {
            //textArea.setLineWrap(true);
            JScrollPane scrollPane = new JScrollPane(textArea);
            panel.add(scrollPane, BorderLayout.CENTER);
        }
        {
            JPanel panel_1 = new JPanel();
            FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
            flowLayout.setAlignment(FlowLayout.RIGHT);
            panel.add(panel_1, BorderLayout.SOUTH);
            {
                JButton imageButton = new JButton(FILE);

                imageButton.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent arg0) {
                        createFileDialog(isFront);

                    }

                });

                panel_1.add(imageButton);


                JButton resetButton = new JButton(RESET);

                resetButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        resetTextArea(isFront);
                    }
                });
                panel_1.add(resetButton);
            }
        }
        return panel;
    }

    private void createFileDialog(boolean isFront){
        final JFileChooser openFileChooser = new JFileChooser();
        FileNameExtensionFilter filter = GUIConstants.IMAGE_FILTER;
        openFileChooser.setFileFilter(filter);
        int fileValue = openFileChooser.showOpenDialog(this);


        if(fileValue == JFileChooser.APPROVE_OPTION){
            // they selected an image
            File imageFile = openFileChooser.getSelectedFile();
            String[] splitName = imageFile.getName().split("\\.");

            String fileExt;
            if(splitName.length >= 1){
                fileExt = splitName[splitName.length - 1];
            } else {
                fileExt = null;
            }

            try {
                if(isFront){

                    frontImage = ImageIO.read(imageFile);
                    frontFileExt = fileExt;

                    leftTextPanel.removeAll();
                    leftTextPanel.add(new ImagePanel(frontImage));
                    leftTextPanel.update(frontPanel.getGraphics());
                    leftTextPanel.revalidate();
                    frontPanel.revalidate();
                    frontPanel.repaint();

                } else {
                    backImage = ImageIO.read(imageFile);
                    backFileExt = fileExt;

                    rightTextPanel.removeAll();
                    rightTextPanel.add(new ImagePanel(backImage));
                    rightTextPanel.update(backPanel.getGraphics());
                    rightTextPanel.revalidate();
                    backPanel.revalidate();
                    backPanel.repaint();

                }

                //                imagePanel.setPreferredSize(new Dimension(openedImage.getWidth(), openedImage.getHeight()));
                //                scrollPane.update(scrollPane.getGraphics());
                //                scrollPane.revalidate();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return;
            }          

        } else {
            // they didn't select an image
        }
    }    

}