package main.GUI.Dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import java.awt.FlowLayout;
import java.io.File;

import javax.swing.border.EmptyBorder;

import main.GUI.Main;

/**
 * Export card dialog
 * @author David
 */

public class ExportCardDialog extends JDialog implements ActionListener {
	
    // Default serial version number
	private static final long serialVersionUID = 1L;
	
	private static final String NOTE_2_FLASH = new String("Note 2 Flash (.mv.db)");
	private static final String PDF = new String("Adobe PDF (.pdf)");
	private static final String ANKI = new String("Anki Flashcards (.txt)");
	private static final Dimension WINDOW_SIZE = new Dimension(400,200);
	
	private JTextField dirPathPane;
    private JComboBox<String> formatBox;
	
	/**
	 * Create a new dialog box
	 */
    public ExportCardDialog(Point center) {
        
		setTitle("Export Flashcards");
		setResizable(false);
		setBounds(center.x - WINDOW_SIZE.width / 2, center.y - WINDOW_SIZE.height / 2, WINDOW_SIZE.width, WINDOW_SIZE.height);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel buttonPane = new JPanel();
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		JButton btnOK = new JButton("OK");
		btnOK.setActionCommand("OK");
        btnOK.addActionListener(this);
		buttonPane.add(btnOK);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setActionCommand("Cancel");
        btnCancel.addActionListener(this);
		buttonPane.add(btnCancel);
		
		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(null);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		dirPathPane = new JTextField();
		dirPathPane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		dirPathPane.setBounds(60, 42, 300, 20);
		contentPanel.add(dirPathPane);
		
		JButton btnBrowse = new JButton("Browse");
		btnBrowse.setActionCommand("Browse");
		btnBrowse.setBounds(271, 73, 89, 23);
        btnBrowse.addActionListener(this);
		contentPanel.add(btnBrowse);
		
		JLabel lblExportLocation = new JLabel("Export Location:");
		lblExportLocation.setBounds(60, 26, 300, 14);
		contentPanel.add(lblExportLocation);
		
		JLabel lblExportFormat = new JLabel("Export Format:\r\n\t");
		lblExportFormat.setBounds(60, 111, 191, 14);
		contentPanel.add(lblExportFormat);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(15, 151, 400, 2);
		contentPanel.add(separator);
		
		String[] outputFormats = {PDF, NOTE_2_FLASH, ANKI};

        formatBox = new JComboBox<String>(outputFormats);
		formatBox.setBounds(185, 106, 140, 27);
		contentPanel.add(formatBox);
	}


	public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        if (action.equals("Browse")) {
        	final JFileChooser chooser = new JFileChooser();
        	chooser.setDialogTitle("Choose a Target Directory");
        	chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        	int returnVal = chooser.showOpenDialog(this);
        	if (returnVal == JFileChooser.APPROVE_OPTION) {
        		File selectedFile = chooser.getSelectedFile();
        		dirPathPane.setText(selectedFile.getPath());
        	}
        }
        if(action.equals("OK")){
            
            String format = (String) formatBox.getSelectedItem();
            System.out.println("EXPORTING " + dirPathPane.getText() + " // " + format);
            if(format.equals(PDF)) format = ".pdf";
            else if(format.equals(NOTE_2_FLASH)) format = ".db";
            else if(format.equals(ANKI)) format = ".txt";
            boolean isSuccess = Main.mainController.exportButtonPressed(dirPathPane.getText(), format);
            if (isSuccess) {
                this.dispose();
            }
        }
        if(action.equals("Cancel")){
            this.dispose();
        }
	}
}
