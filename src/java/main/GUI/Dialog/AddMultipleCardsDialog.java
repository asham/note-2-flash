package main.GUI.Dialog;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import main.GUI.GUIConstants;
import main.GUI.ImageSelectionPanel;
import main.GUI.Main;

/**
 * Dialog box for creating an AddMultipleCardsDialog window, which allows users to add
 * multiple cards at once from a PDF or image.
 * @author Trey
 */
public class AddMultipleCardsDialog extends JDialog {


    // required serial version id
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();

    private static final Dimension WINDOW_SIZE = new Dimension(540,620);

    private Boolean initialSelection;
    
    /**
     * The panel containing the image that users will create cards from
     */
    private ImageSelectionPanel imagePanel;

    /**
     * The currently opened image in the viewer.
     */
    private BufferedImage openedImage;
    
    private ButtonGroup modeGroup;
    
    /**
     * JScrollPane containing the imagePanel
     */
    private JScrollPane scrollPane;

    /**
     * File dialog for choosing the image
     */
    private AddMultipleCardsDialog thisDialog;

    /**
     * File extension of the currently viewing image
     */
    private String currentFileExtension;
    
    /**
     * Create the AddMultipleCards dialog.
     */  
    public AddMultipleCardsDialog(Point center) {
        openedImage = null;
        initialSelection = true;
        thisDialog = this;

        setTitle("Add From Image");
        setBounds(center.x - WINDOW_SIZE.width / 2, center.y - WINDOW_SIZE.height / 2, WINDOW_SIZE.width, WINDOW_SIZE.height);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BorderLayout(0, 0));


        imagePanel = new ImageSelectionPanel(null);

        imagePanel.addMouseListener(new MouseListener(){

            @Override
            public void mouseClicked(MouseEvent arg0) {              
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {                   
            }

            @Override
            public void mouseExited(MouseEvent arg0) {             
            }

            @Override
            public void mousePressed(MouseEvent arg0) {
                if(initialSelection){
                    Point originPoint = arg0.getPoint();
                    imagePanel.setStartPoint(originPoint);
                    imagePanel.revalidate();
                    imagePanel.repaint();
                } else {
                    if(arg0.isMetaDown()){
                        imagePanel.removeDivider(arg0.getPoint());
                        imagePanel.revalidate();
                        imagePanel.repaint();
                    } else {
                        imagePanel.addDivider(arg0.getPoint());
                        imagePanel.revalidate();
                        imagePanel.repaint();
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
                if(initialSelection && !arg0.isMetaDown() && imagePanel.pointsValid() && imagePanel.isDrawingTable()){
                    initialSelection = false;
                }
                imagePanel.flushSelection();
                imagePanel.revalidate();
                imagePanel.repaint();
            }


        });

        imagePanel.addMouseMotionListener(new MouseMotionListener(){

            @Override
            public void mouseDragged(MouseEvent arg0) {
                if(initialSelection && !arg0.isMetaDown()){
                    Point endPoint = arg0.getPoint();
                    imagePanel.setEndPoint(endPoint);
                    imagePanel.revalidate();
                    imagePanel.repaint();
                }
            }

            @Override
            public void mouseMoved(MouseEvent arg0) {                  
            }

        });

        scrollPane = new JScrollPane(imagePanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(GUIConstants.SCROLL_SPEED);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(GUIConstants.SCROLL_SPEED);
        contentPanel.add(scrollPane, BorderLayout.CENTER);


        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        SpringLayout layout = new SpringLayout();
        toolBar.setLayout(layout);
        contentPanel.add(toolBar, BorderLayout.NORTH);

        JButton btnFile = new JButton("Load File");
        btnFile.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
                createFileDialog(); 
            }
        });
        toolBar.add(btnFile);

        // Mode buttons
        
        modeGroup = new ButtonGroup();
        
        JToggleButton selectionModeButton = new JToggleButton("Selection Mode");
        selectionModeButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
                selectionMode(); 
            }
        });
        modeGroup.add(selectionModeButton);
        selectionModeButton.setSelected(true);
        toolBar.add(selectionModeButton);
        
        JToggleButton tableModeButton = new JToggleButton("Table Mode");
        tableModeButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
                tableMode(); 
            }
        });
        modeGroup.add(tableModeButton);
        toolBar.add(tableModeButton);
        
        
        JButton btnUndo = new JButton("Clear");
        btnUndo.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                imagePanel.clearSelection();
                imagePanel.revalidate();
                imagePanel.repaint();
                initialSelection = true;
            }                 
        });
        toolBar.add(btnUndo);


        JButton btnHelp = new JButton("Help");
        btnHelp.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                TutorialDialog dialog = new TutorialDialog(new Point(thisDialog.getX()+thisDialog.getWidth()/2, thisDialog.getY()+thisDialog.getHeight()/2));
                dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                dialog.setVisible(true);
                dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            }                 
        });
        toolBar.add(btnHelp);      
        layout.putConstraint(SpringLayout.WEST,  btnFile, 5, SpringLayout.WEST, toolBar);
        layout.putConstraint(SpringLayout.WEST, selectionModeButton, 5, SpringLayout.EAST, btnFile);
        layout.putConstraint(SpringLayout.WEST, tableModeButton, 5, SpringLayout.EAST, selectionModeButton);
        layout.putConstraint(SpringLayout.EAST,  btnHelp, -2, SpringLayout.EAST, toolBar);
        layout.putConstraint(SpringLayout.EAST,  btnUndo, -5, SpringLayout.WEST, btnHelp);
        toolBar.setPreferredSize(new Dimension(WINDOW_SIZE.width, 35));

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        JButton addButton = new JButton("Make Cards");
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                
                // have to test this first, getFrontList/BackList clears selections
                if(imagePanel.selectionsCount() == 1){
                 // Only one selected, display error
                    JOptionPane.showMessageDialog(thisDialog, "Two sub-images must be selected to make a card.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                
                List<BufferedImage> frontList = imagePanel.getFrontList();
                List<BufferedImage> backList = imagePanel.getBackList();
                
                System.out.println(imagePanel.selectionsCount());
                
                if(frontList.isEmpty() || backList.isEmpty()){
                    // nothing selected, display error
                    JOptionPane.showMessageDialog(thisDialog, "No image selections made.", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    Main.mainController.createMultipleCardsButtonPressed(frontList, backList, currentFileExtension);
                }
                
                imagePanel.clearSelection();
                imagePanel.revalidate();
                imagePanel.repaint();
                initialSelection = true;
            }
        });
        buttonPane.add(addButton);
        getRootPane().setDefaultButton(addButton);


        JButton closeButton = new JButton("Close");
        closeButton.setActionCommand("Cancel");
        closeButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
                dispose();
            }
        });
        buttonPane.add(closeButton);

    }

    /**
     * Sets the selection panel to table mode
     */
    private void tableMode(){
        imagePanel.drawTable();
        initialSelection = true;
    }
    
    /**
     * Sets the selection panel to selection mode
     */
    private void selectionMode(){
        imagePanel.drawSelection();
        initialSelection = true;
    }

    /**
     * Creates the file explorer dialog for letting the user choose an image
     * file to open.
     */
    private void createFileDialog(){
        final JFileChooser openFileChooser = new JFileChooser();
        FileNameExtensionFilter filter = GUIConstants.IMAGE_FILTER;
        openFileChooser.setFileFilter(filter);
        int fileValue = openFileChooser.showOpenDialog(this);

        if(fileValue == JFileChooser.APPROVE_OPTION){
            // they selected an image
            File imageFile = openFileChooser.getSelectedFile();
            
            try {
                openedImage = ImageIO.read(imageFile);
                String[] splitPath = imageFile.getName().split("\\.");
                
                // only allow supported image files
                if(splitPath.length > 0 && Arrays.asList(filter.getExtensions()).contains(splitPath[splitPath.length - 1])){
                    currentFileExtension = splitPath[splitPath.length - 1];
                } else {
                    throw new IOException();
                }
                
                imagePanel.setImage(openedImage);
                imagePanel.setPreferredSize(new Dimension(openedImage.getWidth(), openedImage.getHeight()));
                scrollPane.update(scrollPane.getGraphics());
                scrollPane.revalidate();
            } catch (IOException e) {
                
                //custom title, error icon
                JOptionPane.showMessageDialog(this,
                    "File failed to load correctly.",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
                
                e.printStackTrace();
                return;
            }          

        } else {
            // they didn't select an image
        }
    }    
}
