package main.GUI.Dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import main.GUI.Main;

/**
 * Add deck dialog
 * @author Will
 */

public class NewDeckDialog extends JDialog {
    
    // Default serial version number
    private static final long serialVersionUID = 1L;
    private JTextField textPane;
    
    private static final Dimension WINDOW_SIZE = new Dimension(400,150);
    
    private NewDeckDialog parent;
    
    /**
     * Create a new dialog box
     */
    public NewDeckDialog(Point center) {
        
        parent = this;
        
        setTitle("New Deck");
        setResizable(false);
        setBounds(center.x - WINDOW_SIZE.width / 2, center.y - WINDOW_SIZE.height / 2, WINDOW_SIZE.width, WINDOW_SIZE.height);
        getContentPane().setLayout(new BorderLayout(0, 0));
        
        JPanel buttonPane = new JPanel();
        getContentPane().add(buttonPane, BorderLayout.SOUTH);
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        
        JButton btnOK = new JButton("OK");
        btnOK.addActionListener(new OkListener());
        buttonPane.add(btnOK);
        
        JButton btnCancel = new JButton("Cancel");
        btnCancel.addActionListener(new CancelListener());
        buttonPane.add(btnCancel);
        
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(null);
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        
        textPane = new JTextField();
        textPane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        textPane.setBounds(60, 42, 300, 20);
        textPane.addActionListener(new OkListener());
        
        contentPanel.add(textPane);
        
        JLabel lblExportLocation = new JLabel("Deck Name:");
        lblExportLocation.setBounds(60, 26, 300, 14);
        contentPanel.add(lblExportLocation);
        
    }

    /**
     * Listener for Accept events 
     * @author Trey
     */
    private class OkListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent arg0) {
            Main.mainController.addDeckButtonPressed(textPane.getText());
            parent.dispose();
        }
    }
    
    /**
     * Listener for cancel events
     * @author Trey
     */
    private class CancelListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent arg0) {
            parent.dispose();
        }
    }
}
