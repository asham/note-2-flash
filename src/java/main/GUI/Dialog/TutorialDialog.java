package main.GUI.Dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Tutorial dialog box
 * @autor Tianchi
 */
public class TutorialDialog extends JDialog {

    private static final long serialVersionUID = 1L;

    private static final Dimension WINDOW_SIZE = new Dimension(450,300);

    /**
     * Create the dialog.
     */
    public TutorialDialog(Point center) {
        setTitle("Selection Modes Tutorial");
        setBounds(center.x - WINDOW_SIZE.width / 2, center.y - WINDOW_SIZE.height / 2, WINDOW_SIZE.width, (int) (WINDOW_SIZE.height * 1.5));
        this.setResizable(false);

        // Bottom panel for control buttons
        getContentPane().setLayout(new BorderLayout(0, 0));

        JPanel bottomPanel = new JPanel();
        FlowLayout flowLayout = (FlowLayout) bottomPanel.getLayout();
        flowLayout.setAlignment(FlowLayout.RIGHT);
        getContentPane().add(bottomPanel, BorderLayout.SOUTH);

        JButton doneButton = new JButton("Done");
        doneButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
                dispose();
            }
        });
        bottomPanel.add(doneButton);
        
        
        JPanel panel = new JPanel();
        getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(new BorderLayout(0, 0));
        
        Component verticalStrut = Box.createVerticalStrut(20);
        panel.add(verticalStrut, BorderLayout.NORTH);
        
        JLabel lblNewLabel = new JLabel("<html><h1>Using selection mode:</h1>Left click and drag to make selections. The first selection will be the front of the card, and the second will be the back of the card.<br><br>"
                + "<h1>Using table mode:</h1>Left click drag to select entire area.<br><br>Then:<br>Left click to make dividers.<br>Right click to delete dividers.<br>Click near the bottom of the grid to move the last divider.<br><br>"
                + "Click Clear button to remove all selection boxes<html>");
        
        panel.add(lblNewLabel, BorderLayout.CENTER);
        
        Component horizontalStrut = Box.createHorizontalStrut(75);
        panel.add(horizontalStrut, BorderLayout.WEST);
        
        Component horizontalStrut_1 = Box.createHorizontalStrut(75);
        panel.add(horizontalStrut_1, BorderLayout.EAST);

    }

    // Central panel for displaying informations
    {


    }

}
