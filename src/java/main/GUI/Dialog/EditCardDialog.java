package main.GUI.Dialog;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import main.GUI.GUIConstants;
import main.GUI.ImageConvert;
import main.GUI.ImagePanel;
import main.GUI.Main;
import model.Card;
import model.SideReader;

/**
 * Dialog window for editing one card at a time
 * @author Adrian
 */
public class EditCardDialog extends JDialog implements SideReader {

    private static final long serialVersionUID = 1L;

    private final JPanel contentPanel = new JPanel();

    private static final String ACCEPT = " Next ";
    private static final String CLOSE = "Close";
    private static final String TITLE = "Edit Card";
    private static final String RESET = "Clear";
    private static final String FILE = "Image";
    private static final String PREVIOUS = "Previous";

    private static final Dimension WINDOW_SIZE = new Dimension(530,300);

    private static final int MAX_NUM_CHARS = 500;

    private JPanel frontPanel;
    private JPanel leftTextPanel;
    private JTextArea leftText;
    private BufferedImage frontImage;
    private String frontFileExt;

    private JPanel backPanel;
    private JPanel rightTextPanel;
    private JTextArea rightText;
    private BufferedImage backImage;
    private String backFileExt;

    private JTextField tagsBox;

    private JButton prevButton;
    private JButton nextButton;
    private JButton closeButton;
    
    private boolean goBack;
    private boolean close;
    private Card oldCard;

    /**
     * Create a New Card Dialog
     * @param center 
     */
    public EditCardDialog(Point center, Card card) {
        this.oldCard = card;
        this.goBack = false;
        this.close = false;
        setTitle(TITLE);
        setBounds(center.x - WINDOW_SIZE.width / 2, center.y - WINDOW_SIZE.height / 2, WINDOW_SIZE.width, WINDOW_SIZE.height);
        //this.setResizable(false);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
        Action traverseFocus = new AbstractAction(){
            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent arg0) {
                Component parent = (Component)arg0.getSource();
                parent.transferFocus();
            }
        };

        // left panel
        leftText = new JTextArea();
        leftText.getInputMap().put(KeyStroke.getKeyStroke("TAB"), "focus");
        leftText.getActionMap().put("focus", traverseFocus);
        leftText.setLineWrap(true);
        leftTextPanel = new JPanel(new BorderLayout());
        leftTextPanel.add(leftText);
        frontPanel = makePanel(leftTextPanel, "Front", true);
        frontPanel.setPreferredSize(contentPanel.getSize());
        contentPanel.add(frontPanel);
        oldCard.readFront(this);


        // right panel
        rightText = new JTextArea();
        rightText.getInputMap().put(KeyStroke.getKeyStroke("TAB"), "focus");
        rightText.getActionMap().put("focus", traverseFocus);
        rightText.setLineWrap(true);
        rightTextPanel = new JPanel(new BorderLayout());
        rightTextPanel.add(rightText);
        backPanel = makePanel(rightTextPanel, "Back", false);
        backPanel.setPreferredSize(contentPanel.getSize());
        contentPanel.add(backPanel);
        oldCard.readBack(this);


        // buttons
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        prevButton = new JButton(PREVIOUS);
        prevButton.setActionCommand(PREVIOUS);
        prevButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                goBack = true;
                acceptChanges();
            }
        });
        buttonPane.add(prevButton);
        
        nextButton = new JButton(ACCEPT);
        nextButton.setActionCommand(ACCEPT);
        nextButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                acceptChanges();
            }
        });
        buttonPane.add(nextButton);
        getRootPane().setDefaultButton(nextButton);
        
        JLabel label = new JLabel("Tags: ");
        buttonPane.add(label);


        tagsBox = new JTextField();
        tagsBox.setText(commaSeparatedTagList());
        tagsBox.setColumns(12);
        buttonPane.add(tagsBox);

        closeButton = new JButton("OK");
        closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                close = true;
                acceptChanges();
            }
        });
        closeButton.setActionCommand(CLOSE);
        buttonPane.add(closeButton);
        
        closeButton = new JButton(CLOSE);
        closeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                close = true;
                dispose();
            }
        });
        closeButton.setActionCommand(CLOSE);
        buttonPane.add(closeButton);


    }
    
    public void disablePrevCard(){
        prevButton.setEnabled(false);
    }
    public void disableNextCard(){
        nextButton.setEnabled(false);
        getRootPane().setDefaultButton(closeButton);
    }
    
    private void acceptChanges(){
        String left = leftText.getText();
        String right = rightText.getText();
        String leftString = left.length() < MAX_NUM_CHARS ? left : left.substring(0, MAX_NUM_CHARS);
        String rightString = right.length() < MAX_NUM_CHARS ? right : right.substring(0, MAX_NUM_CHARS);
        Main.mainController.editCardCallback(oldCard, leftString, frontImage, frontFileExt, 
                rightString, backImage, backFileExt, tagsBox.getText());
        dispose();
    }
    
    private void resetTextArea(boolean isFront){

        if(isFront){
            // left panel
            frontImage = null;
            leftText.setText(" ");
            leftTextPanel.removeAll();
            leftTextPanel.add(leftText);
            leftTextPanel.repaint();
            leftText.setText("");
            frontPanel.revalidate();
            frontPanel.repaint();
        } else {
            // right panel
            backImage = null;
            rightText.setText(" ");
            rightTextPanel.removeAll();
            rightTextPanel.add(rightText);
            rightTextPanel.repaint();
            rightText.setText("");
            backPanel.revalidate();
            backPanel.repaint();
        }

    }

    private JPanel makePanel(JPanel textArea, String lblText, final boolean isFront){
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout(0, 0));

        JPanel panel_1 = new JPanel();
        panel.add(panel_1, BorderLayout.NORTH);

        JLabel lblNewLabel = new JLabel(lblText);
        panel_1.add(lblNewLabel);

        //textArea.setLineWrap(true);
        JScrollPane scrollPane = new JScrollPane(textArea);
        panel.add(scrollPane, BorderLayout.CENTER);


        JPanel panel_2 = new JPanel();
        FlowLayout flowLayout = (FlowLayout) panel_2.getLayout();
        flowLayout.setAlignment(FlowLayout.RIGHT);
        panel.add(panel_2, BorderLayout.SOUTH);

        JButton imageButton = new JButton(FILE);

        imageButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent arg0) {
                createFileDialog(isFront);

            }

        });

        panel_2.add(imageButton);


        JButton resetButton = new JButton(RESET);

        resetButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                resetTextArea(isFront);
            }
        });
        panel_2.add(resetButton);


        return panel;
    }

    private void createFileDialog(boolean isFront){
        final JFileChooser openFileChooser = new JFileChooser();
        FileNameExtensionFilter filter = GUIConstants.IMAGE_FILTER;
        openFileChooser.setFileFilter(filter);
        int fileValue = openFileChooser.showOpenDialog(this);


        if(fileValue == JFileChooser.APPROVE_OPTION){
            // they selected an image
            File imageFile = openFileChooser.getSelectedFile();
            
            String[] splitName = imageFile.getName().split("\\.");
            String fileExt;
            if(splitName.length >= 1){
                fileExt = splitName[splitName.length - 1];
            } else {
                fileExt = null;
            }
            
            try {
                if(isFront){

                    frontImage = ImageIO.read(imageFile);
                    frontFileExt = fileExt;
                    
                    leftTextPanel.removeAll();
                    leftTextPanel.add(new ImagePanel(frontImage));
                    leftTextPanel.update(frontPanel.getGraphics());
                    leftTextPanel.revalidate();
                    frontPanel.revalidate();
                    frontPanel.repaint();

                } else {
                    backImage = ImageIO.read(imageFile);
                    backFileExt = fileExt;
                    
                    rightTextPanel.removeAll();
                    rightTextPanel.add(new ImagePanel(backImage));
                    rightTextPanel.update(backPanel.getGraphics());
                    rightTextPanel.revalidate();
                    backPanel.revalidate();
                    backPanel.repaint();

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return;
            }          
        }
    }

    @Override
    public void readFrontSide(BufferedImage imgFront, String fileExt) {
        this.frontImage = imgFront;
        this.frontFileExt = fileExt;

        leftTextPanel.add(new ImagePanel(ImageConvert.toBufferedImage(frontImage)));
        leftTextPanel.update(frontPanel.getGraphics());
        leftTextPanel.revalidate();
    }

    @Override
    public void readFrontSide(String textFront) {
        this.leftText.setText(textFront);
    }

    @Override
    public void readBackSide(BufferedImage imgBack, String fileExt) {
        this.backImage = imgBack;
        this.backFileExt = fileExt;

        rightTextPanel.add(new ImagePanel(ImageConvert.toBufferedImage(backImage)));
        rightTextPanel.update(backPanel.getGraphics());
        rightTextPanel.revalidate();
    }

    @Override
    public void readBackSide(String textBack) {
        this.rightText.setText(textBack);		
    }

    private String commaSeparatedTagList() {
        String tagString = "";
        Iterator<String> tagIter = oldCard.getTags().iterator();
        while (tagIter.hasNext()) {
            tagString = tagString + tagIter.next();
            if (tagIter.hasNext()) {
                tagString += ", ";
            }
        }
        return tagString;
    }
    public boolean getGoBack(){
        return goBack;
    }
    public boolean getClose(){
        return close;
    }
}