package main.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Card;

public class MainWindowGUI extends JFrame {

    // Default serial id
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;

    private static MainCardView cardViewingPanel;
    private static MainDeckView deckViewingPanel;
    
    /**
     * Debug constructor
     */
    protected MainWindowGUI(){
        
    }
    
    /**
     * Create the frame.
     */
    public MainWindowGUI(String title) {
        // Create the window frame
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Set window size
        setPreferredSize(new Dimension(GUIConstants.MAIN_WINDOW_WIDTH, 
                GUIConstants.MAIN_WINDOW_HEIGHT));
        
        // Add menu bar
        setJMenuBar(new MainMenu());
        
        // Set up content pane
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
        
        // Add deck view and card view
        // cardViewingPanel = new MainCardView();
        deckViewingPanel = new MainDeckView(new ArrayList<String>());
        cardViewingPanel = new MainCardView(new ArrayList<CardDisplay>());
        
        contentPane.add(deckViewingPanel, BorderLayout.WEST);
        contentPane.add(cardViewingPanel, BorderLayout.CENTER);
        
        // Required operations
        pack();
        setVisible(true);
        repaint();
    }
    
    /**
     * Update the main card view of the UI
     * @param viewingCards The new cards to display in the viewing window
     */
    public void setCardView(List<CardDisplay> viewingCards){
        cardViewingPanel.setCards(viewingCards);
        cardViewingPanel.revalidate();
        cardViewingPanel.repaint();
    }
    
    public void updateCardView(){
        cardViewingPanel.updateCards();
        cardViewingPanel.revalidate();
        cardViewingPanel.repaint();
    }
    
    public void setDeckView(List<String> decks){
        deckViewingPanel.setDecks(decks);
        deckViewingPanel.revalidate();
        deckViewingPanel.repaint();
    }
    
    public void setSelectedDeck(int index) {
        deckViewingPanel.setSelectedDeckRow(index);
    }
    
    public void setTags(Set<String> tags){
        cardViewingPanel.setTags(tags);
        cardViewingPanel.revalidate();
        cardViewingPanel.repaint();
    }
    
    /**
     * Get the currently selected cards
     */
    public List<CardDisplay> getSelectedCards(){
        return cardViewingPanel.getSelectedCards();
    }
    
    /**
     * Opens an edit dialog for the given card. Needed for modality
     * @param oldCard the card to edit
     */
    public void openEditDialog(List<Card> oldCards){
        cardViewingPanel.openEditDialog(oldCards);
    }
    

}
