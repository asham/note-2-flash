package main.GUI;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Stores dimension constants for GUIs
 * @author TianChi
 */
public class GUIConstants {
    // Main window dimensions
    public static final int MAIN_WINDOW_WIDTH = 1020;
    public static final int MAIN_WINDOW_HEIGHT = 600;

    public static final int WINDOW_OFFSET = 50;

    // Card view dimensions
    public static final int CARD_VIEW_WIDTH = (MAIN_WINDOW_WIDTH * 3) / 4;
    public static final int CARD_VIEW_HEIGHT = MAIN_WINDOW_HEIGHT - WINDOW_OFFSET;

    // Deck view dimensions
    public static final int DECK_VIEW_WIDTH = MAIN_WINDOW_WIDTH / 4;
    public static final int DECK_VIEW_HEIGHT = MAIN_WINDOW_HEIGHT - WINDOW_OFFSET;
    public static final Dimension DECK_VIEW_DIMENSIONS = new Dimension(DECK_VIEW_WIDTH, DECK_VIEW_HEIGHT); 

    // Image formats supported
    public static final FileNameExtensionFilter IMAGE_FILTER = new FileNameExtensionFilter(
            "JPEG and PNG images", "jpg", "jpeg", "JPG", "JPEG", "png", "PNG");
    
    // Highlight color
    // public static final Color HIGHLIGHT_COLOR = new Color(210, 230, 85);
    public static final Color HIGHLIGHT_COLOR = Color.RED;
    
    // Highlight border
    public static final Border HIGHLIGHT_BORDER = BorderFactory.createLineBorder(HIGHLIGHT_COLOR, 3);
    
    // Normal border (Contains empty border to be the same size as the highlight border)
    public static final Border NORMAL_BORDER = new CompoundBorder(new EmptyBorder(2, 2, 2, 2), BorderFactory.createLineBorder(Color.BLACK));
    
    // Tool bar parameters
    public static final int TOOLBAR_HEIGHT = 35;
    
    // The speed that components with scroll bars scroll at
    public static final int SCROLL_SPEED = 16;
}
