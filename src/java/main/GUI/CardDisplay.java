package main.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import model.Card;
import model.SideReader;

/**
 * Card view with the ability to display a card
 * by implementing side reader
 * @author Trey
 */
public class CardDisplay implements SideReader {

    // Light gray
    private static final Color FRONT_COLOR = new Color(210, 210, 210);
    
    // Light blue
    private static final Color BACK_COLOR = new Color(214, 230, 255);
    
    /**
     * The default dimensions for the displayed card
     */
    private static final Dimension CARD_SIZE = new Dimension(250, 150);
    
    /**
     * The default dimensions for the button
     */
    private static final Dimension BUTTON_SIZE = new Dimension(30, 30);
    
    /**
     * Location of flip card icon image
     */
    private static final String FLIP_IMAGE_LOCATION = "img/flip.png";
    
    /**
     * The front side of the card
     */
    private JPanel frontSide;
    
    /**
     * The back side of the card
     */
    private JPanel backSide;
    
    /**
     * The content on the front side of the card
     */
    private JPanel frontContent;
    
    /**
     * The content on the back side of the card
     */
    private JPanel backContent;


    /**
     * A reference to card that this CardDisplay represents
     */
    private Card actualCard;

    /**
     * Boolean representing whether or not the card display is currently selected
     */
    private boolean isSelected;
    
    /**
     * Boolean representing whether or not the card is flipped so that
     * the back is visible
     */
    private boolean flipped;

    /**
     * Creates a new card display for the given card
     * @param card The card to create a card display from
     */
    public CardDisplay(Card card){
        
        frontSide = new JPanel(new BorderLayout());
        frontSide.setBorder(GUIConstants.NORMAL_BORDER);
        frontSide.addMouseListener(new CardMouseListener());
        
        backSide = new JPanel(new BorderLayout());
        backSide.setBorder(GUIConstants.NORMAL_BORDER);
        backSide.addMouseListener(new CardMouseListener());
        
        JPanel frontCommandBar = new JPanel(new BorderLayout());
        frontCommandBar.setBackground(FRONT_COLOR);
        frontCommandBar.add(new JLabel("Q: "), BorderLayout.WEST);
        
        JPanel backCommandBar = new JPanel(new BorderLayout());
        backCommandBar.setBackground(BACK_COLOR);
        backCommandBar.add(new JLabel("A: "), BorderLayout.WEST);
        
        if(card.getTags().size() != 0){
            
            String tags = card.getTags().toString();
            
            // remove the brackets from the front and back of the tag strings
            tags = removeBrackets(tags);
            JLabel frontTags = new JLabel(tags);
            JLabel backTags = new JLabel(tags);
            frontTags.setPreferredSize(new Dimension(50, frontCommandBar.getHeight()));
            backTags.setPreferredSize(new Dimension(50, backCommandBar.getHeight()));
            frontCommandBar.add(frontTags, BorderLayout.CENTER);
            backCommandBar.add(backTags, BorderLayout.CENTER);
        }
        
        JPanel frontCommands = new JPanel();
        frontCommands.setBackground(FRONT_COLOR);
        
        JPanel backCommands = new JPanel();
        backCommands.setBackground(BACK_COLOR);
        
        actualCard = card;
        flipped = false;
        
        frontContent = new JPanel();
        card.readFront(this);
        frontContent.setPreferredSize(CARD_SIZE);
        frontContent.setMinimumSize(CARD_SIZE);
        frontContent.setMaximumSize(CARD_SIZE);
        frontContent.setBackground(Color.WHITE);
        
        backContent = new JPanel();
        card.readBack(this);
        backContent.setPreferredSize(CARD_SIZE);
        backContent.setMinimumSize(CARD_SIZE);
        backContent.setMaximumSize(CARD_SIZE);
        backContent.setBackground(Color.WHITE);
        
        JButton flipFrontButton = new JButton();
        flipFrontButton.addActionListener(new ButtonListener());
        flipFrontButton.setMaximumSize(BUTTON_SIZE); 
        
        JButton flipBackButton = new JButton();
        flipBackButton.addActionListener(new ButtonListener());
        flipBackButton.setMaximumSize(BUTTON_SIZE);
        
        try {
            BufferedImage img = ImageIO.read(new File(FLIP_IMAGE_LOCATION));
            flipFrontButton.setIcon(new ImageIcon(img));
            flipBackButton.setIcon(new ImageIcon(img));
        } catch (IOException ex) {
        }
        
        frontCommands.add(flipFrontButton);
        backCommands.add(flipBackButton);
        
        frontCommandBar.add(frontCommands, BorderLayout.EAST);
        backCommandBar.add(backCommands, BorderLayout.EAST);
        
        frontSide.add(frontCommandBar, BorderLayout.NORTH);
        frontSide.add(frontContent, BorderLayout.CENTER);
        
        backSide.add(backCommandBar, BorderLayout.NORTH);
        backSide.add(backContent, BorderLayout.CENTER);
        
        isSelected = false;
    }
    
    /**
     * Getter for the frontside JPanel
     * @return A JPanel representing the front of the card
     */
    public JPanel getFrontSide(){
        return frontSide;
    }
    
    /**
     * Getter for the backside JPanel
     * @return A JPanel representing the back of the card
     */
    public JPanel getBackSide(){
        return backSide;
    }

    /**
     * Returns whether or not the card is flipped
     * @return True if the back of the card is visible, false otherwise
     */
    public boolean isFlipped(){
        return flipped;
    }

    /**
     * Sets the check boxes of the card based on the selected parameter
     * @param selected
     */
    public void setSelected(boolean selected){
        isSelected = selected;
        updateBorders();
    }
    
    /**
     * Flip the card over
     * @param flipped True to set the card to flipped, false otherwise
     */
    public void setFlipped(boolean flipped){
        this.flipped = flipped;
        Main.mainController.updateCardView();
    }

    /**
     * Returns whether or not the displayed card is selected
     * @return True if it is selected, false otherwise
     */
    public boolean isSelected(){
        return isSelected;
    }

    /**
     * Remove the front and back brackets from a tag string
     * @param str The string to remove the brackets from
     * @return The resulting string with brackets removed
     */
    private String removeBrackets(String str){
        str = str.replaceAll("^\\[", "");
        str = str.replaceAll("\\]$", "");
        return str;
    }
    
    /**
     * Returns a reference to the card that this display card
     * represents
     * @return A reference to the card that this display card
     * represents
     */
    public Card getCard(){
        return actualCard;
    }

    /**
     * Read in the front side of a card
     * @param imgFront The image on the front of the card
     */
    @Override
    public void readFrontSide(BufferedImage imgFront, String fileExt) {
        
        frontContent = new ImagePanel(imgFront);
        
    }

    /**
     * Read in the front side of a card
     * @param textFront The text on the front of the card
     */
    @Override
    public void readFrontSide(String textFront) {
        
        JTextArea text = new JTextArea(textFront);
        text.addMouseListener(new CardMouseListener());
        text.setSize(CARD_SIZE);
        text.setLineWrap(true);
        text.setEditable(false);
        
        frontContent.add(text);
        
        
    }

    /**
     * Read in the back side of the card
     * @param imgBack The image on the back side of the card
     */
    @Override
    public void readBackSide(BufferedImage imgBack, String fileExt) {
        
        backContent = new ImagePanel(imgBack);

    }

    /**
     * Read in the back side of the card
     * @param textBack The text on the back side of the card
     */
    @Override
    public void readBackSide(String textBack) {
        
        JTextArea text = new JTextArea(textBack);
        text.addMouseListener(new CardMouseListener());
        text.setSize(CARD_SIZE);
        text.setLineWrap(true);
        text.setEditable(false);
        
        backContent.add(text);
        
    }
    
    /**
     * Listener for handling a flip card button press
     * Sets card side and updates the view
     * @author Trey
     */
    private class ButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent arg0) {
            setFlipped(!isFlipped());
            Main.mainController.updateCardView();
        }
        
    }
    
    private class CardMouseListener implements MouseListener{

        @Override
        public void mouseClicked(MouseEvent arg0) {
            // toggle selection
            isSelected = !isSelected;
            updateBorders();
        }

        @Override
        public void mouseEntered(MouseEvent arg0) {}
        @Override
        public void mouseExited(MouseEvent arg0) {}
        @Override
        public void mousePressed(MouseEvent arg0) {}
        @Override
        public void mouseReleased(MouseEvent arg0) {}
        
    }
    
    /**
     * Recolors the borders after changing isSelected
     */
    public void updateBorders(){
        
        if(isSelected){
            frontSide.setBorder(GUIConstants.HIGHLIGHT_BORDER);
            backSide.setBorder(GUIConstants.HIGHLIGHT_BORDER);
        } else {
            frontSide.setBorder(GUIConstants.NORMAL_BORDER);
            backSide.setBorder(GUIConstants.NORMAL_BORDER);
        }
        
        frontSide.revalidate();
        frontSide.repaint();
        backSide.revalidate();
        backSide.repaint();
    }
}
