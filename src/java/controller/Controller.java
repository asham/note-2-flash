package controller;

import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import main.GUI.CardDisplay;
import main.GUI.MainWindowGUI;
import model.Card;
import model.Deck;
import model.QueryProcessor;
import external_modules.AnkiFormat;
import external_modules.ExternalFormat;
import external_modules.Note2FlashFormat;
import external_modules.PDFFormat;

/**
 * Controller contains methods which will be called on GUI events. They will
 * then call appropriate update methods on MainWindowGUI to update the view
 * @author Trey, Will
 */
public class Controller {
    /**
     * The default deck name
     */
    private static final String DEFAULT_NAME = "Default";
    
    /**
     * List containing the external modules which are plugged in
     */
    private ArrayList<ExternalFormat> externalModules;
    
    /**
     * The deck currently being viewed
     */
    private Deck currentDeck;
    private Boolean debugFlag;
    
    /**
     * The names of the decks in the deck view JTree
     */
    private List<String> displayDecks;
    
    /**
     * Reference to the main window for components that need their parent
     */
    public MainWindowGUI mainWindow;
    
    /**
     * Default constructor (called directly to debug)
     * @param debug true if debugging (no GUI will be created)
     */
    public Controller(boolean debug){
        
        try{
            QueryProcessor qp = QueryProcessor.getInstance();
            displayDecks = qp.getDecks();
            
        } catch (Error e) {
            JOptionPane.showMessageDialog(null, "Note 2 Flash already running. Close other instances", "ERROR", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        
        if(displayDecks.isEmpty()){
            this.currentDeck = new Deck(DEFAULT_NAME);
            displayDecks.add(DEFAULT_NAME);
            
        } else {
            // just get the first deck
            this.currentDeck = Deck.getStoredDeck(displayDecks.get(0));
        }
        
        externalModules = new ArrayList<ExternalFormat>();
        externalModules.add(new AnkiFormat());
        externalModules.add(new Note2FlashFormat());
        externalModules.add(new PDFFormat());
        
        if ( debug == true ){
            this.debugFlag = true;
            this.mainWindow = null;
        } else {
            this.debugFlag = false;
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    createGUI();
                }
            });
        }
        
    }
    
    private void createGUI(){
        this.mainWindow = new MainWindowGUI("Note 2 Flash");
        this.debugFlag = false;

        // set original cards
        mainWindow.setCardView(getCurrentDisplayCards());
        mainWindow.setDeckView(displayDecks);
        mainWindow.setSelectedDeck(0);
        mainWindow.setTags(getCurrentTags());
    }
    /**
     * Controller method for updating the GUI card view
     */
    public void updateCardView(){
        mainWindow.updateCardView();
    }
    
    /**
     * Method called when user presses the add card button that creates and adds a new
     * card to the current deck, then updates the view to show the card at the top. Pass in
     * null for parameters that aren't being used.
     * 
     * @param frontText String representing the information on the front of the card
     * @param frontImage BufferedImage representing the information on the front of the card
     * @param frontFileExt String representing the file extension of the front image
     * @param backText String representing the information on the back of the card
     * @param backImage Image representing the information on the back of the card
     * @param backFileExt String representing the file extension of the back image
     * @param tags String containing comma separated list of tags
     */
    public void addCardButtonPressed(String frontText, BufferedImage frontImage, String frontFileExt,
            String backText, BufferedImage backImage, String backFileExt, String tags){
        Card newCard;
        
        // split on comma surrounded by spaces
        String[] tagArray = tags.split(",");
        for (int i = 0; i < tagArray.length; i++) {
            tagArray[i] = tagArray[i].trim();
        }
        Set<String> tagSet = new HashSet<String>(Arrays.asList(tagArray));
        
        if(frontImage == null){
            if(backImage == null){
                newCard = new Card(frontText, backText, tagSet);
            } else {
                newCard = new Card(frontText, backImage, backFileExt, tagSet);
            }
        } else {
            if(backImage == null){
                newCard = new Card(frontImage, frontFileExt, backText, tagSet);
            } else {
                newCard = new Card(frontImage, frontFileExt, backImage, backFileExt, tagSet);
            }
        }
        
        List<Card> newCardList = new ArrayList<Card>();
        newCardList.add(newCard);
        currentDeck.addCards(newCardList);
        
        if(!debugFlag){
            mainWindow.setCardView(getCurrentDisplayCards());
            mainWindow.setTags(getCurrentTags());
        }
        
    }
    
    /**
     * Method called when user presses the edit card button
     */
    public void editCardButtonPressed(){
        List<CardDisplay> selected;
        if(!debugFlag) {
            selected = mainWindow.getSelectedCards();
        } else {
            List<Card> cards = currentDeck.getCards();
            selected = new ArrayList<CardDisplay>();
            for(Card c : cards){
                selected.add(new CardDisplay(c));
            }
        }
        
        if(selected.isEmpty()){
            // nothing selected, display error
            JOptionPane.showMessageDialog(mainWindow, "No cards selected.", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            List<Card> cards = new ArrayList<Card>();
            for(int i=0; i < selected.size(); i++){
                cards.add(selected.get(i).getCard());
            }
            mainWindow.openEditDialog(cards);
        }

        
    }
    
    /**
     * Callback from the edit card dialog. Information on the new content of the card is
     * passed from the dialog.
     * 
     * @param oldCard the Card to edit
     * @param frontText String representing the front text (null if image)
     * @param frontImage BufferedImage representing the front image (null if text)
     * @param frontFileExt the file extension of the front image
     * @param backText String representing the back text (null if image)
     * @param backImage BufferedImage representing the back image (null if text)
     * @param backFileExt the file extension of the back image
     * @param tags The tags of the card as a comma separated string
     */
    public void editCardCallback(Card oldCard, String frontText, BufferedImage frontImage, String frontFileExt,
            String backText, BufferedImage backImage, String backFileExt, String tags){
        
        // split on comma surrounded by spaces
        String[] tagArray = tags.split(",");
        for (int i = 0; i < tagArray.length; i++) {
            tagArray[i] = tagArray[i].trim();
        }
        Set<String> tagSet = new HashSet<String>(Arrays.asList(tagArray));
        
        oldCard.setTags(tagSet);
        
        if(frontImage == null){
            oldCard.setFront(frontText);
        } else { 
            oldCard.setFront(frontImage, frontFileExt);
        }
        
        if(backImage == null){
            oldCard.setBack(backText);
        } else {
            oldCard.setBack(backImage, backFileExt);
        }
        
        if(!debugFlag){
            mainWindow.setCardView(getCurrentDisplayCards());
            mainWindow.setTags(getCurrentTags());
        }
        
    }
    
    
    /**
     * Method called when the user presses the remove card button which will remove the 
     * card from the currently selected deck and update the view
     */
    public void removeCardButtonPressed(){
        // Get selected cards from the GUI to determine which cards we're removing
        List<Card> toRemove = new ArrayList<Card>();
        List<CardDisplay> selected;
        if(!debugFlag) selected = mainWindow.getSelectedCards();
        else {
            List<Card> cards = currentDeck.getCards();
            selected = new ArrayList<CardDisplay>();
            for(Card c : cards){
                selected.add(new CardDisplay(c));
            }
        }
        
        if(selected.isEmpty()){
            // nothing selected, display error
            JOptionPane.showMessageDialog(mainWindow, "No cards selected.", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            // get the real cards from the CardDisplay representations
            for(CardDisplay cd: selected){
                toRemove.add(cd.getCard());
            }
            // remove the cards
            currentDeck.removeCards(toRemove);
            if(!debugFlag){
                mainWindow.setCardView(getCurrentDisplayCards());
                mainWindow.setTags(getCurrentTags());
            }
        
        }
    }
    
    /**
     * Method called when the user selects a new filter, which updates the view based on 
     * the given tag strings.
     * 
     * @param tags A set of Strings containing the tags to filter by
     * @return 
     */
    public void filterCardsAction(String tagString){
        
        // if the filter is the empty string, then get all cards in deck
        if(tagString.equals("")){
           mainWindow.setCardView(getCurrentDisplayCards()); 
        } else {
        
            String[] tagArray = tagString.split("[,\\s]+");
            
            Set<String> tagSet = new HashSet<String>(Arrays.asList(tagArray));
            List<Card> filteredCards = currentDeck.getCardsByTag(tagSet);
            mainWindow.setCardView(getDisplayCards(filteredCards));
            mainWindow.setTags(getCurrentTags());
        }
    }
    
    /**
     * Method called when user selects a deck, which updates the main window view
     * 
     * @param deckName The name of the deck selected
     */
    public void deckSelected(String deckName){
        
        currentDeck = Deck.getStoredDeck(deckName);
        if(currentDeck == null) {
            throw new IllegalArgumentException("Deck name does not exist and can't be selected.");
        }
        if(!debugFlag) {
            mainWindow.setCardView(getCurrentDisplayCards());
            mainWindow.setTags(getCurrentTags());
        }
        
        
    }
    
    /**
     * Method called when the user presses the create multiple cards button
     * @throws  IllegalArgumentException if the list of images are not the same size, or if
     *          either argument is null, or if one of the images is null
     * @param front
     * @param back
     */
    public void createMultipleCardsButtonPressed(List<BufferedImage> front, List<BufferedImage> back, String fileExt){
        
        List<Card> newCards = new ArrayList<Card>();
        
        if(front == null || back == null || front.size() != back.size()){
            throw new IllegalArgumentException();
        }
        
        for(int i = 0; i < front.size(); i++){
            
            if(front.get(i) == null || back.get(i) == null){
                throw new IllegalArgumentException();
            }
            
            newCards.add(new Card(front.get(i), fileExt, back.get(i), fileExt, new HashSet<String>()));
            
        }
        
        currentDeck.addCards(newCards);
        
        if(!debugFlag){ 
            mainWindow.setCardView(getCurrentDisplayCards());
            mainWindow.setTags(getCurrentTags());
        }
        
    }
    
    /**
     * Method called when the user presses the add deck button which creates
     * a deck with the given deck name, and adds it to the view
     */
    public void addDeckButtonPressed(String deckName){
        String trimmedName = deckName.trim();
        if(!trimmedName.equals("")){
            if(Deck.getStoredDeck(trimmedName) == null){
                Deck newDeck = new Deck(trimmedName);
                currentDeck = newDeck;
                displayDecks.add(trimmedName);
                
                if(!debugFlag){
                    mainWindow.setCardView(getCurrentDisplayCards());
                    mainWindow.setDeckView(displayDecks);
                    mainWindow.setSelectedDeck(displayDecks.size() - 1);
                }
            }  else {
                JOptionPane.showMessageDialog(null, "There is already a deck named " + trimmedName + ".", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
        
    }
    
    /**
     * Method called when the user presses the delete deck button which removes
     * the selected deck
     */
    public void deleteDeckButtonPressed(){
        
        String deckNameToRemove = currentDeck.getDeckName();
        
       if(debugFlag || JOptionPane.showConfirmDialog(null, 
               "Confirm that you want to delete this deck.\nAll cards within \"" + deckNameToRemove + "\" will be lost.",
               "Delete Deck", JOptionPane.YES_NO_OPTION) 
               == JOptionPane.YES_OPTION)
       {
            
            displayDecks.remove(deckNameToRemove);
            currentDeck.remove();
            
            if(displayDecks.isEmpty()){
                // there are no decks, create default
                currentDeck = new Deck("Default");
                displayDecks.add("Default");
            } else {
                // just get the first one
                currentDeck = Deck.getStoredDeck(displayDecks.get(0));
            }
            if(currentDeck == null){
                throw new NullPointerException("deckname " + displayDecks.get(0) + " does not exist");
            }
            if(!debugFlag){
                mainWindow.setDeckView(displayDecks);
                mainWindow.setCardView(getCurrentDisplayCards());
                mainWindow.setSelectedDeck(0);
            }
       }
    }
    
    /**
     * Method called when the user presses the import button
     * 
     * @param filePath The path to the file from which to import
     * @param deckName The name of the deck to be created
     * @return true iff imported files could be found, else false. 
     */
    public boolean importButtonPressed(String filePath, String mediaFolder, String deckName){
        // verify that input file exists
        if (!new File(filePath).exists()) {
            JOptionPane.showMessageDialog(null, 
                    "Unable to locate input file.", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }     
        
        // verify that input media directory exists (if it's provided)
        if (mediaFolder.trim().length() != 0 && !new File(mediaFolder).exists()) {
            JOptionPane.showMessageDialog(null, 
                    "Unable to locate media folder.", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }     
        
        // do not allow import if deckname is empty or duplicate
        if (deckName.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, 
                    "No deck name entered.", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        if(displayDecks.contains(deckName)){
            JOptionPane.showMessageDialog(null, 
                    "Duplicate deck name entered.", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        
        if(!filePath.equals("") && filePath.lastIndexOf('.') > 0){
            String extension = filePath.substring(filePath.lastIndexOf('.'));
            System.out.println("Extension is " + extension);
            // find module to handle file extension
            for(ExternalFormat format : externalModules){
                if(format.isImporter(extension)){
                    System.out.println("import module found for extension " + extension);
                    //if the imported file is anki, the format module will receive 
                    //the file location and media folder location squashed together
                    //in the same string - because the kludge life is the life for me.
                    if (extension.equals(".txt")) {
                    	filePath = filePath.concat("%" + mediaFolder);
                    }
                    try {
                    	format.importCards(filePath, 
                    		QueryProcessor.getInstance(), deckName);
                    } catch (Exception e) {
                    	JOptionPane.showMessageDialog(null, 
                                e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                    //Update view if a new deck was create in the process of importing
                    if(!deckName.equals("") && !displayDecks.contains(deckName)){
                        displayDecks.add(deckName);
                        mainWindow.setDeckView(displayDecks);
                        mainWindow.setSelectedDeck(displayDecks.size() - 1);
                    }
                    deckSelected(deckName);
                }
            }
        }
        return true;
    }
    
    /**
     * Helper function which opens the PDF file that was saved
     * @param directory the location of the pdf file
     */
    private void openPDF(String directory){
        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File(directory + "\\Note2Flash.pdf");
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                // no application registered for PDFs
            }
        }
    }
    
    /**
     * Method called when the user presses the export button
     * 
     * @param exportPath The path to export the file to
     * @param exportType The type of file to export
     * @return true iff export location could be found, else false. 
     */
    public boolean exportButtonPressed(String exportPath, String exportType){
        // verify that output directory exists
        if (!new File(exportPath).exists()) {
            JOptionPane.showMessageDialog(null, 
                    "Unable to locate output directory.", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }        
        
        // convert CardDisplay to cards
        List<CardDisplay> cardDisplays = mainWindow.getSelectedCards();
        
        List<Card> cards = new ArrayList<Card>();
        
        if(cardDisplays.size() == 0){
            cards = currentDeck.getCards();
        } else {
            for(CardDisplay card:cardDisplays){
                cards.add(card.getCard());
            }
        }
        // find module to handle the export type
        for(ExternalFormat format : externalModules){
            if(format.isExporter(exportType)){
                try {
                    format.exportCards(exportPath, cards);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, 
                            e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                // open PDF after saving
                if(!debugFlag && cards.size() > 0 && format instanceof PDFFormat){
                    openPDF(exportPath);
                }
            }
        }
        
        return true;
    }
    
    
    /**
     * Private helper method that returns the displayable cards for the GUI
     * @param cards The cards to get the displayable cards from
     * @return The displayable cards
     */
    private List<CardDisplay> getCurrentDisplayCards(){
        return getDisplayCards(currentDeck.getCards());
    }
    
    /**
     * Private helper method that returns a list of display cards from
     * a list of cards
     * @param cards The cards to get display cards for
     * @return A list of display cards that the GUI can use
     */
    private List<CardDisplay> getDisplayCards(List<Card> cards){
        List<CardDisplay> result = new ArrayList<CardDisplay>();
        
        for(Card card: cards){
            result.add(new CardDisplay(card));
        }
        
        return result;
    }
    
    /**
     * Returns a set of cards found in the current deck
     * @return A set of cards representing all tags in the current deck
     */
    private Set<String> getCurrentTags(){
        return getTags(currentDeck.getCards());
    }
    
    private Set<String> getTags(List<Card> cards){
        
        Set<String> result = new HashSet<String>();
        
        for(Card card: cards){
            result.addAll(card.getTags());
        }
        
        return result;
    }
}
