Note2Flash is a Java based application for creating and managing flash cards. The goal is to make
the flash card creation process as fast and easy as possible, to cut down on the amount of time 
students spend doing mechanical busy-work and allow them to spend more time actually studying.
 
You can use cards created with Note2Flash in your favorite card viewer, and can even import and 
export existing cards from other formats, making sharing between viewers a breeze

Users and developers can find more information online: https://bitbucket.org/asham/note-2-flash/wiki/Home

You can also pull the Note2Flash wiki repo by visitting the page listed above and
clicking the "Clone Wiki" button in the top right corner of the screen.

In order to run Note2Flash, double click on Note2Flash.jar

NOTE: Only one instance of Note2Flash can be running at a time